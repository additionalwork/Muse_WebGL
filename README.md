

# Naval Games

## Project overview

WebGL приложение для виртуальных экскурсий по военно-морскому музею.
Весь контент динамически подгружается с сервера и соответствует конкретному музею.
(C# код: https://gitlab.com/additionalwork/Muse_WebGL/-/tree/trunk/Assets/Scripts?ref_type=heads)

## Files Structure
<details style="text-indent: 1em;">
    <summary>AddressableAssetsData</summary>

    * AssetGroups
    * AssetGroupTemplates
    * DataBuilders
    * WebGL
    * Windows

</details>

<details style="text-indent: 1em;">
    <summary>AddressableResources</summary>

    * Exhibits
    * Quizzes
    * Tasks

</details>

<details style="text-indent: 1em;">
    <summary>Art</summary>

    * Fonts
    * Materials
    * Models
    * Sounds
    * Textures

</details>

<details style="text-indent: 1em;">
    <summary>Docs</summary>

    * Naval

</details>

<details style="text-indent: 1em;">
    <summary>Localization</summary>

    * Locales
    * LocalizationTables
    
</details>

<details style="text-indent: 1em;">
    <summary>Plugins</summary>
</details>

<details style="text-indent: 1em">
    <summary>Resources</summary>
    <details style="text-indent: 2em">
        <summary>Prefabs</summary>
        <details style="text-indent: 3em">
            <summary>UI</summary>
            <details style="text-indent: 4em">
                <summary>Windows</summary>
            </details>
        </details>
    </details>
</details>

<details style="text-indent: 1em;">
    <summary>Scenes</summary>
</details>

<details style="text-indent: 1em;">
    <summary>Scripts</summary>

    * EditorScripts
    * Extensions
    * MobileOrientation
    * Repository
    * ScriptableObjects
    * UI

</details>

<details style="text-indent: 1em;">
    <summary>Settings</summary>
</details>

<details style="text-indent: 1em;">
    <summary>Sounds</summary>
</details>

<details style="text-indent: 1em;">
    <summary>TestsEditMode</summary>
</details>

<details style="text-indent: 1em;">
    <summary>TestsPlayMode</summary>
</details>

<details style="text-indent: 1em;">
    <summary>TextMesh Pro</summary>
</details>

<details style="text-indent: 1em;">
    <summary>WebGLTemplates</summary>
</details>


## [Documentation](./Assets/Docs/Docs.md)
