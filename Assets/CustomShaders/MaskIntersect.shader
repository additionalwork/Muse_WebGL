Shader "CustomRenderTexture/MaskIntersect"
{
    Properties
    {
        _MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
        _Cutoff ("Base Alpha cutoff", Range (0,1)) = 0.5
    }

    SubShader {  
        Offset 0, -1
        ColorMask 0
        ZWrite On
        Pass
        {
            AlphaTest Less [_Cutoff]      
            SetTexture [_MainTex] {
                combine texture * primary, texture
            }
        }
    }
}
