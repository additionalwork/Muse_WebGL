<style>
details {
  border: 1px solid #aaa;
  border-radius: 4px;
  padding: 0.5em 0.5em 0;
}
summary {
    cursor: pointer;
    margin-left: auto;
    margin-right: auto;
    font-weight: bold;
    padding: 0.5em;
    outline: none;
    font-size: 1.5em;
}

details[open] {
  padding: 0.5em;
}

details[open] > summary::marker {
    content: "- ";
}

details > summary::marker {
    content: "+ ";
}

ul::marker {
    display: none;
}

</style>

# Naval Games technical documentation
<details>
    <summary>Naval.UI</summary>

- [BaseWindow](./Naval/UI/BaseWindowBehaviour.md)
- [WindowAbstract](./Naval/UI/WindowAbstract.md)
- [MainMenuController](./Naval/UI/MainMenuController.md)
- [QRCodeController](./Naval/UI/QRCodeController.md)
- [GuideController](./Naval/UI/GuideController.md)
- [GuideManShowing](./Naval/UI/GuideManShowing.md)
- [GuideMenuExhibits](./Naval/UI/GuideMenuExhibits.md)
- [SlideShowing](./Naval/UI/SlideShowing.md)
- [SubtitleShowing](./Naval/UI/SubtitleShowing.md)
- [BoatScheme](./Naval/UI/BoatScheme.md)
- [ExhibitButton](./Naval/UI/ExhibitButton.md)
- [SettingsSaver](./Naval/UI/SettingsSaver.md)
- [SwitchToggle](./Naval/UI/SwitchToggle.md)
</details>

<details>
    <summary>Naval</summary>

- [GameObject Localize Extension](./Naval/GameObjectLocalizeExtension.md)
- [Animation Unit](./Naval/AnimationUnit.md)
- [FAQ Unit](./Naval/FAQUnit.md)
- [Picture Unit](./Naval/PictureUnit.md)
- [Speech Unit](./Naval/SpeechUnit.md)
- [Speech Unit](./Naval/SpeechUnit.md)
- [Exhibit](./Naval/Exhibit.md)
- [ExhibitRepository](./Naval/ExhibitRepository.md)
- [AudioPlayer](./Naval/AudioPlayer.md)
</details>


