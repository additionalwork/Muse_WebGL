<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class Exhibit

Namespace: Naval

Inheritance:

<details open>
    <summary>Object</summary>
    <p>Exhibit</p>
</details>


## Properties
| Access | Type            | Name                    |
|--------|-----------------|-------------------------|
| public | bool            | HasParent               |
| public | Exhibit[]       | Children                |
| public | Exhibit         | Parent                  |
| public | string          | Id                      |
| public | bool            | IsRoot                  |
| public | bool            | IsHall                  |
| public | string          | NameKey                 |
| public | string          | LocalizeStringTable     |
| public | string          | LocalizeAudioTable      |
| public | PictureUnit[]   | PictureUnits            |
| public | SpeechUnit[]    | SpeechUnits             |
| public | AnimationUnit[] | AnimationUnits          |
| public | FAQUnit[]       | FAQUnits                |
| public | string          |  ExcursionNextExhibitId |