<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class ExhibitRepository

Namespace: Naval

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>ExhibitRepository</p>
</details>

## Methods
| Access  | Signature                                                       | Description                                           |
|---------|-----------------------------------------------------------------|-------------------------------------------------------|
| public  | void Start()                                                    | Initializing audioSource                              |
| public  | void PlayAudio(AudioClip, float, float)                         | Starts ToPlayVolumeSmooth coroutine                   |
| public  | void PlayAudio(AudioClip, (float min, float max), float, float) | Starts ToPlayVolumeSmooth coroutine with random pitch |
| public  | void StopAudio(float)                                           | Starts ToStopVolumeSmooth coroutine                   |
| public  | void OnDestroy()                                                | Plays audioClip after destruction                     |
| private | IEnumerator ToStopVolumeSmooth(float)                           | Smooth ending transition of audioClip                 | 
| private | IEnumerator ToPlayVolumeSmooth(AudioClip, float, float)         | Smooth starting transition of audioClip               | 


## Fields
| Access  | Type        | Name                |
|---------|-------------|---------------------|
| private | AudioClip   | audioClipForDestroy |
| private | AudioSource | audioSource         |
| private | Coroutine   | coroutine           |
