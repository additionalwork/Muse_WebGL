<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Struct AnimationUnit

Namespace: Naval

Inheritance:

<details open>
    <summary>Object</summary>
    <p>AnimationUnit</p>
</details>

## Fields
| Access  | Type   | Name           |
|---------|--------|----------------|
| public  | string | AnimationKey   |
| public  | float  | PlaybackDelay  |