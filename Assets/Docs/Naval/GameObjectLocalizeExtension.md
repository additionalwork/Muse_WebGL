<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Static Class GameObjectLocalizeExtension

Namespace: Naval

Inheritance:

<details open>
    <summary>Object</summary>
    <p>GameObjectLocalizeExtension</p>
</details>

## Static Methods
| Access | Signature                                                                | Description                       |
|--------|--------------------------------------------------------------------------|-----------------------------------|
| public | void SetLocalizedString(this, string, string, params (string, string)[]) | Sets localized string for this    |
| public | void SetLocalizedAudioClip(this, string, string)                         | Sets localized audioClip for this |
