<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class ExhibitRepository

Namespace: Naval

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>ExhibitRepository</p>
</details>

## Methods
| Access  | Signature                                          | Description                   |
|---------|----------------------------------------------------|-------------------------------|
| private | void Awake()                                       | Initializing fields           |
| private | Exhibit CreateExhibit(ExhibitProfile, Exhibit)     | Creates new exhibit from node |
| private | Exhibit[] CollectChildren(ExhibitProfile, Exhibit) | Array of children Exhibits    | 
| private | Exhibit GetExhibit(string)                         | Returns exhibit by key        | 
| private | FAQUnit GetFAQ(string, int)                        | Returns FAQ from exhibit      | 


## Fields
| Access  | Type                        | Name              |
|---------|-----------------------------|-------------------|
| private | ExhibitProfile              | ExhibitBoatRoot   |
| private | Exhibit                     | Root              |
| private | Dictionary<string, Exhibit> | ExhibitDictionary |
