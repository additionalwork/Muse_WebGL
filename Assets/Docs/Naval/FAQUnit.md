<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Struct FAQUnit

Namespace: Naval

Inheritance:

<details open>
    <summary>Object</summary>
    <p>FAQUnit</p>
</details>

## Fields
| Access | Type            | Name           |
|--------|-----------------|----------------|
| public | string          | FAQNameKey     |
| public | PictureUnit[]   | PictureUnits   |
| public | SpeechUnit[]    | SpeechUnits    |
| public | AnimationUnit[] | AnimationUnits |