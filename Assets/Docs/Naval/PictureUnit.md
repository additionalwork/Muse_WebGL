<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Struct PictureUnit

Namespace: Naval

Inheritance:

<details open>
    <summary>Object</summary>
    <p>PictureUnit</p>
</details>

## Fields
| Access  | Type   | Name            |
|---------|--------|-----------------|
| public  | Sprite | Image           |
| public  | float  | AppearanceDelay |