<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Struct SpeechUnit

Namespace: Naval

Inheritance:

<details open>
    <summary>Object</summary>
    <p>SpeechUnit</p>
</details>

## Fields
| Access  | Type   | Name          |
|---------|--------|---------------|
| public  | string | AudioClipKey  |
| public  | string | SubtitlesKey  |
| public  | float  | PlaybackDelay |