<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class MainMenuController

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>MainMenuController</p>
</details>

## Methods
| Access  | Signature                   | Description                                                             |
|---------|-----------------------------|-------------------------------------------------------------------------|
| private | void Awake()                | Checks mainMenuController for uniqueness and sets instance to this      |
| private | void Start()                | Adds CreateWindow listeners onClick for all Buttons or starts Excursion |
| private | void OnDestroy()            | Removes Buttons onClick listener                                        | 
| private | void CreateMenu(GameObject) | Hides main menu, Instantiates window from parameter                     | 
| public  | void ShowMenu()             | Sets menu to active                                                     | 
| public  | void HideMenu()             | Sets menu to inactive                                                   | 

## Static Properties
| Access | Type               | Name         |
|--------|--------------------|--------------|
| public | MainMenuController | Instance     |
| public | Transform          | ScreenCanvas |

## Fields
| Access  | Type                     | Name     |
|---------|--------------------------|----------|
| private | ButtonWindowPrefabPair[] | Buttons  |

## Classes
[ButtonWindowPrefabPair]()