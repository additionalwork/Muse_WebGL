<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class SubtitleShowing

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>SubtitleShowing</p>
</details>

## Methods
| Access   | Signature                                | Description                                 |
|----------|------------------------------------------|---------------------------------------------|
| public   | void ShowSubtitle(string, string, float) | Starts ShowingRoutine with current text     |
| public   | void HideSubtitles()                     | Starts ToHideSubtitles coroutine            | 
| private  | Color SetAlphaForColor(Color, float)     | Sets alpha value for the color              | 
| private  | Color IterateAlphaForColor(Color, float) | Increases alpha value for the color         | 
| private  | IEnumenator ToShowSubtitle(float)        | Fades from alpha and show text for showTime |
| private  | IEnumenator ToHideSubtitle()             | Fades to alpha                              |


## Fields
| Access  | Type            | Name           |
|---------|-----------------|----------------|
| private | TextMeshProUGUI | text           |
| private | Image           | background     |
| private | float           | timeToFade     |
| private | Coroutine       | showingRoutine |
