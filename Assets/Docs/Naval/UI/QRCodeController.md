<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class QRCodeController

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>QRCodeController</p>
</details>

## Methods
| Access  | Signature           | Description                                                        |
|---------|---------------------|--------------------------------------------------------------------|
| private | IEnumenator Start() | Gets permission from browser to start webcam and starts it         |
| private | void Update()       | if webcam is on and qrcode is on screen, scans it and stop camera. |
| private | void StartCamera()  | Shows live webcam image on screen                                  | 
| private | void StopCamera()   | Stops showing live webcam image on screen                          | 
| private | void SwapCamera()   | Turns on next camera                                               | 



## Fields
| Access  | Type                 | Name                |
|---------|----------------------|---------------------|
| private | RawImage             | display             |
| private | WebCamDevice[]       | _devices            |
| private | WebCamTexture        | _texture            |
| private | int                  | _currentCameraIndex |
| private | Color32[]            | _cameraColorData    |
| private | bool                 | _finalWebcamSetup   |
| private | ZXing.IBarcodeReader | _barcodeReader      |
| private | ZXing.Result         | _result             |
