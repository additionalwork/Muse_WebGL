<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class BoatScheme

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>WindowAbstract, IDragHandler, IInitializePotentialDragHandler</summary>
    <p>BoatScheme</p>
</details>

## Methods
| Access  | Signature                                        | Description                                                                                    |
|---------|--------------------------------------------------|------------------------------------------------------------------------------------------------|
| private | void Start()                                     | Calls InitiateMap()                                                                            |
| private | void Update()                                    | Every frame calls UpdateExhibitMarks(), UpdateScale() if wheel was scrolled and updateDragging |
| private | void UpdateDragging(Vector3)                     | Ensures that position matches the requirements and applies it to SchemeTransform               | 
| private | void UpdateScale(float)                          | Ensures that scale matches the requirements and applies it to SchemeTransform                  |
| private | void UpdateExhibitMarks()                        |                                                                                                | 
| private | void InitiateMap()                               | Sets Canvas and initial scale                                                                  |
| public  | void OnDrug(PointerEventData)                    | Gets new position from event data and calls UpdateDragging                                     | 
| public  | void OnInitializePotentialDrag(PointerEventData) | Sets useDragThreshold to false                                                                 | 

## Fields
| Access  | Type          | Name            |
|---------|---------------|-----------------|
| private | RectTransform | SchemeTransform |
| private | float         | ZoomSpeed       |
| private | Canvas        | Canvas          |