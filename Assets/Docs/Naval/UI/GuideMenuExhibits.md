<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class GuideMenuExhibits

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>WindowAbstract</summary>
    <p>GuideMenuExhibits</p>
</details>

## Methods
| Access   | Signature                      | Description                              |
|----------|--------------------------------|------------------------------------------|
| private  | void Start()                   | Sets listeners to Buttons                |
| private  | void ActivateExhibitsContent() | Sets buttons to start excursions         | 
| private  | void ActivateFAQContent()      | Sets buttons to start faq                | 
| private  | void ClearScrollViewContent()  | Destroys scroll view content             |
| private  | void SelectButton(Button)      | Clears Scroll view and activates content |


## Fields
| Access  | Type       | Name              |
|---------|------------|-------------------|
| private | Button     | Exhibits          |
| private | Button     | FAQ               |
| private | Transform  | ScrollViewContent |
| private | GameObject | ButtonTilePrefab  |
| private | Button     | SelectedButton    |

