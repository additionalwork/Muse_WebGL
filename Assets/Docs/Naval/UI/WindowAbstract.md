<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class WindowAbstract

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>WindowAbstract</p>
</details>

## Methods
| Access  | Signature                    | Description                  |
|---------|------------------------------|------------------------------|
| private | virtual void DestroyWindow() | Destroys attached gameObject |
