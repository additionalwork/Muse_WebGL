<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class SlideShowing

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>SlideShowing</p>
</details>

## Methods
| Access   | Signature                                | Description                                  |
|----------|------------------------------------------|----------------------------------------------|
| public   | void ShowSlide(Sprite, float)            | Starts ShowingRoutine with current sprite    |
| public   | void HideSlide()                         | Starts ToHideSlide coroutine                 | 
| private  | Color SetAlphaForColor(Color, float)     | Sets alpha value for the color               | 
| private  | Color IterateAlphaForColor(Color, float) | Increases alpha value for the color          | 
| private  | IEnumenator ToShowSlide(float)           | Fades from alpha and show image for showTime |
| private  | IEnumenator ToHideSlide()                | Fades to alpha                               |


## Fields
| Access  | Type      | Name           |
|---------|-----------|----------------|
| private | Image     | image          |
| private | float     | timeToFade     |
| private | Coroutine | showingRoutine |
