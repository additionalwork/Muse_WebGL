<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class ExhibitButton

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>ExhibitButton</p>
</details>

## Methods
| Access  | Signature        | Description                                              |
|---------|------------------|----------------------------------------------------------|
| private | void Start()     | Adds listener to ButtonComponent for starting exhibition |
| private | void OnDestroy() | Removes all onClick listeners from button component      |