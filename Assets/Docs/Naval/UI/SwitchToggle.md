<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class SwitchToggle

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>SwitchToggle</p>
</details>

## Methods
| Access  | Signature           | Description                   |
|---------|---------------------|-------------------------------|
| private | void Awake()        | Sets listeners to move handle |
| private | void OnDestroy()    | Removes listeners from toggle |
| private | void OnSwitch(bool) | Switch colors and position    |

## Fields
| Access  | Type          | Name                    |
|---------|---------------|-------------------------|
| private | Toggle        | _toggle                 |
| private | Vector2       | _handlePosition         |
| private | Color         | _backgroundDefaultColor |
| private | Color         | _handleDefaultColor     |
| private | Image         | _backgroundImage        |
| private | Image         | _handleImage            |
| private | Color         | backgroundActiveColor   |
| private | Color         | handleActiveColor       |
| private | RectTransform | uiHandleRectTransform   |
