<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class GuideManShowing

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>GuideManShowing</p>
</details>

## Methods
| Access  | Signature                                          | Description                                                                                                                         |
|---------|----------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| private | void Start()                                       | Initializes childRenderers and make transition from transparent                                                                     |
| private | IEnumenator MaterialTransition(Func<float, float>) | Make transition according to function passed as parameter                                                                           | 
| public  | void Destroy                                       | Call this method instead of Destroy(gameObject). It will increase transparency and only after gameObject is transparent, destroy it | 
| private | void SetFloatAll(int, float)                       | Sets the property by id in all materials in childRenderers to the passed value                                                      | 
| private | void SetAllAlpha(float)                            | Sets all materials in childRenderers transparent property to value                                                                  | 


## Static Methods
| Access  | Signature                                          | Description                                                                                                                         |
|---------|----------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| private | void SetAlpha(Material, float alphaVal)            | Sets material's transparent property to value                                                                                       |


## Fields
| Access  | Type       | Name                         |
|---------|------------|------------------------------|
| private | Render[]   | _childRenderers              |
| private | float      | alphaChanelChangeStep        |
| private | float      | alphaTransitionStepDuration  |
| private | GameObject | guide                        |


## Static Fields
| Access  | Type | Name    |
|---------|------|---------|
| private | int  | Surface |
