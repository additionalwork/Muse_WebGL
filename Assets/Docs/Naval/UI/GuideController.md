<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class GuideController

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>GuideController</p>
</details>

## Methods
| Access  | Signature                                                  | Description                                                                                             |
|---------|------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| private | void Start()                                               | Sets onClick listeners on all speedButtons according to their speed value, pause, stop and skip buttons |
| private | void OnDestroy()                                           | Removes onClick listeners from all speedButtons, pause, stop and skip buttons, Time.timeScale = 1f      | 
| public  | AudioClip StartSpeech(SpeechUnit, StringTable, AudioTable) | Gets clip from AudioTable, plays it according for speed and show subtitles if necessary                 | 
| public  | void SetPlaySpeed(float)                                   | timeScale and audioSpeed changed                                                                        | 
| private | void TogglePauseSpeech()                                   | Pause and unpaused events                                                                               | 
| private | void StopExcursion()                                       | Calls StopExcursionRoutine                                                                              | 
| private | void ResetSpeech()                                         | Stops all previous speaking activity                                                                    | 
| private | void HideSlide()                                           | Stops slide Routine and hides slides                                                                    | 
| private | void StopExcursionRoutine()                                | Stops slide Routine and hides slides                                                                    | 
| private | void SkipSpeech()                                          | Skip to next speech in excursion                                                                        | 
| private | void PlayExcursion(Exhibit)                                | Ends current excursion and starts new one                                                               | 
| private | void PlayFAQ(Exhibit, int)                                 | Ends current excursion and starts FAQ coroutine                                                         | 
| private | IEnumenator ToPlayBoatExcursion(Exhibit)                   | Plays all boat excursion one by one                                                                     | 
| private | IEnumenator ToPlayHallExcursion(Exhibit)                   | Plays all hall excursion one by one                                                                     | 
| private | IEnumenator ToPlayExhibit(Exhibit)                         | Plays slides and speech for excursion                                                                   | 
| private | IEnumenator ToPlayFAQ(Exhibit, int)                        | Plays slides and speech for FAQ                                                                         | 
| private | IEnumenator ToPlaySpeech(Exhibit, SpeechUnit)              | Plays speech audioClip                                                                                  | 
| private | IEnumenator ToPlaySlides(Exhibit)                          | Plays slides one by one for the all speech duration time                                                |



## Fields
| Access  | Type            | Name                |
|---------|-----------------|---------------------|
| private | SpeedButton[]   | speedButton         |
| private | Button          | pauseButton         |
| private | Button          | stopButton          |
| private | Button          | skipButton          |
| private | SubtitleShowing | subtitles           |
| private | SlideShowing    | slides              |
| private | GuideManShowing | guideMan            |
| private | AudioPlayer     | audioPlayer         |
| private | AudioSource     | audioSource         |
| private | float           | currentTimeScale    |
| private | float           | currentPictureTimer |
| private | bool            | IsPaused            |
| private | Coroutine       | excursionRoutine    |
| private | Coroutine       | speechRoutine       |
| private | Coroutine       | slideRoutine        |
| private | float           | defaultDuration     |
| public  | Exhibit         | CurrentHall         |

## Structures
[SpeedButton]()