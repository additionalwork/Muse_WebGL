<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class BaseWindowBehaviour

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>BaseWindowBehaviour</p>
</details>

## Methods
| Access  | Signature             | Description                                                  |
|---------|-----------------------|--------------------------------------------------------------|
| private | void Start()          | Adds BackToMainMenu() as onClick listener for BackButton     |
| private | void Update()         | Every frame if Escape key is pressed, calls BackToMainMenu() |
| private | void OnDestroy()      | Removes BackButton onClick listener                          | 
| private | void BackToMainMenu() | Destroys current window and calls ShowMenu on MainMenu       | 

## Fields
| Access  | Type               | Name       |
|---------|--------------------|------------|
| private | Button             | BackButton |
| private | MainMenuController | MainMenu   |