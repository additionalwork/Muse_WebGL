<style>
details > summary {
width: 200px;
border: none;
cursor: pointer;
}

details > p {
padding: 10px;
margin: 0;
}
</style>

# Class SettingsSaver

Namespace: Naval.UI

Inheritance:

<details open>
    <summary>MonoBehaviour</summary>
    <p>SettingsSaver</p>
</details>

## Methods
| Access  | Signature        | Description                                                     |
|---------|------------------|-----------------------------------------------------------------|
| private | void Awake()     | Sets listeners to buttons in settings and values to PlayerPrefs |
| private | void OnDestroy() | Removes listeners from buttons in settings                      |

## Fields
| Access  | Type   | Name            |
|---------|--------|-----------------|
| private | Toggle | subtitlesToggle |
| private | Slider | VolumeSlider    |
| private | string | Subtitle        |
| private | string | Volume          |
