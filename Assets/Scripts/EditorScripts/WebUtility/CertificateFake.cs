﻿#if UNITY_EDITOR
namespace Naval.Editor
{
    using UnityEngine.Networking;

    public class CertificateFake : CertificateHandler
    {
        protected override bool ValidateCertificate(byte[] certificateData)
        {
            return true;
        }
    }
}
#endif
