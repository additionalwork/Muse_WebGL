namespace Naval.Editor
{
#if UNITY_EDITOR
    using System;
    using System.Collections;
    using System.Linq;
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;

    [CustomPropertyDrawer(typeof(SubclassPickerAttribute))]
    public class SubclassPickerDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Type t;
            if (this.fieldInfo.FieldType.IsArray)
            {
                t = this.fieldInfo.FieldType.GetElementType();
            }
            else
            {
                t = this.fieldInfo.FieldType;
            }

            var typeName = property.managedReferenceValue?.GetType().Name ?? "Not set";

            var dropdownRect = position;
            dropdownRect.x += EditorGUIUtility.labelWidth + 2;
            dropdownRect.width -= EditorGUIUtility.labelWidth + 2;
            dropdownRect.height = EditorGUIUtility.singleLineHeight;

            if (EditorGUI.DropdownButton(dropdownRect, new (typeName), FocusType.Keyboard))
            {
                var menu = new GenericMenu();

                menu.AddItem(new GUIContent("None"), property.managedReferenceValue == null, () =>
                {
                    property.managedReferenceValue = null;
                    property.serializedObject.ApplyModifiedProperties();
                });

                foreach (Type type in this.GetClasses(t))
                {
                    menu.AddItem(new GUIContent(type.Name), typeName == type.Name, () =>
                    {
                        property.managedReferenceValue = type.GetConstructor(Type.EmptyTypes).Invoke(null);
                        property.serializedObject.ApplyModifiedProperties();
                    });
                }

                menu.ShowAsContext();
            }

            EditorGUI.PropertyField(position, property, label, true);
        }

        private IEnumerable GetClasses(Type baseType)
        {
            return Assembly.GetAssembly(baseType).GetTypes().Where(t => t.IsClass && !t.IsAbstract && baseType.IsAssignableFrom(t));
        }
    }
#endif
}
