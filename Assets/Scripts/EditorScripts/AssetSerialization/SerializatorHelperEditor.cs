namespace Naval.Editor
{
#if UNITY_EDITOR
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(SerializatorHelper))]
    public class SerializatorHelperEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializatorHelper myScript = (SerializatorHelper)target;

            if (GUILayout.Button("Serialize Exhibit Default"))
            {
                myScript.SerializeExhibitsDefault();
            }

            if (GUILayout.Button("Serialize Exhibit Interactive"))
            {
                myScript.SerializeExhibitsInteractive();
            }

            if (GUILayout.Button("Serialize Quiz"))
            {
                myScript.SerializeQuiz();
            }

            if (GUILayout.Button("TEST"))
            {
                
            }
        }
    }
#endif
}
