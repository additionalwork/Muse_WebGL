namespace Naval.Model
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class SerializeDataCollection<T>
        where T : IData
    {
        public List<T> Profiles;
    }
}
