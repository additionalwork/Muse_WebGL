namespace Naval.Editor
{
    using System.IO;
    using System.Text;
    using UnityEngine;
    using Naval.Model;
#if UNITY_EDITOR
    using UnityEditor;
#endif
    using System.Linq;
    using System.Collections;

    public class SerializatorHelper : MonoBehaviour
    {
        public void Serialize()
        {
#if UNITY_EDITOR
            //AssetDatabase.ImportAsset(path);
#endif
        }

        public void SerializeExhibitsInteractive()
        {
            var repo = AppControllerSingleton.Instance.ExcursionInteractiveController.ExcursionInteractiveRepository;
            var objs = new WebProfileCollection<ExhibitInteractiveWebProfile>();
            objs.profiles = repo.GetAllData<ExhibitInteractive>()
                .Select(item =>
                {
                    var profile = PreserializeExhibitInteractive(item);
                    profile.Tasks = item.TaskUnits.Select(task => new TaskUnitWebProfile()
                    {
                        Type = new EnumWebProfile { EnumNumber = (int)task.Type },
                        Name = task.Name,
                        Text = task.Text,
                        CorrectAnswer = task.CorrectAnswer,
                        Hints = task.Hints.Select(hint => new TaskHintWebProfile()
                        {
                            Name = hint.Name,
                            PictureUnits = PreserializePictureUnit(hint.PictureUnits),
                            SpeechUnits = PreserializeSpeechUnit(hint.SpeechUnits),
                            AnimationUnits = PreserializeAnimationUnit(hint.AnimationUnits),
                        })
                        .ToArray(),
                        IncorrectAnswers = task.IncorrectAnswers,
                    })
                    .ToArray();
                    return profile;
                })
                .ToList();

            SaveJsonToFile(objs, "exhibits-interactive.json");

            ExhibitInteractiveWebProfile PreserializeExhibitInteractive(ExhibitInteractive item)
            {
                var profile = new ExhibitInteractiveWebProfile()
                {
                    Id = item.Id,
                    Name = item.Name,
                    IsHall = item.IsHall,
                    SpeechUnits = PreserializeSpeechUnit(item.SpeechUnits),
                    FAQUnits = PreserializeFAQUnit(item.FAQUnits),
                    PictureUnits = PreserializePictureUnit(item.PictureUnits),
                    Characters = PreserializeCharacter(item.Characters),
                    //ChildExhibitProfiles = item.ChildExhibits.Select(exhibit => exhibit.Id).ToArray(),
                };

                return profile;
            }
        }

        public void SerializeExhibitsDefault()
        {
            var repo = AppControllerSingleton.Instance.ExcursionDefaultController.ExcursionDefaultRepository;
            var objs = new WebProfileCollection<ExhibitDefaultWebProfile>();
            objs.profiles = repo.GetAllData<ExhibitDefault>()
                .Select(item => PreserializeExhibitDefault(item))
                .ToList();

            SaveJsonToFile(objs, "exhibits-default.json");

            ExhibitDefaultWebProfile PreserializeExhibitDefault(ExhibitDefault item)
            {
                var profile = new ExhibitDefaultWebProfile()
                {
                    Id = item.Id,
                    Name = item.Name,
                    IsHall = item.IsHall,
                    SpeechUnits = PreserializeSpeechUnit(item.SpeechUnits),
                    FAQUnits = PreserializeFAQUnit(item.FAQUnits),
                    PictureUnits = PreserializePictureUnit(item.PictureUnits),
                    Characters = PreserializeCharacter(item.Characters),
                    //ChildExhibitProfiles = item.ChildExhibits.Select(exhibit => exhibit.Id).ToArray(),
                };

                return profile;
            }
        }

        public void SerializeQuiz()
        {
            var repo = AppControllerSingleton.Instance.QuizController.QuizRepository;
            var objs = new WebProfileCollection<QuestionWebProfile>();
            objs.profiles = repo.GetAllData<QuizQuestion>()
                .Select(item => PreserializeQuiz(item))
                .ToList();

            SaveJsonToFile(objs, "quiz.json");

            QuestionWebProfile PreserializeQuiz(QuizQuestion item)
            {
                var profile = new QuestionWebProfile()
                {
                    Name = item.Id,
                    CorrectAnswer = item.CorrectAnswer,
                    IncorrectAnswers = item.IncorrectAnswers,
                };

                switch (item.QuestionType)
                {
                    case TextQuestionType question:
                        profile.QuestionTitle = question.QuestionTitle;
                        profile.QuizQuestionType = new EnumWebProfile { Name = question.GetType().Name };
                        profile.EntityValue = question.Text;
                        break;

                    case ImageQuestionType question:
                        profile.QuestionTitle = question.QuestionTitle;
                        profile.QuizQuestionType = new EnumWebProfile { Name = question.GetType().Name };
                        profile.EntityValue = question.Image.ToString();
                        break;
                }

                return profile;
            }
        }

        public void SaveJsonToFile<T>(WebProfileCollection<T> collection, string fileName)
            where T : IDataProfile
        {
            var workFolder = "Assets/Resources";
            var path = Path.Combine(workFolder, fileName);

            using (var writer = new StreamWriter(path, false))
            {
                var sb = new StringBuilder();

                sb.Append(JsonUtility.ToJson(collection, false));

                writer.Write(sb);

                writer.Close();
            }
        }

        

        public CharacterWebProfile[] PreserializeCharacter(Character[] units)
        {
            return units
                .Select(item => new CharacterWebProfile()
                {
                    CharacterPositionIndex = item.CharacterPositionIndex,
                    CharacterObjectName = item.CharacterObject.name,
                })
                .ToArray();
        }

        public AnimationUnitWebProfile[] PreserializeAnimationUnit(AnimationUnit[] units)
        {
            return units
                .Select(item => new AnimationUnitWebProfile()
                {
                    CharacterIndex = item.CharacterIndex,
                    PlaybackDelay = item.PlaybackDelay,
                    PlaybackDuration = item.PlaybackDuration,
                    AnimationType = (int)item.AnimationType,
                    AnimationKey = item.AnimationKey,
                    MoveType = (int)item.MoveType,
                    MoveDirection = (int)item.MoveDirection,
                })
                .ToArray();
        }

        public SpeechUnitWebProfile[] PreserializeSpeechUnit(SpeechUnit[] units)
        {
            return units
                .Select(item => new SpeechUnitWebProfile()
                {
                    //URLValue = item.AudioGuid,
                    Subtitles = item.Subtitles,
                    PlaybackDelay = item.PlaybackDelay,
                    AnimationUnits = PreserializeAnimationUnit(item.AnimationUnits),
                })
                .ToArray();
        }

        public FAQUnitWebProfile[] PreserializeFAQUnit(FAQUnit[] units)
        {
            return units
                .Select(item => new FAQUnitWebProfile()
                {
                    Name = item.Name,
                    Text = item.Text,
#if UNITY_EDITOR
                    //ImageName = Path.GetFileName(AssetDatabase.GetAssetPath(item.Image.texture)),
#endif
                })
                .ToArray();
        }

        public PictureUnitWebProfile[] PreserializePictureUnit(PictureUnit[] units)
        {
            return units
                .Select(item => new PictureUnitWebProfile()
                {
#if UNITY_EDITOR
                    //ImageName = Path.GetFileName(AssetDatabase.GetAssetPath(item.Image.texture)),
#endif
                    AppearanceDelay = item.AppearanceDelay,
                })
                .ToArray();
        }

        private string GetAssetFileName(string guid)
        {
#if UNITY_EDITOR
            return Path.GetFileName(AssetDatabase.GUIDToAssetPath(guid));
#else
            return string.Empty;
#endif
        }
    }
}
