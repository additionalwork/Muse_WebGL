#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;

class WebGLBuilder : Editor
{
    static public void Build()
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.WebGL, BuildTarget.WebGL);
        EditorUserBuildSettings.selectedBuildTargetGroup = BuildTargetGroup.WebGL;
        EditorUserBuildSettings.webGLBuildSubtarget = WebGLTextureSubtarget.DXT;

        string[] arguments = Environment.GetCommandLineArgs();
        
        string mainScene = "Assets/Scenes/MainScene.unity";
        string destinationDirectoryPath = arguments[arguments.Length - 3];
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        
        buildPlayerOptions.scenes = new[] { mainScene };
        buildPlayerOptions.locationPathName = destinationDirectoryPath;
        buildPlayerOptions.target = BuildTarget.WebGL;
        buildPlayerOptions.options = BuildOptions.None;
        BuildPipeline.BuildPlayer(buildPlayerOptions);
    }
}
#endif
