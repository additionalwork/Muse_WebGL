namespace Naval.UI
{
    using Naval.Model;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;
    using App = AppControllerSingleton;

    public class QuizWindow : MonoBehaviour
    {
        [SerializeField] private Button nextButton;
        [SerializeField] private GameObject quizIntro;
        [SerializeField] private GameObject quizResults;
        [SerializeField] private GameObject quizQuestion;
        [SerializeField] private TextMeshProUGUI Results;
        [SerializeField] private TextMeshProUGUI questionHeader;
        [SerializeField] private Transform questionSection;
        [SerializeField] private Transform scrollViewContent;
        [SerializeField] private GameObject questionBody;

        [SerializeField] private GameObject choiceButtonPrefab;
        [SerializeField] private GameObject textQuestionPrefab;
        [SerializeField] private GameObject imageQuestionPrefab;

        [SerializeField] private GameObject StartWindowPrefab;

        private List<QuizController.Answer> choiceButtons = new ();
        private QuizController.QuestionData currentQuestion;
        public QuizController _quizController;
        private bool results = false;
        private GridLayoutGroup questionBodyLayoutGroup;
        private RectTransform questionBodyRectTransform;

        private IEnumerator Start()
        {

            _quizController = App.Instance.QuizController;
            nextButton.onClick.AddListener(OnClickNext);
            if (_quizController.CurrentQuestion >= 0 && _quizController.CurrentQuestion < _quizController.ChosenQuestions.Count)
            {
                currentQuestion = _quizController.ChosenQuestions[_quizController.CurrentQuestion];
            }
            SetupPanel();
            questionBodyLayoutGroup = questionBody.GetComponent<GridLayoutGroup>();
            questionBodyRectTransform = questionBody.GetComponent<RectTransform>();
            yield return ResourceManager.Instance.LoadQuiz();
        }

        private void Update()
        {
            UpdateOrientation();
        }

        private void OnDestroy()
        {
            nextButton.onClick.RemoveAllListeners();
        }

        private void UpdateOrientation()
        {
            if (questionBodyRectTransform.rect.width > questionBodyRectTransform.rect.height)
            {
                var cellSize = new Vector2(questionBodyRectTransform.rect.width / 2, questionBodyRectTransform.rect.height);
                questionBodyLayoutGroup.cellSize = cellSize;
            }
            else
            {
                var cellSize = new Vector2(questionBodyRectTransform.rect.width, questionBodyRectTransform.rect.height / 2);
                questionBodyLayoutGroup.cellSize = cellSize;
            }
        }

        private void OnClickNext()
        {
            currentQuestion = _quizController.NextQuestion();
            if (results)
            {
                App.CreateWindow(App.Instance.StartMenuPrefab);
                _quizController.ShuffleQuestions();
                Destroy(this);
            }
            else
            {
                SetupPanel();
            }
        }

        private void SetupPanel()
        {
            quizIntro.SetActive(false);
            quizQuestion.SetActive(false);
            quizResults.SetActive(false);

            if (!_quizController.QuizStarted)
            {
                quizIntro.SetActive(true);
            }
            else if (currentQuestion != null)
            {
                quizQuestion.SetActive(true);
                nextButton.interactable = false;
                SetupQuestion(currentQuestion.Question);
            }
            else
            {
                quizResults.SetActive(true);
                results = true;
                SetupResults();
            }
        }

        private void SetupQuestion(AbstractQuizQuestionType type)
        {
            foreach (Transform obj in scrollViewContent)
            {
                Destroy(obj.gameObject);
            }

            foreach (Transform obj in questionSection)
            {
                Destroy(obj.gameObject);
            }

            switch (type)
            {
                case TextQuestionType question:
                    {
                        var questionObject = Instantiate(textQuestionPrefab, questionSection);
                        var questionComponent = questionObject.GetComponentInChildren<TextQuestion>();
                        questionComponent.TextKey.SetLocalizedStringText(question.Text);
                    }

                    break;

                case ImageQuestionType question:
                    {
                        var questionObject = Instantiate(imageQuestionPrefab, questionSection);
                        var questionComponent = questionObject.GetComponentInChildren<ImageQuestion>();
                        questionComponent.Image.sprite = ResourceManager.Instance.GetResource(question.Image).GetSprite();
                    }

                    break;
            }

            questionHeader.SetLocalizedStringText(type.QuestionTitle);
            
            choiceButtons.Clear();

            for (var i = 0; i < currentQuestion.Answers.Length; ++i)
            {
                var answer = currentQuestion.Answers[i];
                choiceButtons.Add(answer);
                var buttonObject = Instantiate(choiceButtonPrefab, scrollViewContent);
                answer.Button = buttonObject.GetComponent<Button>();
                answer.Button.onClick.AddListener(() => { SelectButton(answer); });
                var choiceButton = buttonObject.GetComponent<ChoiceButton>();
                choiceButton.MainText.SetLocalizedStringText(answer.AnswerKey);
                choiceButton.QuestionNumber = (i + 1).ToString();
            }
        }

        private void SelectButton(QuizController.Answer clickedButton)
        {
            foreach (var button in choiceButtons)
            {
                button.Button.interactable = true;
            }

            nextButton.interactable = true;
            clickedButton.IsSelected = true;
            clickedButton.Button.interactable = false;
        }

        private void SetupResults()
        {
            var correctAnswersCount = _quizController.ChosenQuestions
                .Count(item => item.Answers
                    .Any(item2 => item2.IsSelected && item2.IsCorrectAnswer));
            Results.text = correctAnswersCount + " / " + _quizController.ChosenQuestions.Count;
        }
    }
}
