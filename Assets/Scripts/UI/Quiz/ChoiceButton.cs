namespace Naval.UI
{
    using TMPro;
    using UnityEngine;

    public class ChoiceButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI questionNumber;
        [field: SerializeField] public TextMeshProUGUI MainText { get; set; }
        
        public string QuestionNumber
        {
            get => questionNumber.text;
            set => questionNumber.text = value;
        }
    }
}
