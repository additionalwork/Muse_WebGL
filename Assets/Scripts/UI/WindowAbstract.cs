namespace Naval.UI
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class WindowAbstract : MonoBehaviour
    {
        public virtual void DestroyWindow()
        {
            Destroy(gameObject);
        }
    }
}
