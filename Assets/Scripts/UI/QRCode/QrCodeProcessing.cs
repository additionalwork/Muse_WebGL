namespace Naval.UI
{
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;
    using ZXing;
    
    public class QrCodeProcessing : MonoBehaviour
    {
        [HideInInspector] public UnityEvent<string> Callback;
        [HideInInspector] public bool OnlyCallback;

        [SerializeField] private RawImage display;
        [SerializeField] private AspectRatioFitter aspectRatioFitter;

        public bool IsDecodeStopped = false;

        private Texture2D tempTexture2d;
        private Color32[] tempColor32Set;
        private WebCamDevice[] _devices;
        private WebCamTexture _texture;
        private int _currentCameraIndex = 0;
        
        private Color32[] _cameraColorData;

        private bool _finalWebcamSetup;
        
        // create a reader with a custom luminance source
        private readonly IBarcodeReader _barcodeReader = new BarcodeReader {
            AutoRotate = false,
            Options = new ZXing.Common.DecodingOptions {
                TryHarder = false,
                TryInverted = false,
                PossibleFormats = new[] { BarcodeFormat.QR_CODE },
            }
        };

        private Result _result;

        private IEnumerator Start()
        {
            yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
            if (!Application.HasUserAuthorization(UserAuthorization.WebCam)) yield break;
            
            _devices = WebCamTexture .devices;

            for (var i = 0; i < _devices.Length; ++i)
            {
                if (_devices[i].isFrontFacing) continue;
                _currentCameraIndex = i;
                break;
            }

            StartCamera();
        }

        private IEnumerator ToSetTextureRatio() {
            while (_texture.width < 100 && _texture.height < 100) {
                yield return null;
            }

            SetTextureRatio();
        }

        private void OnDestroy()
        {
            StopCamera();
        }

        private void SetTextureRatio() {
            aspectRatioFitter.aspectRatio = (float)_texture.width / (float)_texture.height;
        }

        private void SwapCamera()
        {
            ++_currentCameraIndex;
            _currentCameraIndex %= _devices.Length;
            StartCoroutine(ToSetTextureRatio());
        }

        private void StartCamera()
        {
            if (WebCamTexture.devices.Length <= 0) return;
            
            var device = WebCamTexture.devices[_currentCameraIndex];
            _texture = new WebCamTexture(device.name);
            StartCoroutine(ToSetTextureRatio());
            display.texture = _texture;

            _texture.Play();
        }
 
        private void StopCamera()
        {
            if (_texture == null) return;
            
            _texture.Stop();
            display.texture = null;
            _texture = null;
        }
        
        private void Update()
        {
            if (!IsDecodeStopped && TryDecode(out var text))
            {
                Callback.Invoke(text);
            }
        }

        private bool TryDecode(out string text)
        {
            text = string.Empty;
            if (_texture is not { isPlaying: true } || _texture.width < 100 && _texture.height < 100) return false;

            if (!_finalWebcamSetup)
            {
                _cameraColorData = new Color32[_texture.width * _texture.height];
                _finalWebcamSetup = !_finalWebcamSetup;
            }

            var scanerWidth = _texture.width / 2;
            var scanerHeight = _texture.height / 2;
            if (tempTexture2d == null)
            {
                tempTexture2d = new Texture2D(scanerWidth, scanerHeight);
            }

            GetReadableTexture2d(_texture, tempTexture2d, scanerWidth, scanerHeight);
            tempColor32Set = tempTexture2d.GetPixels32();

            _result = _barcodeReader.Decode(tempColor32Set, tempTexture2d.width, tempTexture2d.height);

            if (_result == null) return false;

            text = _result.Text;
            if (AppControllerSingleton.Instance.TryGetUrlQuery(_result.Text, out var query))
            {
                text = query;
            }
            return true;
        }

        private void GetReadableTexture2d(Texture sourceTexture, Texture2D destinationTexture, int scanerWidth, int scanerHeight)
        {
            var tmp = RenderTexture.GetTemporary(
                sourceTexture.width,
                sourceTexture.height,
                0,
                RenderTextureFormat.Default,
                RenderTextureReadWrite.Linear
            );
            Graphics.Blit(sourceTexture, tmp);

            var previousRenderTexture = RenderTexture.active;
            RenderTexture.active = tmp;

            destinationTexture.ReadPixels(new Rect(scanerWidth / 2, scanerHeight / 2, sourceTexture.width, sourceTexture.height), 0, 0);
            destinationTexture.Apply();

            RenderTexture.active = previousRenderTexture;
            RenderTexture.ReleaseTemporary(tmp);
        }
    }
}
