namespace Naval.UI
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    
    public class MultiButton : Button
    {
        [Serializable]
        public class GraphicParameters
        {
            public Graphic TargetGraphic;
            public ColorBlock Colors;
        }
        
        [SerializeField] private GraphicParameters[] targetsGraphics;

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            if (!gameObject.activeInHierarchy)
                return;
            
            foreach (var graphic in targetsGraphics)
            {
                Color tintColor;
                Sprite transitionSprite;
                string triggerName;


                switch (state)
                {
                    case SelectionState.Normal:
                        tintColor = graphic.Colors.normalColor;
                        transitionSprite = null;
                        triggerName = animationTriggers.normalTrigger;
                        break;
                    case SelectionState.Highlighted:
                        tintColor = graphic.Colors.highlightedColor;
                        transitionSprite = spriteState.highlightedSprite;
                        triggerName = animationTriggers.highlightedTrigger;
                        break;
                    case SelectionState.Pressed:
                        tintColor = graphic.Colors.pressedColor;
                        transitionSprite = spriteState.pressedSprite;
                        triggerName = animationTriggers.pressedTrigger;
                        break;
                    case SelectionState.Selected:
                        tintColor = graphic.Colors.selectedColor;
                        transitionSprite = spriteState.selectedSprite;
                        triggerName = animationTriggers.selectedTrigger;
                        break;
                    case SelectionState.Disabled:
                        tintColor = graphic.Colors.disabledColor;
                        transitionSprite = spriteState.disabledSprite;
                        triggerName = animationTriggers.disabledTrigger;
                        break;
                    default:
                        tintColor = Color.black;
                        transitionSprite = null;
                        triggerName = string.Empty;
                        break;
                }


                switch (transition)
                {
                    case Transition.ColorTint:
                        StartColorTween(graphic, tintColor * graphic.Colors.colorMultiplier, instant);
                        break;
                    case Transition.SpriteSwap:
                        DoSpriteSwap(graphic.TargetGraphic, transitionSprite);
                        break;
                    case Transition.Animation:
                        TriggerAnimation(triggerName);
                        break;
                    case Transition.None:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        
        void StartColorTween(GraphicParameters graphic, Color targetColor, bool instant)
        {
            graphic.TargetGraphic.CrossFadeColor(targetColor, instant ? 0f : graphic.Colors.fadeDuration, true, true);
        }

        void DoSpriteSwap(Graphic graphic, Sprite newSprite)
        {
            ((Image)graphic).overrideSprite = newSprite;
        }

        void TriggerAnimation(string triggername)
        {
#if PACKAGE_ANIMATION
            if (transition != Transition.Animation || animator == null || !animator.isActiveAndEnabled || !animator.hasBoundPlayables || string.IsNullOrEmpty(triggername))
                return;

            animator.ResetTrigger(m_AnimationTriggers.normalTrigger);
            animator.ResetTrigger(m_AnimationTriggers.highlightedTrigger);
            animator.ResetTrigger(m_AnimationTriggers.pressedTrigger);
            animator.ResetTrigger(m_AnimationTriggers.selectedTrigger);
            animator.ResetTrigger(m_AnimationTriggers.disabledTrigger);

            animator.SetTrigger(triggername);
#endif
        }
    }
}
