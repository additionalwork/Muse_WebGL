namespace Naval.UI
{
    using TMPro;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;
    using static Naval.UI.StartMenu;
    using App = AppControllerSingleton;

    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private ButtonLocalizedKeyPair DefaultExcursion;
        [SerializeField] private ButtonLocalizedKeyPair InteractiveExcursion;
        [SerializeField] private ButtonLocalizedKeyPair Quiz;
        [SerializeField] private ButtonLocalizedKeyPair Morse;
        [SerializeField] private ButtonLocalizedKeyPair Scheme;
        [SerializeField] private ButtonLocalizedKeyPair Settings;
        [SerializeField] private ButtonLocalizedKeyPair QR;

        [SerializeField] private Button PrevButton;
        [SerializeField] private Button NextButton;
        [SerializeField] private GameObject DefaultExcursionIndicator;
        [SerializeField] private GameObject InteractiveExcursionIndicator;
        [SerializeField] private GameObject QuizIndicator;
        [SerializeField] private GameObject MorseIndicator;
        [SerializeField] private GameObject SchemeIndicator;
        [SerializeField] private ScrollRect MainScroll;
        [SerializeField] private TextMeshProUGUI DescriptionText;
        [SerializeField] private float MainScrollAdjustmentSpeed = 2;

        private Coroutine ScrollToItemCoroutine;

        private int CurrentItem;
        private ButtonLocalizedKeyPair[] Pairs;

        private int ButtonsCount => Pairs.Length;
        private bool hasTouchLastFrame;

        private void Start()
        {
            Pairs = new ButtonLocalizedKeyPair[] { DefaultExcursion, InteractiveExcursion, Quiz, Morse, Scheme, Settings, QR };

            NextButton.onClick.AddListener(() => UpdateCurrentItem(CurrentItem + 1));
            PrevButton.onClick.AddListener(() => UpdateCurrentItem(CurrentItem - 1));
            MainScroll.onValueChanged.AddListener(ScrollValueChanged);


            App.Instance.ExcursionDefaultController.ExcursionDefaultRepository.DataLoadedEvent += 
                ToggleLoadIndicatorOffDefaultExcursion;

            App.Instance.ExcursionInteractiveController.ExcursionInteractiveRepository.DataLoadedEvent += 
                ToggleLoadIndicatorOffDefaultInteractiveExcursion;

            App.Instance.QuizController.QuizRepository.DataLoadedEvent += 
                ToggleLoadIndicatorOffQuiz;

            App.Instance.MorseController.MorseRepository.DataLoadedEvent += ToggleLoadIndicatorOffMorse;

            App.Instance.ExcursionDefaultController.ExcursionDefaultRepository.DataLoadedEvent += 
                ToggleLoadIndicatorOffScheme;

            var isLoaded = App.Instance.ExcursionDefaultController.ExcursionDefaultRepository.IsDataLoaded;
            ToggleLoadIndicator(DefaultExcursion.Button, DefaultExcursionIndicator, !isLoaded);
            ToggleLoadIndicator(Scheme.Button, SchemeIndicator, !isLoaded);

            isLoaded = App.Instance.ExcursionInteractiveController.ExcursionInteractiveRepository.IsDataLoaded;
            ToggleLoadIndicator(InteractiveExcursion.Button, InteractiveExcursionIndicator, !isLoaded);

            isLoaded = App.Instance.QuizController.QuizRepository.IsDataLoaded;
            ToggleLoadIndicator(Quiz.Button, QuizIndicator, !isLoaded);

            isLoaded = App.Instance.MorseController.MorseRepository.IsDataLoaded;
            ToggleLoadIndicator(Morse.Button, MorseIndicator, !isLoaded);

            DefaultExcursion.Button.onClick.AddListener(() => App.StartNewExcursionDefault());
            InteractiveExcursion.Button.onClick.AddListener(() => App.StartNewExcursionInteractive());
            Quiz.Button.onClick.AddListener(() => App.CreateWindow(App.Instance.QuizPrefab));
            Morse.Button.onClick.AddListener(() => App.CreateWindow(App.Instance.MorsePrefab));
            Scheme.Button.onClick.AddListener(() => App.CreateWindow(App.Instance.SchemePrefab));
            Settings.Button.onClick.AddListener(() => App.CreateWindow(App.Instance.SettingsPrefab));
            QR.Button.onClick.AddListener(() => App.CreateWindow(App.Instance.QRPrefab));
        }

        private void Update()
        {
            var hasTouch = Input.touchCount > 0;
            if (Input.GetMouseButtonDown(0) || hasTouch)
            {
                if (ScrollToItemCoroutine != null)
                {
                    StopCoroutine(ScrollToItemCoroutine);
                    return;
                }
            }

            if (Input.GetMouseButtonUp(0) || (!hasTouch && hasTouchLastFrame))
            {
                ScrollToItemCoroutine = StartCoroutine(MainScroll.FocusAtPointCoroutine(new Vector2(1.0f * CurrentItem / (ButtonsCount - 1), 1), MainScrollAdjustmentSpeed));
            }

            hasTouchLastFrame = hasTouch;
        }

        private void OnDestroy()
        {
            DefaultExcursion.Button.onClick.RemoveAllListeners();
            InteractiveExcursion.Button.onClick.RemoveAllListeners();
            Quiz.Button.onClick.RemoveAllListeners();
            Morse.Button.onClick.RemoveAllListeners();
            Scheme.Button.onClick.RemoveAllListeners();
            Settings.Button.onClick.RemoveAllListeners();
            QR.Button.onClick.RemoveAllListeners();

            if (App.Instance != null)
            {
                App.Instance.ExcursionDefaultController.ExcursionDefaultRepository.DataLoadedEvent -=
                ToggleLoadIndicatorOffDefaultExcursion;
                App.Instance.ExcursionInteractiveController.ExcursionInteractiveRepository.DataLoadedEvent -=
                    ToggleLoadIndicatorOffDefaultInteractiveExcursion;
                App.Instance.QuizController.QuizRepository.DataLoadedEvent -=
                    ToggleLoadIndicatorOffQuiz;
                App.Instance.ExcursionDefaultController.ExcursionDefaultRepository.DataLoadedEvent -=
                    ToggleLoadIndicatorOffScheme;
            }
        }

        private void ToggleLoadIndicatorOffDefaultExcursion()
        {
            ToggleLoadIndicator(DefaultExcursion.Button, DefaultExcursionIndicator, false);
        }

        private void ToggleLoadIndicatorOffDefaultInteractiveExcursion()
        {
            ToggleLoadIndicator(InteractiveExcursion.Button, InteractiveExcursionIndicator, false);
        }

        private void ToggleLoadIndicatorOffQuiz()
        {
            ToggleLoadIndicator(Quiz.Button, QuizIndicator, false);
        }

        private void ToggleLoadIndicatorOffMorse()
        {
            ToggleLoadIndicator(Morse.Button, MorseIndicator, false);
        }

        private void ToggleLoadIndicatorOffScheme()
        {
            ToggleLoadIndicator(Scheme.Button, SchemeIndicator, false);
        }

        private void ToggleLoadIndicator(Button button, GameObject indicator, bool isEnable)
        {
            indicator.SetActive(isEnable);
            EnableButton(button, !isEnable);
        }

        private void EnableButton(Button button, bool isEnable)
        {
            if (!isEnable || AuthManager.Authorized || 
                button != DefaultExcursion.Button && button != InteractiveExcursion.Button && button != Scheme.Button)
            {
                button.interactable = isEnable;
            }
        }

        public void Authorize()
        {
            if (App.Instance.ExcursionDefaultController.ExcursionDefaultRepository.IsDataLoaded)
            {
                ToggleLoadIndicatorOffDefaultExcursion();
                ToggleLoadIndicatorOffScheme();
            }

            if (App.Instance.ExcursionInteractiveController.ExcursionInteractiveRepository.IsDataLoaded)
            {
                ToggleLoadIndicatorOffDefaultInteractiveExcursion();
            }            
        }

        public void Logoff()
        {
            EnableButton(DefaultExcursion.Button, false);
            EnableButton(InteractiveExcursion.Button, false);
            EnableButton(Scheme.Button, false);
        }

        public GameObject CreateWindow(GameObject windowPrefab)
        {
            if (windowPrefab == null) return null;
            
            return App.CreateWindow(windowPrefab);
        }

        private void ScrollValueChanged(Vector2 vector)
        {
            var num = Mathf.RoundToInt(vector.x * (ButtonsCount - 1));

            if (Input.GetMouseButton(0) || Input.touchCount > 0)
            {
                UpdateCurrentItem(num);
            }

            if (num <= 0)
            {
                PrevButton.gameObject.SetActive(false);
            }
            else
            {
                PrevButton.gameObject.SetActive(true);
            }

            if (num >= ButtonsCount - 1)
            {
                NextButton.gameObject.SetActive(false);
            }
            else
            {
                NextButton.gameObject.SetActive(true);
            }

            DescriptionText.SetLocalizedString(Pairs[CurrentItem].DescriptionKey);
        }

        private void UpdateCurrentItem(int num)
        {
            if (num < 0) num = 0;
            if (num >= ButtonsCount) num = ButtonsCount - 1;

            CurrentItem = num;
        }
    }
}
