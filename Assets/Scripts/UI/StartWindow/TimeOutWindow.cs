namespace Naval.UI
{
    using UnityEngine.UI;
    using UnityEngine;
    
    public class TimeOutWindow : MonoBehaviour
    {
        [SerializeField] private Button okButton;
        
        private void Start()
        {
            okButton.onClick.AddListener(() => AppControllerSingleton.CreateWindow(AppControllerSingleton.Instance.StartMenuPrefab));
        }

        private void OnDestroy()
        {
            okButton.onClick.RemoveAllListeners();
        }
    }
}