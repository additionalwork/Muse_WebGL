namespace Naval.UI
{
    using System;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;
    using App = AppControllerSingleton;

    public class StartMenu : MonoBehaviour
    {
        [SerializeField] private ButtonLocalizedKeyPair MainMenu;
        [SerializeField] private ButtonLocalizedKeyPair Quiz;
        [SerializeField] private ButtonLocalizedKeyPair Morse;
        [SerializeField] private ButtonLocalizedKeyPair Settings;

        [SerializeField] private Button PrevButton;
        [SerializeField] private Button NextButton;
        [SerializeField] private GameObject QuizIndicator;
        [SerializeField] private GameObject MorseIndicator;
        [SerializeField] private ScrollRect MainScroll;
        [SerializeField] private TextMeshProUGUI DescriptionText;
        [SerializeField] private float MainScrollAdjustmentSpeed = 2;

        private Coroutine ScrollToItemCoroutine;

        private int CurrentItem;
        private ButtonLocalizedKeyPair[] Pairs;

        private int ButtonsCount => Pairs.Length;
        private bool hasTouchLastFrame;

        private void Start()
        {
            AppControllerSingleton.Instance.ExcursionDefaultController.ExcursionDefaultRepository.DataLoadedEvent += ToggleLoadIndicatorOff;

            if (!App.Instance.QuizController.QuizRepository.IsDataLoaded)
            {
                ToggleLoadIndicator(true);
            }

            // Configure order here
            Pairs = new ButtonLocalizedKeyPair[4] { MainMenu, Quiz, Morse, Settings };

            MainMenu.Button.onClick.AddListener(() => App.CreateWindow(App.Instance.QRPrefab));
            Quiz.Button.onClick.AddListener(() => App.CreateWindow(App.Instance.QuizPrefab));
            Settings.Button.onClick.AddListener(GoToSettings);
            Morse.Button.onClick.AddListener(() => App.CreateWindow(App.Instance.MorsePrefab));

            NextButton.onClick.AddListener(() => UpdateCurrentItem(CurrentItem + 1));
            PrevButton.onClick.AddListener(() => UpdateCurrentItem(CurrentItem - 1));
            MainScroll.onValueChanged.AddListener(ScrollValueChanged);
        }

        private void OnDestroy()
        {
            MainMenu.Button.onClick.RemoveAllListeners();
            Quiz.Button.onClick.RemoveAllListeners();
            Morse.Button.onClick.RemoveAllListeners();
            Settings.Button.onClick.RemoveAllListeners();

            NextButton.onClick.RemoveAllListeners();
            PrevButton.onClick.RemoveAllListeners();
            MainScroll.onValueChanged.RemoveAllListeners();

            if (AppControllerSingleton.Instance != null)
            {
                AppControllerSingleton.Instance.ExcursionDefaultController.ExcursionDefaultRepository.DataLoadedEvent -= ToggleLoadIndicatorOff;
            }
        }

        private void Update()
        {
            var hasTouch = Input.touchCount > 0;
            if (Input.GetMouseButtonDown(0) || hasTouch)
            {
                if (ScrollToItemCoroutine != null)
                {
                    StopCoroutine(ScrollToItemCoroutine);
                    return;
                }
            }

            if (Input.GetMouseButtonUp(0) || (!hasTouch && hasTouchLastFrame))
            {
                ScrollToItemCoroutine = StartCoroutine(MainScroll.FocusAtPointCoroutine(new Vector2(1.0f * CurrentItem / (ButtonsCount - 1), 1), MainScrollAdjustmentSpeed));
            }

            hasTouchLastFrame = hasTouch;
        }

        private void ScrollValueChanged(Vector2 vector)
        {
            var num = Mathf.RoundToInt(vector.x * (ButtonsCount - 1));

            if (Input.GetMouseButton(0) || Input.touchCount > 0)
            {
                UpdateCurrentItem(num);
            }

            if (num <= 0)
            {
                PrevButton.gameObject.SetActive(false);
            }
            else
            {
                PrevButton.gameObject.SetActive(true);
            }

            if (num >= ButtonsCount - 1)
            {
                NextButton.gameObject.SetActive(false);
            }
            else
            {
                NextButton.gameObject.SetActive(true);
            }

            DescriptionText.SetLocalizedString(Pairs[CurrentItem].DescriptionKey);
        }

        private void UpdateCurrentItem(int num)
        {
            if (num < 0) num = 0;
            if (num >= ButtonsCount) num = ButtonsCount - 1;

            CurrentItem = num;
        }

        private void ToggleLoadIndicatorOff()
        {
            ToggleLoadIndicator(false);
        }

        private void ToggleLoadIndicator(bool isEnable)
        {
            Quiz.Button.interactable = !isEnable;
            QuizIndicator.SetActive(isEnable);

            Morse.Button.interactable = !isEnable;
            MorseIndicator.SetActive(isEnable);
        }

        private void GoToSettings()
        {
            var pref = App.CreateWindow(App.Instance.SettingsPrefab);
            if (pref.TryGetComponent<SettingsWindow>(out var settings))
            {
                settings.FromStartWindow = true;
            }
        }

        [Serializable]
        public class ButtonLocalizedKeyPair
        {
            public Button Button;
            public string DescriptionKey;
        }
    }
}
