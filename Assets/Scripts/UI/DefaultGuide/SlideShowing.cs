namespace Naval.UI
{
    using UnityEngine;
    using System.Collections;
    using UnityEngine.UI;

    public class SlideShowing : MonoBehaviour
    {
        private Image _image;
        private AspectRatioFitter _aspectRatioFitter;
        [SerializeField] private float timeToFade = 0.5f;
        public Coroutine showingRoutine;

        private void Awake()
        {
            transform.SetParent(AppControllerSingleton.Instance.BackgroundCanvas, false);
            _image = GetComponent<Image>();
            _aspectRatioFitter = GetComponent<AspectRatioFitter>();
        }

        public void ShowSlide(Sprite sprite, float showTime)
        {
            if (showingRoutine != null)
            {
                StopCoroutine(showingRoutine);
            }
            
            gameObject.SetActive(true);
            _image.color = SetAlphaForColor(_image.color, 0f);
            _image.sprite = sprite;
            if (sprite != null)
            {
                _aspectRatioFitter.aspectRatio = sprite.rect.width / sprite.rect.height;
            }
            showingRoutine = StartCoroutine(ToShowSlide(showTime));
        }

        public void HideSlide()
        {
            if (showingRoutine != null)
            {
                StopCoroutine(showingRoutine);
            }

            showingRoutine = StartCoroutine(ToHideSlide());
        }

        private Color SetAlphaForColor(Color color, float alpha)
        {
            color.a = alpha;
            return color;
        }

        private Color IterateAlphaForColor(Color color, float iterateAlpha)
        {
            color.a += iterateAlpha;
            return color;
        }

        private IEnumerator ToShowSlide(float showTime)
        {
            do
            {
                _image.color = IterateAlphaForColor(_image.color, (Time.deltaTime / timeToFade));
                yield return new WaitForSeconds(Time.deltaTime);
            }
            while (_image.color.a < 1f);

            _image.color = SetAlphaForColor(_image.color, 1f);
            yield return new WaitForSeconds(showTime - timeToFade);
            yield return ToHideSlide();
        }

        private IEnumerator ToHideSlide()
        {
            while (_image.color.a > 0f)
            {
                _image.color = IterateAlphaForColor(_image.color, -(Time.deltaTime / timeToFade));
                yield return new WaitForSeconds(Time.deltaTime);
            }

            _image.color = SetAlphaForColor(_image.color, 0f);
            gameObject.SetActive(false);
        }
    }
}
