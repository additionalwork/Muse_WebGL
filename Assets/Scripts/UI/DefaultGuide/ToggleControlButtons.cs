namespace Naval
{
    using System.Collections;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class ToggleControlButtons : MonoBehaviour
    {
        [SerializeField] private GameObject ButtonsPanel;
        [SerializeField] private float idleToHideButtonsTime;
        [SerializeField] private float fadeTime;
        [SerializeField] private float maxBackgroundAlpha;
        [SerializeField] private Image[] togglableImages;
        [SerializeField] private Image backgroundImage;

        private float timer;
        private Coroutine showRoutine;

        public bool isEnabled;

        private void Start()
        {
            isEnabled = true;
        }

        private void Update()
        {
            if (!isEnabled)
            {
                return;
            }

            if (Input.anyKeyDown)
            {
                if (EventSystem.current.currentSelectedGameObject == null && ButtonsPanel.activeSelf)
                {
                    if (showRoutine != null)
                    {
                        StopCoroutine(showRoutine);
                    }

                    showRoutine = StartCoroutine(ToHideButtons());

                    return;
                }

                timer = 0;

                if (EventSystem.current.currentSelectedGameObject == null && !ButtonsPanel.activeSelf)
                {
                    if (showRoutine != null)
                    {
                        StopCoroutine(showRoutine);
                    }

                    showRoutine = StartCoroutine(ToShowButtons());
                }
            }

            if (ButtonsPanel.activeSelf)
            {
                timer += Time.deltaTime;

                if (timer > idleToHideButtonsTime)
                {
                    timer = 0;

                    if (showRoutine != null)
                    {
                        StopCoroutine(showRoutine);
                    }

                    showRoutine = StartCoroutine(ToHideButtons());
                }
            }
        }

        public void DisableButtons()
        {
            HideButtonsInstant();
            isEnabled = false;
        }

        private void HideButtonsInstant()
        {
            for (int i = 0; i < togglableImages.Length; i++)
            {
                togglableImages[i].color = SetAlphaForColor(togglableImages[i].color, 0);
            }

            backgroundImage.color = SetAlphaForColor(backgroundImage.color, 0);

            ButtonsPanel.SetActive(false);
        }

        public void EnableButtons()
        {
            isEnabled = true;
        }

        private IEnumerator ToShowButtons()
        {
            ButtonsPanel.SetActive(true);

            var startTime = Time.time;
            do
            {
                for (int i = 0; i < togglableImages.Length; i++)
                {
                    togglableImages[i].color = IterateAlphaForColor(togglableImages[i].color, (Time.deltaTime / fadeTime));
                }

                backgroundImage.color = IterateAlphaForColor(backgroundImage.color, (Time.deltaTime / fadeTime) * maxBackgroundAlpha);

                yield return new WaitForSeconds(Time.deltaTime);
            }
            while (Time.time < startTime + fadeTime);

            for (int i = 0; i < togglableImages.Length; i++)
            {
                togglableImages[i].color = SetAlphaForColor(togglableImages[i].color, 1);
            }

            backgroundImage.color = SetAlphaForColor(backgroundImage.color, maxBackgroundAlpha);
        }

        private IEnumerator ToHideButtons()
        {
            var startTime = Time.time;
            do
            {
                for (int i = 0; i < togglableImages.Length; i++)
                {
                    togglableImages[i].color = IterateAlphaForColor(togglableImages[i].color, -(Time.deltaTime / fadeTime));
                }

                backgroundImage.color = IterateAlphaForColor(backgroundImage.color, -(Time.deltaTime / fadeTime) * maxBackgroundAlpha);

                yield return new WaitForSeconds(Time.deltaTime);
            }
            while (Time.time < startTime + fadeTime);

            for (int i = 0; i < togglableImages.Length; i++)
            {
                togglableImages[i].color = SetAlphaForColor(togglableImages[i].color, 0);
            }

            backgroundImage.color = SetAlphaForColor(backgroundImage.color, 0);

            ButtonsPanel.SetActive(false);
        }

        private Color SetAlphaForColor(Color color, float alpha)
        {
            color.a = alpha;
            return color;
        }

        private Color IterateAlphaForColor(Color color, float iterateAlpha)
        {
            color.a += iterateAlpha;
            return color;
        }
    }
}
