namespace Naval.UI
{
    using UnityEngine;
    using System.Collections;
    using UnityEngine.UI;
    using TMPro;

    public class SubtitleShowing : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private Image background;
        [SerializeField] private float timeToFade = 0.5f;
        [SerializeField] private float timeScrollDeadZone = 2f;
        [SerializeField] private float backgroundTransparency = 0.6f;
        private Coroutine showingRoutine;
        private Coroutine scrollingRoutine;
        private Vector2 defaultAnchoredTextPosition;

        public void ShowSubtitle(string subtitle, float showTime)
        {
            if (showingRoutine != null)
            {
                StopCoroutine(showingRoutine);
            }

            if (scrollingRoutine != null)
            {
                StopCoroutine(scrollingRoutine);
                text.GetComponent<RectTransform>().anchoredPosition = defaultAnchoredTextPosition;
            }

            gameObject.SetActive(true);
            background.color = SetAlphaForColor(background.color, 0f);
            text.SetLocalizedStringText(subtitle);
            text.color = SetAlphaForColor(text.color, 0f);
            defaultAnchoredTextPosition = text.GetComponent<RectTransform>().anchoredPosition;
            showingRoutine = StartCoroutine(ToShowSubtitle(showTime));
            scrollingRoutine = StartCoroutine(ToScrollText(showTime));
        }

        public void HideSubtitle()
        {
            if (showingRoutine != null)
            {
                StopCoroutine(showingRoutine);
            }

            if (scrollingRoutine != null)
            {
                StopCoroutine(scrollingRoutine);
                text.GetComponent<RectTransform>().anchoredPosition = defaultAnchoredTextPosition;
            }

            showingRoutine = StartCoroutine(ToHideSubtitle());
        }

        private Color SetAlphaForColor(Color color, float alpha)
        {
            color.a = alpha;
            return color;
        }

        private Color IterateAlphaForColor(Color color, float iterateAlpha)
        {
            color.a += iterateAlpha;
            return color;
        }

        private IEnumerator ToScrollText(float duration)
        {
            var currentDeadZone = timeScrollDeadZone;

            yield return new WaitForSeconds(currentDeadZone);

            var backgroundRect = background.GetComponent<RectTransform>();
            var textRect = text.GetComponent<RectTransform>();
            var motionDistance = Mathf.Max(textRect.rect.height - textRect.anchoredPosition.y * 2 - backgroundRect.rect.height, 0f);
            var motionDuration = (duration - currentDeadZone * 2);
            var motionFactor = motionDistance / motionDuration;

            if (duration < timeScrollDeadZone * 2)
            {
                currentDeadZone = duration / 2;
            }

            while (motionDistance > 0f)
            {
                var motionUnit = Time.deltaTime * motionFactor;
                motionDistance -= motionUnit;
                textRect.anchoredPosition += new Vector2(0f, motionUnit);

                yield return new WaitForEndOfFrame();
            }

            yield return null;
        }

        private IEnumerator ToShowSubtitle(float showTime)
        {
            do
            {
                background.color = IterateAlphaForColor(background.color, (Time.deltaTime / timeToFade));
                text.color = IterateAlphaForColor(text.color, (Time.deltaTime / timeToFade));
                yield return new WaitForSeconds(Time.deltaTime);
            }
            while (background.color.a < backgroundTransparency);

            background.color = SetAlphaForColor(background.color, backgroundTransparency);
            text.color = SetAlphaForColor(text.color, 1f);
            yield return new WaitForSeconds(showTime - timeToFade);
            yield return ToHideSubtitle();
        }

        private IEnumerator ToHideSubtitle()
        {
            while (background.color.a > 0f)
            {
                background.color = IterateAlphaForColor(background.color, -(Time.deltaTime / timeToFade));
                text.color = IterateAlphaForColor(text.color, -(Time.deltaTime / timeToFade));
                yield return new WaitForSeconds(Time.deltaTime);
            }

            background.color = SetAlphaForColor(background.color, 0f);
            text.color = SetAlphaForColor(text.color, 0f);
            gameObject.SetActive(false);
        }
    }
}
