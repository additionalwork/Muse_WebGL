namespace Naval.UI
{
    using UnityEngine;
    using System.Collections;
    using System;

    public class GuideManShowing : MonoBehaviour
    {
        private static readonly int Surface = Shader.PropertyToID("_Surface");

        [SerializeField] private float alphaChanelChangeStep;
        [SerializeField] private float alphaTransitionStepDuration;

        private Renderer[] _childrenRenderers;

        private void Start()
        {
            _childrenRenderers = GetComponentsInChildren<Renderer>();

            StartCoroutine(MaterialTransition(val => val));
        }

        private IEnumerator MaterialTransition(Func<float, float> indexComputation)
        {
            SetFloatAll(Surface, 1);

            for (var i = 0f; i < 1f; i += alphaChanelChangeStep)
            {
                SetAllAlpha(indexComputation(i));
                yield return new WaitForSeconds(alphaTransitionStepDuration);

            }

            SetFloatAll(Surface, 0);
        }

        /// <summary>
        /// Call this method instead of Destroy(gameObject).
        /// It will increase transparency and only after gameObject is transparent, destroy it
        /// </summary>
        public void Destroy()
        {
            StartCoroutine(MaterialTransition(val => 1f - val));
            Destroy(this, alphaTransitionStepDuration * (1f / alphaChanelChangeStep));
        }

        private void SetFloatAll(int nameID, float val)
        {
            foreach (var childRenderer in _childrenRenderers)
            {
                foreach (var material in childRenderer.materials)
                {
                    material.SetFloat(nameID, val);
                }
            }
        }

        private void SetAllAlpha(float val)
        {
            foreach (var childRenderer in _childrenRenderers)
            {
                foreach (var material in childRenderer.materials)
                {
                    SetAlpha(material, val);
                }
            }
        }

        private static void SetAlpha(Material mat, float alphaVal)
        {
            Color oldColor = mat.color;
            Color newColor = new Color(oldColor.r, oldColor.g, oldColor.b, alphaVal);
            mat.color = newColor;
        }
    }
}
