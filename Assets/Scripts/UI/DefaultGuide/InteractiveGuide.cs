namespace Naval.UI
{
    using UnityEngine;
    using System.Collections;
    using Naval.Model;
    using System.Linq;

    public class InteractiveGuide : Guide
    {
        [SerializeField] private TasksShowing tasks;

        public string taskAnswer;
        public bool toSkipTask;

        private void HideTasks()
        {
            if (tasks.gameObject.activeSelf)
            {
                tasks.HidePanels();
            }
        }

        private new void ResetShowings()
        {
            base.ResetShowings();

            HideTasks();
        }

        public void PlayTask(ExhibitDefault exhibit, int taskIndex)
        {
            StopExcursionRoutine();
            excursionRoutine = StartCoroutine(ToPlayTask((ExhibitInteractive)exhibit, taskIndex));

            var interRepo = AppControllerSingleton.Instance.ExcursionInteractiveController.ExcursionInteractiveRepository;
            var rootExhibit = ((AbstractExcursionRepository)interRepo).Root;

            excursionRoutine = StartCoroutine(ToPlayBoatExcursion(rootExhibit));
        }

        public void SetAnswer(string answer)
        {
            taskAnswer = answer;
        }

        public void SkipTask()
        {
            toSkipTask = true;
        }

        private protected override IEnumerator ToPlayBoatExcursion(ExhibitDefault exhibit)
        {
            if (!exhibit.IsFinished)
            {
                yield return ToPlayExhibit(exhibit);
            }

            if (exhibit.NextExhibit != null)
            {
                yield return ToPlayHallExcursion(exhibit.NextExhibit, false);
            }

            var interRepo = AppControllerSingleton.Instance.ExcursionInteractiveController.ExcursionInteractiveRepository;
            var rootExhibit = ((AbstractExcursionRepository)interRepo).Root;

            ShowEndPanel(rootExhibit);
        }

        protected private override IEnumerator ToPlayHallExcursion(ExhibitDefault exhibit, bool playFinished)
        {
            if (exhibit == null)
            {
                yield break;
            }

            CurrentHall = exhibit;

            if (!exhibit.IsFinished || exhibit.ChildExhibits.Any(child => !child.IsFinished))
            {
                yield return ToPlayExhibit(exhibit);
            }

            yield return ToGiveTasks((ExhibitInteractive)exhibit);

            if (exhibit.ChildExhibits == null || exhibit.ChildExhibits.Length == 0)
            {
                yield break;
            }

            var nextExhibit = exhibit.ChildExhibits[exhibit.ChildExhibits.Length - 1].NextExhibit;

            if (exhibit.NextExhibit != null)
            {
                if (exhibit.IsHall)
                {
                    yield return ToPlayHallExcursion(nextExhibit, false);
                }
                else
                {
                    yield return ToPlayExhibit(nextExhibit);
                }
            }
        }

        private protected override IEnumerator ToPlayExhibit(ExhibitDefault exhibit)
        {
            if (!exhibit.IsFinished)
            {
                yield return LoadAndPreload(exhibit);

                PrepareCharacters(exhibit);

                CurrentExhibit = exhibit;
                AppControllerSingleton.Instance.ExcursionDefaultController.CurrentExhibit = exhibit;

                ExhibitNameText.SetLocalizedStringText(exhibit.Name);

                slideRoutine = StartCoroutine(ToShowSlides(exhibit));

                foreach (var speech in exhibit.SpeechUnits)
                {
                    speechRoutine = StartCoroutine(ToPlaySpeech(exhibit, speech));
                    while (speechRoutine != null)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                }

                exhibit.IsFinished = true;

                ResetShowings();
            }

            if (!exhibit.IsRoot && !exhibit.IsHall)
            {
                yield return ToGiveTasks((ExhibitInteractive)exhibit);
            }

            if (exhibit.NextExhibit != null && !exhibit.NextExhibit.IsHall)
            {
                yield return ToPlayExhibit(exhibit.NextExhibit);
            }
        }

        private IEnumerator ToGiveTasks(ExhibitInteractive exhibit)
        {
            for (int i = 0; i < exhibit.TaskUnits.Length; i++)
            {
                if (!exhibit.TaskUnits[i].IsFinished)
                {
                    yield return ToPlayTask(exhibit, i);
                }
            }
        }
        
        private IEnumerator ToPlayTask(ExhibitInteractive exhibit, int taskIndex)
        {
            if (taskIndex > 0 && !exhibit.TaskUnits[taskIndex - 1].IsFinished && !exhibit.TaskUnits[taskIndex - 1].IsSkipped)
            {
                yield break;
            }

            ResetShowings();
            toggleButtons.DisableButtons();

            var task = exhibit.TaskUnits[taskIndex];
            taskAnswer = string.Empty;
            toSkipTask = false;

            tasks.StartTask(exhibit, taskIndex);

            var timer = 0f;
            var hintIndex = 0;

            while (taskAnswer != task.CorrectAnswer && !toSkipTask)
            {
                timer += Time.deltaTime;

                if (taskAnswer != string.Empty)
                {
                    timer = 61f;
                    taskAnswer = string.Empty;

                    // there's no extra try or hint for TaskType.Select
                    if (task.Type == TaskType.Select)
                    {
                        break;
                    }
                    else
                    {
                        yield return ToReactTaskFailed();
                    }
                }

                if (timer > 60f && hintIndex < task.Hints.Length)
                {
                    yield return ToPlayHint(exhibit, taskIndex, hintIndex);

                    timer = 0;
                    hintIndex++;
                }

                yield return new WaitForEndOfFrame();
            }

            if (toSkipTask)
            {
                exhibit.TaskUnits[taskIndex].IsSkipped = true;

                yield return ToReactTaskSkipped();

                ResetShowings();
                yield break;
            }

            exhibit.TaskUnits[taskIndex].IsFinished = true;
            exhibit.TaskUnits[taskIndex].IsCorrect = taskAnswer == task.CorrectAnswer;

            if (task.Type == TaskType.Select && taskAnswer != task.CorrectAnswer)
            {
                yield return ToReactTaskFailed();
            }

            if (taskAnswer == task.CorrectAnswer)
            {
                yield return ToReactTaskCompleted();
            }

            ResetShowings();
        }

        private IEnumerator ToReactTaskCompleted()
        {
#if DEBUG
            Debug.Log("COMPLETED");
#endif
            yield return new WaitForSeconds(1.5f);
        }

        private IEnumerator ToReactTaskSkipped()
        {
#if DEBUG
            Debug.Log("SKIPPED");
#endif
            yield return new WaitForEndOfFrame();
        }

        private IEnumerator ToReactTaskFailed()
        {
#if DEBUG
            Debug.Log("FAILED");
#endif
            yield return new WaitForSeconds(1.5f);
        }

        private IEnumerator ToPlayHint(ExhibitInteractive exhibit, int taskIndex, int hintIndex)
        {
            foreach (var speech in exhibit.TaskUnits[taskIndex].Hints[hintIndex].SpeechUnits)
            {
                speechRoutine = StartCoroutine(ToPlaySpeech(exhibit, speech));
                while (speechRoutine != null && taskAnswer != exhibit.TaskUnits[taskIndex].CorrectAnswer)
                {
                    yield return new WaitForEndOfFrame();
                }
            }

            ResetSpeech();
        }

        public new void ShowEndPanel(ExhibitDefault root)
        {
            ResetShowings();

            menu.ShowEndWindow(root, true);
        }
    }
}
