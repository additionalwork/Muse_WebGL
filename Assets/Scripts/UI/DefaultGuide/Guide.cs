namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using System.Collections;
    using System.Linq;
    using TMPro;
    using Naval.Model;
    using System.Collections.Generic;

    public class Guide : MonoBehaviour
    {
        [SerializeField] private float[] speeds;
        [SerializeField] private Button speedButton;
        [SerializeField] private Button pauseButton;
        [SerializeField] private Button skipButton;
        [SerializeField] private Button previousButton;
        [SerializeField] private Button muteButton;
        [SerializeField] private Button settingsButton;

        [SerializeField] private Image playImage;
        [SerializeField] public Sprite playSprite;
        [SerializeField] private Sprite stopSprite;

        [SerializeField] private Image muteImage;
        [SerializeField] public Sprite muteSprite;
        [SerializeField] private Sprite unmuteSprite;

        [SerializeField] private protected TextMeshProUGUI ExhibitNameText;

        [SerializeField] private SubtitleShowing subtitles;
        [SerializeField] public SlideShowing slides;

        [SerializeField] public GameObject loadingIndicator;

        [SerializeField] public Transform[] CharacterPositions = new Transform[5];
        [SerializeField] public Dictionary<int, Character> CharactersDict = new();

        protected private GuideMenu menu;
        public ToggleControlButtons toggleButtons;
        private AudioPlayer audioPlayer;
        private AudioSource audioSource;

        public int currentSpeedIndex = 0;
        private float currentPictureTimer = 0f;
        private bool isPaused = false;
        private bool isInMenu = false;

        public Coroutine excursionRoutine;
        public Coroutine speechRoutine;
        public Coroutine slideRoutine;
        private List<Coroutine> animationRoutines = new();

        public ExhibitDefault HeadExhibit;
        public ExhibitDefault CurrentExhibit;
        public ExhibitDefault CurrentHall;
        public bool IsInitiated = false;

        private const float defaultSpeechDuration = 10f;

        private static readonly int IsTalking = Animator.StringToHash("bTalk");


        private void Awake()
        {
            GetComponent<BaseWindowBehaviour>().CornerButton.onClick.AddListener(() => 
            {
                if (!isPaused)
                {
                    TogglePauseSpeech(); 
                    isInMenu = true;
                }
            });

            toggleButtons = GetComponent<ToggleControlButtons>();

            speedButton.onClick.AddListener(SetNextSpeed);
            pauseButton.onClick.AddListener(TogglePauseSpeech);
            skipButton.onClick.AddListener(SkipSpeech);
            muteButton.onClick.AddListener(MuteSpeech);
            previousButton.onClick.AddListener(GoToPreviousSpeech);

            audioPlayer = GetComponent<AudioPlayer>();
            audioSource = GetComponent<AudioSource>();

            menu = GetComponent<GuideMenu>();

            CurrentExhibit = AppControllerSingleton.Instance.ExcursionDefaultController.CurrentExhibit;

            IsInitiated = true;
        }

        private void Update()
        {
            if (excursionRoutine == null || (CurrentExhibit != null && CurrentExhibit.IsRoot))
            {
                previousButton.gameObject.SetActive(false);
            }
            else
            {
                previousButton.gameObject.SetActive(true);
            }

            if (isPaused && isInMenu && !AppControllerSingleton.Instance.MainMenu.gameObject.activeSelf)
            {
                TogglePauseSpeech();
            }

            if (isInMenu && !AppControllerSingleton.Instance.MainMenu.gameObject.activeSelf)
            {
                isInMenu = false;
            }
        }

        private void OnDestroy()
        {
            speedButton.onClick.RemoveAllListeners();
            pauseButton.onClick.RemoveAllListeners();
            skipButton.onClick.RemoveAllListeners();
            muteButton.onClick.RemoveAllListeners();
            previousButton.onClick.RemoveAllListeners();

            Time.timeScale = 1f;

            if (slides != null)
            {
                Destroy(slides.gameObject);
            }
        }

        public AudioClip StartAudioSpeech(SpeechUnit speech, ExhibitDefault exhibit)
        {
            var clip = ResourceManager.Instance.GetResource(speech.AudioGuid).GetAudio();
            if (clip == null)
            {
                return null;
            }

            audioPlayer.PlayAudio(clip, 0.5f);

            SetPlaySpeed(speeds[currentSpeedIndex]);

            return clip;
        }

        public void SetNextSpeed()
        {
            currentSpeedIndex += 1;
            currentSpeedIndex %= speeds.Length;
            speedButton.GetComponentInChildren<TextMeshProUGUI>().text = $"{speeds[currentSpeedIndex]}X";

            if (isPaused)
            {
                TogglePauseSpeech();
            }
            else
            {
                SetPlaySpeed(speeds[currentSpeedIndex]);
            }
        }

        public void SetPlaySpeed(float speed)
        {
            Time.timeScale = speed;
            audioSource.UnPause();
            audioSource.pitch = speed;
            isPaused = false;
        }

        public void PauseForOpenedWindow()
        {
            if (!isPaused)
            {
                TogglePauseSpeech();
            }

            toggleButtons.DisableButtons();
        }

        public void UnpauseForOpenedWindow()
        {
            if (isPaused)
            {
                TogglePauseSpeech();
            }

            toggleButtons.EnableButtons();
        }

        private void TogglePauseSpeech()
        {
            if (isPaused)
            {
                SetPlaySpeed(speeds[currentSpeedIndex]);
                isPaused = false;
                playImage.sprite = stopSprite;
            }
            else
            {
                audioSource.Pause();
                Time.timeScale = 0f;
                isPaused = true;
                playImage.sprite = playSprite;
            }

            
        }

        private void StopExcursion()
        {
            StopExcursionRoutine();
        }

        private void MuteSpeech()
        {
            if (audioPlayer.ToggleMute())
            {
                muteImage.sprite = unmuteSprite;
            }
            else
            {
                muteImage.sprite = muteSprite;
            }
        }

        protected private void ResetSpeech()
        {
            if (speechRoutine != null)
            {
                StopCoroutine(speechRoutine);
                speechRoutine = null;
            }
            
            if (subtitles.gameObject.activeSelf)
            {
                subtitles.HideSubtitle();
            }

            if (audioSource != null && audioSource.clip != null)
            {
                currentPictureTimer += (audioSource.clip.length - audioSource.time);
            }

            if (audioPlayer != null)
            {
                audioPlayer.StopAudio(0.5f);
            }
        }

        private void HideSlide()
        {
            if (slides.gameObject.activeSelf)
            {
                slides.HideSlide();
            }

            if (slideRoutine != null)
            {
                StopCoroutine(slideRoutine);
                slideRoutine = null;
            }
        }

        protected private void ResetShowings()
        {
            ResetSpeech();

            HideSlide();

            toggleButtons.EnableButtons();
            UnpauseForOpenedWindow();
        }

        protected private void StopExcursionRoutine()
        {
            ResetShowings();

            if (excursionRoutine != null)
            {
                StopCoroutine(excursionRoutine);
                excursionRoutine = null;
            }
        }

        private void SkipSpeech()
        {
            if (isPaused)
            {
                TogglePauseSpeech();
            }

            if (speechRoutine != null)
            {
                ResetSpeech();
            }
        }

        private void GoToPreviousSpeech()
        {
            if (CurrentExhibit.PrevExhibit == null)
            {
                return;
            }

            RevertIsFinished(CurrentExhibit.PrevExhibit);

            StartExcursionCoroutine(HeadExhibit);
        }

        private void RevertIsFinished(ExhibitDefault exhibit)
        {
            if (exhibit == null)
            {
                return;
            }

            if (exhibit.SpeechUnits == null || exhibit.SpeechUnits.Length == 0)
            {
                exhibit.IsFinished = false;
                RevertIsFinished(exhibit.PrevExhibit);
            }
            else
            {
                exhibit.IsFinished = false;
            }
        }

        public void PlayExcursion(ExhibitDefault exhibit)
        {
            HeadExhibit = exhibit;

            StartExcursionCoroutine(exhibit);
        }

        public void StartExcursionCoroutine(ExhibitDefault exhibit)
        {
            StopExcursionRoutine();

            if (exhibit.IsRoot)
            {
                excursionRoutine = StartCoroutine(ToPlayBoatExcursion(exhibit));
            }
            else if (exhibit.IsHall)
            {
                excursionRoutine = StartCoroutine(ToPlayHallExcursion(exhibit, true));
            }
            else
            {
                excursionRoutine = StartCoroutine(ToPlayExhibit(exhibit));
            }
        }

        private protected virtual IEnumerator ToPlayBoatExcursion(ExhibitDefault exhibit)
        {
            if (!exhibit.IsFinished)
            {
                yield return ToPlayExhibit(exhibit);
            }

            if (exhibit.NextExhibit != null)
            {
                yield return ToPlayHallExcursion(exhibit.NextExhibit, false);
            }

            var defaultRepo = AppControllerSingleton.Instance.ExcursionDefaultController.ExcursionDefaultRepository;
            var rootExhibit = ((AbstractExcursionRepository)defaultRepo).Root;

            ShowEndPanel(rootExhibit);
        }

        private protected virtual IEnumerator ToPlayHallExcursion(ExhibitDefault exhibit, bool playFinished)
        {
            CurrentHall = exhibit;

            if (!exhibit.IsFinished)
            {
                yield return ToPlayExhibit(exhibit);
            }

            if (exhibit.NextExhibit != null)
            {
                yield return ToPlayExhibit(exhibit.NextExhibit);
            }

            yield return new WaitForEndOfFrame();
        }

        private protected virtual IEnumerator ToPlayExhibit(ExhibitDefault exhibit)
        {
            if (!exhibit.IsFinished)
            {
                yield return LoadAndPreload(exhibit);

                PrepareCharacters(exhibit);

                CurrentExhibit = exhibit;
                AppControllerSingleton.Instance.ExcursionDefaultController.CurrentExhibit = exhibit;

                ExhibitNameText.SetLocalizedStringText(exhibit.Name);

                slideRoutine = StartCoroutine(ToShowSlides(exhibit));

                foreach (var speech in exhibit.SpeechUnits)
                {
                    speechRoutine = StartCoroutine(ToPlaySpeech(exhibit, speech));
                    while (speechRoutine != null)
                    {
                        yield return new WaitForEndOfFrame();
                    }
                }

                exhibit.IsFinished = true;

                ResetShowings();
            }

            if (exhibit.NextExhibit != null)
            {
                if (exhibit.NextExhibit.IsHall)
                {
                    yield return ToPlayHallExcursion(exhibit.NextExhibit, false);
                }
                else
                {
                    yield return ToPlayExhibit(exhibit.NextExhibit);
                }
            }
        }

        protected private IEnumerator LoadAndPreload(ExhibitDefault exhibit)
        {
            if (!ResourceManager.Instance.IsLoadedExhibit(exhibit.Id))
            {
                loadingIndicator.SetActive(true);
                toggleButtons.DisableButtons();
            }

            yield return ResourceManager.Instance.LoadExhibit(exhibit.Id);
            StartCoroutine(ResourceManager.Instance.TryToPreload(exhibit));

            loadingIndicator.SetActive(false);
            toggleButtons.EnableButtons();
        }

        protected private IEnumerator ToPlaySpeech(ExhibitDefault exhibit, SpeechUnit speech)
        {
            yield return new WaitForSeconds(speech.PlaybackDelay);
            var animationDuration = PlayAnimation(exhibit, speech);
            var audioClip = StartAudioSpeech(speech, exhibit);
            var duration = audioClip != null ? audioClip.length : defaultSpeechDuration;

            if (PlayerPrefs.GetInt("subtitles") == 1 && speech.Subtitles != string.Empty && speech.Subtitles != "")
            {
                subtitles.ShowSubtitle(speech.Subtitles, duration);
            }
            else if (audioClip == null)
            {
                duration = 0f;
            }

            yield return new WaitForSeconds(Mathf.Max(animationDuration, duration));

            foreach (var routine in animationRoutines)
            {
                StopCoroutine(routine);
            }

            foreach (var character in CharactersDict)
            {
                character.Value.CharacterObject.transform.position = CharacterPositions[character.Value.CharacterPositionIndex].position;
            }

            ResetBoolAnimations();

            animationRoutines.Clear();

            speechRoutine = null;
        }

        private float PlayAnimation(ExhibitDefault exhibit, SpeechUnit speech)
        {
            ResetBoolAnimations();

            var duration = 0f;
            foreach (var animation in speech.AnimationUnits)
            {
                if (animation.CharacterIndex > exhibit.Characters.Length - 1)
                {
#if DEBUG
                    Debug.Log("Character index is out of range Characters array!");
#endif
                    continue;
                }

                var animator = CharactersDict[animation.CharacterIndex].CharacterObject.GetComponentInChildren<Animator>();
                var animationDuration = SetAnimation(animation, animator);
                var moveDuration = MoveCharacter(animation);
                var maxDuration = Mathf.Max(animationDuration, moveDuration);

                if (duration < maxDuration)
                {
                    duration = maxDuration;
                }
            }
            
            return duration;
        }

        private float MoveCharacter(AnimationUnit animation)
        {
            var moveStep = animation.MoveDirection == MoveDirection.Left ? -1 : 1;

            if (animation.MoveType == MoveType.Walk)
            {
                var resultPositionIndex = CharactersDict[animation.CharacterIndex].CharacterPositionIndex + moveStep;
                if (resultPositionIndex >= 0 && resultPositionIndex <= CharacterPositions.Length - 1)
                {
                    animationRoutines.Add(StartCoroutine(ToMove(moveStep, animation, animation.PlaybackDuration)));
                    return animation.PlaybackDuration + animation.PlaybackDelay;
                }
                else
                {
#if DEBUG
                    Debug.Log("Target position is wrong! (for " + CharactersDict[animation.CharacterIndex].CharacterObject.name + ")");
#endif
                }
            }
            else if (animation.MoveType == MoveType.Turn)
            {
                animationRoutines.Add(StartCoroutine(ToTurn(moveStep, animation, animation.PlaybackDuration)));
                return animation.PlaybackDuration + animation.PlaybackDelay;
            }

            return 0f;
        }

        private IEnumerator ToMove(int moveStep, AnimationUnit animation, float duration)
        {
            var posIndex = CharactersDict[animation.CharacterIndex].CharacterPositionIndex;
            var character = CharactersDict[animation.CharacterIndex].CharacterObject;
            var targetPosition = CharacterPositions[posIndex + moveStep].position;
            var startPosition = character.transform.position;
            var timer = 0f;

            CharactersDict[animation.CharacterIndex] = new Character()
            {
                CharacterObject = CharactersDict[animation.CharacterIndex].CharacterObject,
                CharacterPositionIndex = posIndex + moveStep,
            };

            yield return new WaitForSeconds(animation.PlaybackDelay);

            while ((character.transform.position.x < CharacterPositions[posIndex + moveStep].position.x && moveStep > 0) ||
                (character.transform.position.x > CharacterPositions[posIndex + moveStep].position.x && moveStep < 0))
            {
                timer += Time.deltaTime;
                character.transform.position = Vector3.Lerp(startPosition, targetPosition, timer / duration);

                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator ToTurn(int moveStep, AnimationUnit animation, float duration)
        {
            var character = CharactersDict[animation.CharacterIndex].CharacterObject;
            var targetRotation = character.transform.rotation * Quaternion.AngleAxis(moveStep * 45f, character.transform.up);
            var startRotation = character.transform.rotation;
            var timer = 0f;

            yield return new WaitForSeconds(animation.PlaybackDelay);

            while (character != null && Quaternion.Angle(character.transform.rotation, targetRotation) > 1)
            {
                timer += Time.deltaTime / duration;

                character.transform.rotation = Quaternion.Slerp(startRotation, targetRotation, timer);

                yield return new WaitForEndOfFrame();
            }
        }

        private void ResetBoolAnimations()
        {
            foreach (var character in CharactersDict.Values)
            {
                var animator = character.CharacterObject.GetComponentInChildren<Animator>();

                foreach (var parameter in animator.parameters)
                {
                    if (parameter.type == AnimatorControllerParameterType.Bool)
                    {
                        animator.SetBool(parameter.name, false);
                    }
                }
            }
            
        }

        private float SetAnimation(AnimationUnit animation, Animator animator)
        {
            if (animation.AnimationKey == string.Empty || 
                animation.AnimationKey == "" ||
                !animator.parameters.Any(item => item.name == animation.AnimationKey))
            {
                return 0f;
            }

            var duration = 0f;

            switch (animation.AnimationType)
            {
                case AnimationType.Bool:
                    animator.SetBool(animation.AnimationKey, !animator.GetBool(animation.AnimationKey));
                    break;

                case AnimationType.Float:
#if DEBUG
                    Debug.Log("Not implemented realisation for float type!");
#endif
                    break;

                case AnimationType.Trigger:
                    animator.SetTrigger(animation.AnimationKey);
                    duration = animator.GetCurrentAnimatorStateInfo(0).length;
                    break;
            }

            return duration;
        }

        private protected void PrepareCharacters(ExhibitDefault exhibit)
        {
            CharactersDict.Clear();

            var prefabCharDict = exhibit.Characters
                .Select((item, index) => new { index, item })
                .ToDictionary(item => item.index, item => item.item);

            for (var k = 0; k < CharacterPositions.Length; k++)
            {
                var characters = prefabCharDict
                        .Where(item => item.Value.CharacterPositionIndex == k)
                        .ToDictionary(item => item.Key, item => item.Value);

                foreach (Transform existCharacter in CharacterPositions[k])
                {
                    var coincidences = characters
                        .Where(item => existCharacter.name.Contains(item.Value.CharacterObject.name));

                    if (coincidences.Count() > 0)
                    {
                        CharactersDict.Add(coincidences.First().Key, 
                            new Character { CharacterObject = existCharacter.gameObject, CharacterPositionIndex = k });
                        characters.Remove(coincidences.First().Key);
                    }
                    else
                    {
                        Destroy(existCharacter.gameObject);
                    }
                }

                foreach (var character in characters)
                {
                    var newCharacter = Instantiate(character.Value.CharacterObject, CharacterPositions[k]);
                    CharactersDict.Add(character.Key, new Character { CharacterObject = newCharacter, CharacterPositionIndex = k });
                }
            }
        }

        private protected IEnumerator ToShowSlides(ExhibitDefault exhibit)
        {
            currentPictureTimer = 0f;

            var totalSpeechesDuration = exhibit
                .SpeechUnits
                .Sum(item =>
                {
                    var duration = 0f;
                    var clip = ResourceManager.Instance.GetResource(item.AudioGuid).GetAudio();
                    if (clip == null)
                    {
                        duration = defaultSpeechDuration;
                    }
                    else
                    {
                        duration = clip.length;
                    }

                    return duration + item.PlaybackDelay;
                });

            var unitTime = totalSpeechesDuration / (float)exhibit.PictureUnits.Length;
            
            for (int slideCounter = 0; slideCounter < exhibit.PictureUnits.Length; currentPictureTimer += Time.deltaTime)
            {
                if (currentPictureTimer >= slideCounter * unitTime)
                {
                    slides.ShowSlide(ResourceManager.Instance.GetResource(exhibit.PictureUnits[slideCounter].Image).GetSprite(), unitTime);
                    slideCounter++;
                }

                yield return new WaitForEndOfFrame();
            }
        }

        public void ShowEndPanel(ExhibitDefault root)
        {
            ResetShowings();

            menu.ShowEndWindow(root, false);
        }
    }
}
