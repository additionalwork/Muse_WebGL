namespace Naval.UI
{
    using Naval.AR;
    using Naval.Model;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class QRWindow : MenuWindow
    {
        [Header("Camera")]
        [SerializeField] private QrCodeProcessing QrCodeProcessing;
        [SerializeField] private TextMeshProUGUI InfoText;
        [SerializeField] private AspectRatioFitter _aspectRatioFitter;
        [SerializeField] private Mask _mask;
        [SerializeField] private GameObject _cameraPanel;

        private Button _backButton;
        private bool _isPlayback = false;
        private ARAnimator _animator;
        private BaseWindowBehaviour _baseWindowBehaviour;

        private void Awake()
        {
            _animator = GetComponent<ARAnimator>();
            _backButton = GetComponentInParent<BaseWindowBehaviour>().BackButton;
            _baseWindowBehaviour = GetComponentInParent<BaseWindowBehaviour>();
        }

        private void Start()
        {
            InfoText.SetLocalizedStringText("");

            QrCodeProcessing.Callback.AddListener(text => CallbackQR(text));
        }

        private void Update()
        {
            RefreshAspectRatio();
        }

        public void CallbackQR(string text)
        {
            if (!AuthManager.Authorized)
            {
                AuthManager.Token = text;

                if (AuthManager.Authorized)
                {
                    AppControllerSingleton.CreateWindow(AppControllerSingleton.Instance.SettingsPrefab);
                    AppControllerSingleton.ToggleMainMenu();
                }
                else
                {
                    InfoText.SetLocalizedString("Wrong_Qr_For_Login");
                }

                return;
            }
            else if (AuthManager.IsCorrectToken(text))
            {
                AuthManager.Token = text;
                AppControllerSingleton.CreateWindow(AppControllerSingleton.Instance.SettingsPrefab);
                AppControllerSingleton.ToggleMainMenu();

                return;
            }

            if (AppControllerSingleton.Instance.ExcursionDefaultController.ExcursionDefaultRepository.HasData(text))
            {
                if (_baseWindowBehaviour != null)
                {
                    _baseWindowBehaviour.BackToMainMenu();
                }

                AppControllerSingleton.StartExcursionDefaultById(text);
                
                Destroy();
            }
            else if (ARControllerSingleton.Instance.AnimationProfiles.ContainsKey(text))
            {
                StartPlaybackAR(ARControllerSingleton.Instance.AnimationProfiles[text]);
                _backButton.onClick.AddListener(StopPlaybackAR);
            }
            else
            {
                Debug.Log("Wrong_QR_code: " + text);
                InfoText.SetLocalizedString("Wrong_QR_code: " + text);
            }
        }

        public override void Initialize(ExhibitDefault currentExhibit)
        {
            Start();
        }

        public override void Destroy()
        {
            Destroy(gameObject);
        }

        public void StartPlaybackAR(ARAnimationProfile profile)
        {
            if (_isPlayback) return;

            QrCodeProcessing.IsDecodeStopped = true;
            _animator.PlayARAnimation(profile);
            _backButton.gameObject.SetActive(true);
            _mask.enabled = false;
            _isPlayback = true;
        }

        public void StopPlaybackAR()
        {
            if (!_isPlayback) return;

            QrCodeProcessing.IsDecodeStopped = false;
            _animator.StopARAnimationProfileRoutine();
            _backButton.gameObject.SetActive(false);
            _mask.enabled = true;
            _isPlayback = false;
        }

        public void RefreshAspectRatio()
        {
            var cameraRect = _cameraPanel.GetComponent<RectTransform>();
            var mainRect = transform.GetComponent<RectTransform>().rect;
            cameraRect.rect.Set(mainRect.x, mainRect.y, mainRect.width, mainRect.height);
        }
    }
}
