namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
    using Naval.Model;

    public class FAQWindow : MenuWindow
    {
        [SerializeField] private GameObject Questions;
        [SerializeField] private GameObject QuestionContent;

        [SerializeField] private TextMeshProUGUI ExhibitLabel;
        [SerializeField] private TextMeshProUGUI NoQuestionsLabel;
        [SerializeField] private GameObject FAQTile;
        [SerializeField] private Transform TileContent;
        [SerializeField] private Button BackButton;

        [SerializeField] private TextMeshProUGUI QuestionName;
        [SerializeField] private TextMeshProUGUI QuestionText;
        [SerializeField] private Image QuestionImage;

        [SerializeField] private GridLayoutGroup GridLayout;
        [SerializeField] private RectTransform GridContainer;

        public override void Initialize(ExhibitDefault currentExhibit)
        {
            ToggleQuestionContent(false);

            for (int i = TileContent.childCount - 1; i >= 0; i--)
            {
                Destroy(TileContent.GetChild(i).gameObject);
            }

            ExhibitLabel.SetLocalizedStringText(currentExhibit.Name);

            if (currentExhibit.FAQUnits.Length == 0)
            {
                NoQuestionsLabel.gameObject.SetActive(true);
            }
            else
            {
                NoQuestionsLabel.gameObject.SetActive(false);
            }

            int counter = 0;
            foreach (var FAQ in currentExhibit.FAQUnits)
            {
                var button = Instantiate(FAQTile, TileContent);
                
                button.GetComponentInChildren<TextMeshProUGUI>().SetLocalizedStringText(FAQ.Name);
                var index = counter;
                var faqToButton = FAQ;
                button.GetComponent<Button>().onClick.AddListener(() =>
                {
                    OpenQuestion(faqToButton);  
                });
                counter++;
            }

            BackButton.onClick.AddListener(GoBack);
        }

        private void Update()
        {
            if (GridContainer.rect.width > GridContainer.rect.height)
            {
                GridLayout.cellSize = new Vector2(GridContainer.rect.width / 2 - GridLayout.spacing.x, GridContainer.rect.height - GridLayout.spacing.y);
                GridLayout.startCorner = GridLayoutGroup.Corner.UpperLeft;
            }
            else
            {
                GridLayout.cellSize = new Vector2(GridContainer.rect.width - GridLayout.spacing.x, GridContainer.rect.height / 2 - GridLayout.spacing.y);
                GridLayout.startCorner = GridLayoutGroup.Corner.LowerLeft;
            }
        }

        private void OpenQuestion(FAQUnit FAQ)
        {
            if (FAQ.Image != null)
            {
                QuestionImage.gameObject.SetActive(true);
                QuestionImage.sprite = ResourceManager.Instance.GetResource(FAQ.Image).GetSprite();
            }
            else
            {
                QuestionImage.gameObject.SetActive(false);
            }

            QuestionName.SetLocalizedStringText(FAQ.Name);
            QuestionText.SetLocalizedStringText(FAQ.Text);

            ToggleQuestionContent(true);

            BackButton.gameObject.GetComponentInChildren<TextMeshProUGUI>().SetLocalizedString("GuideWindow_FAQBackToQuestions");
        }

        private void GoBack()
        {
            if (QuestionContent.activeSelf)
            {
                BackButton.gameObject.GetComponentInChildren<TextMeshProUGUI>().SetLocalizedString("GuideWindow_FAQContinueButton");
                ToggleQuestionContent(false);
            }
            else
            {
                Destroy();
            }
        }

        private void ToggleQuestionContent(bool openQuestion)
        {
            Questions.SetActive(!openQuestion);
            QuestionContent.SetActive(openQuestion);
        }

        public override void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
