namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
    using System.Linq;
    using Naval.Model;

    public class EndWindow : MenuWindow
    {
        [HideInInspector] public bool WithTasks;

        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private TextMeshProUGUI textPercentCorrect;
        [SerializeField] private TextMeshProUGUI textPercentCorrectValue;
        [SerializeField] private TextMeshProUGUI textPercentFinished;
        [SerializeField] private TextMeshProUGUI textPercentFinishedValue;
        [SerializeField] private TextMeshProUGUI achievementText;
        [SerializeField] private TextMeshProUGUI noAchievementText;
        [SerializeField] private Transform achievemetsContent;
        [SerializeField] private GameObject achievemetsPrefab;
        [SerializeField] private Button startAgain;

        private ExhibitDefault root;

        public override void Initialize(ExhibitDefault exhibitRoot)
        {
            startAgain.onClick.AddListener(StartAgain);

            for (int i = achievemetsContent.childCount - 1; i >= 0; i--)
            {
                Destroy(achievemetsContent.GetChild(i).gameObject);
            }

            root = exhibitRoot;

            if (!WithTasks)
            {
                text.SetLocalizedString("EndWindow_Info");
                achievementText.gameObject.SetActive(false);
                noAchievementText.gameObject.SetActive(false);
                return;
            }

            achievementText.gameObject.SetActive(true);
            var counter = 0;
            foreach (var hall in exhibitRoot.ChildExhibits)
            {
                var currentHall = (ExhibitInteractive)hall;
                if (currentHall.TaskUnits.All(task => task.IsCorrect) && currentHall.TaskUnits.Length > 0)
                {
                    counter++;
                    GiveAchievement(hall);
                }
            }

            if (counter == 0)
            {
                achievementText.gameObject.SetActive(false);
                noAchievementText.gameObject.SetActive(true);
            }

            var correctNew = 0;
            var finishedNew = 0;
            var all = 0;
            foreach (var hall in exhibitRoot.ChildExhibits)
            {
                correctNew += (hall as ExhibitInteractive).TaskUnits.Count(task => task.IsCorrect);
                finishedNew += (hall as ExhibitInteractive).TaskUnits.Count(task => task.IsFinished);
                all += (hall as ExhibitInteractive).TaskUnits.Count();

                foreach (var exhibit in hall.ChildExhibits)
                {
                    correctNew += (exhibit as ExhibitInteractive).TaskUnits.Count(task => task.IsCorrect);
                    finishedNew += (exhibit as ExhibitInteractive).TaskUnits.Count(task => task.IsFinished);
                    all += (exhibit as ExhibitInteractive).TaskUnits.Count();
                }
            }

            text.SetLocalizedString("EndWindow_InfoWithTasks");
            textPercentFinished.SetLocalizedString("EndWindow_FinishedTasksLabel");
            textPercentCorrect.SetLocalizedString("EndWindow_CorrectTasksLabel");
            textPercentFinishedValue.SetLocalizedStringText($"{(100f * finishedNew / all):F1}% ({finishedNew}/{all})");
            textPercentCorrectValue.SetLocalizedStringText($"{(100f * correctNew / finishedNew):F1}% ({correctNew}/{finishedNew})");
        }

        private void GiveAchievement(ExhibitDefault hall)
        {
            var achievement = GameObject.Instantiate(achievemetsPrefab, achievemetsContent);
            achievement.GetComponentInChildren<TextMeshProUGUI>().SetLocalizedStringText(hall.Name);
        }

        private void StartAgain()
        {
            var currentRepository = WithTasks ?
                AppControllerSingleton.Instance.ExcursionInteractiveController.ExcursionInteractiveRepository :
                AppControllerSingleton.Instance.ExcursionDefaultController.ExcursionDefaultRepository;

            AppControllerSingleton.ResetExcursionProgress(currentRepository);

            if (WithTasks)
            {
                AppControllerSingleton.StartNewExcursionInteractive();
            }
            else
            {
                AppControllerSingleton.StartNewExcursionDefault();
            }

            Destroy();
        }

        public override void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
