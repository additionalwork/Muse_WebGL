namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
    using Naval.Model;

    public class ExhibitsWindow : MenuWindow
    {
        [SerializeField] private TextMeshProUGUI ExhibitLabel;
        [SerializeField] private GameObject ExhibitTile;
        [SerializeField] private GameObject ExhibitParentCurrentGameObject;
        [SerializeField] private Transform TileContent;
        [SerializeField] private GameObject BoatSchemeWindowPrefab;

        public override void Initialize(ExhibitDefault currentExhibit)
        {
            for (int i = TileContent.childCount - 1; i >= 0; i--)
            {
                Destroy(TileContent.GetChild(i).gameObject);
            }

            ExhibitLabel.SetLocalizedStringText(currentExhibit.Name);

            ExhibitParentCurrentGameObject.GetComponent<Button>().onClick.AddListener(() =>
            {
                AppControllerSingleton.CreateWindow(AppControllerSingleton.Instance.SchemePrefab);
                Destroy();
            });

            foreach (var exhibit in currentExhibit.ChildExhibits)
            {
                var button = Instantiate(ExhibitTile, TileContent);
                button.GetComponentInChildren<TextMeshProUGUI>().SetLocalizedStringText(exhibit.Name);
                button.GetComponent<Button>().onClick.AddListener(() =>
                {
                    exhibit.IsFinished = false;
                    AppControllerSingleton.StartExcursionDefaultById(exhibit.Id);
                    Destroy();
                });
            }
        }

        public override void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
