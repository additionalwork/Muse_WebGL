namespace Naval.UI
{
    using Naval.Model;
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class MenuWindow : MonoBehaviour
    {
        public UnityAction Callback;
        public abstract void Initialize(ExhibitDefault exhibit);
        public abstract void Destroy();

        public void OnDestroy()
        {
            if (Callback != null)
            {
                Callback.Invoke();
            }
        }
    }
}
