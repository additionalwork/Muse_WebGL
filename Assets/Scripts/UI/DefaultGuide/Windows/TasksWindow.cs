namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
    using System.Linq;
    using Naval.Model;

    public class TasksWindow : MenuWindow
    {
        [SerializeField] private GameObject TaskTile;
        [SerializeField] private GameObject FinishedTaskTile;
        [SerializeField] private Transform TileContent;
        [SerializeField] private Button BackButton;

        public override void Initialize(ExhibitDefault currentExhibit)
        {
            for (int i = TileContent.childCount - 1; i >= 0; i--)
            {
                Destroy(TileContent.GetChild(i).gameObject);
            }

            BackButton.onClick.AddListener(Destroy);

            int counter = 0;

            GameObject button;
            foreach (var task in ((ExhibitInteractive)currentExhibit).TaskUnits)
            {
                if (task.IsFinished)
                {
                    button = Instantiate(FinishedTaskTile, TileContent);
                    button.GetComponent<Button>().interactable = false;
                }
                else
                {
                    button = Instantiate(TaskTile, TileContent);
                }
                
                button.GetComponentInChildren<TextMeshProUGUI>().SetLocalizedStringText(task.Name);
                var index = counter;
                button.GetComponent<Button>().onClick.AddListener(() =>
                {
                    AppControllerSingleton.StartTask(currentExhibit.Id, index);
                    Callback = null;
                    Destroy();
                });
                counter++;
            }
        }

        public override void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
