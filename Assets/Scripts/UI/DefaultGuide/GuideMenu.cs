namespace Naval.UI
{
    using Naval.Model;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuideMenu : WindowAbstract
    {
        [Header("Common:")]
        [SerializeField] private Transform WindowParent;
        [SerializeField] private GameObject EndWindow;

        [Header("FAQ:")]
        [SerializeField] private Button FAQ;
        [SerializeField] private GameObject FAQPrefab;

        [Header("QR:")]
        [SerializeField] private Button QR;
        [SerializeField] private GameObject QRPrefab;

        [Header("FlowControl:")]
        [SerializeField] private Button FlowControl;
        [SerializeField] private GameObject FlowControlPrefab;

        [Header("Scheme:")]
        [SerializeField] private Button Scheme;
        [SerializeField] private GameObject SchemePrefab;

        [Header("Tasks:")]
        [SerializeField] private Button Tasks;
        [SerializeField] private GameObject TasksPrefab;

        [Header("Settings:")]
        [SerializeField] private Button Settings;
        [SerializeField] private GameObject SettingsPrefab;

        private Guide Guide;
        public GameObject CurrentWindow;

        private void Awake()
        {
            Guide = GetComponent<Guide>();

            FAQ.onClick.AddListener(() => CreateSubWindow(FAQPrefab, true));
            QR.onClick.AddListener(() => CreateSubWindow(QRPrefab, false));
            FlowControl.onClick.AddListener(() => CreateSubWindow(FlowControlPrefab, true));
            Scheme.onClick.AddListener(() => CreateSubWindow(SchemePrefab, false));
            Tasks.onClick.AddListener(() => CreateSubWindow(TasksPrefab, true));
            Settings.onClick.AddListener(() =>
            {
                var settings = CreateSubWindow(SettingsPrefab, false);
                settings.GetComponent<SettingsWindow>().FromGuideWindow = true;
            });
        }

        private void OnDestroy()
        {
            FAQ.onClick.RemoveAllListeners();
            QR.onClick.RemoveAllListeners();
            FlowControl.onClick.RemoveAllListeners();
            Scheme.onClick.RemoveAllListeners();
            Tasks.onClick.RemoveAllListeners();
            Settings.onClick.RemoveAllListeners();
        }

        public GameObject CreateSubWindow(GameObject window, bool isSubWindow = false)
        {
            if (isSubWindow)
            {
                return SetWindow(window);
            }
            else
            {
                return CreateMajorWindow(window);
            }
        }

        public GameObject CreateMajorWindow(GameObject window)
        {
            var newWindow = Instantiate(window, AppControllerSingleton.Instance.ActiveWindows);
            newWindow.GetComponent<BaseWindowBehaviour>().SetupBackButton(() =>
            {
                AppControllerSingleton.StartExcursionDefaultFrom(AppControllerSingleton.Instance.ExcursionDefaultController.CurrentExhibit);
                Destroy(newWindow);
            });
            Destroy(gameObject);
            return newWindow;
        }

        public GameObject SetWindow(GameObject window)
        {
            if (Guide.CurrentExhibit.IsRoot)
            {
                return null;
            }

            var currentHall = Guide.CurrentExhibit.IsHall ? Guide.CurrentExhibit : Guide.CurrentExhibit.Parent;

            Guide.PauseForOpenedWindow();

            if (CurrentWindow != null)
            {
                if (CurrentWindow.TryGetComponent<MenuWindow>(out var menu))
                {
                    menu.Destroy();
                }
                else
                {
                    Destroy(CurrentWindow);
                }
            }

            CurrentWindow = GameObject.Instantiate(window, WindowParent);
            CurrentWindow.GetComponent<MenuWindow>().Initialize(currentHall);
            CurrentWindow.GetComponent<MenuWindow>().Callback = Guide.UnpauseForOpenedWindow;

            return CurrentWindow;
        }

        public void SetWindowExhibits()
        {
            SetWindow(FlowControlPrefab);
        }

        public void ShowEndWindow(ExhibitDefault root, bool withTasks)
        {
            Guide.PauseForOpenedWindow();

            if (CurrentWindow != null)
            {
                if (CurrentWindow.TryGetComponent<MenuWindow>(out var menu))
                {
                    menu.Destroy();
                }
                else
                {
                    Destroy(CurrentWindow);
                }
            }

            CurrentWindow = GameObject.Instantiate(EndWindow, WindowParent);
            CurrentWindow.GetComponent<EndWindow>().WithTasks = withTasks;
            CurrentWindow.GetComponent<MenuWindow>().Initialize(root);
            CurrentWindow.GetComponent<MenuWindow>().Callback = Guide.UnpauseForOpenedWindow;
        }
    }
}
