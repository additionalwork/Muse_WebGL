namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Naval.Model;

    public class TasksShowing : MonoBehaviour
    {
        [SerializeField] private Button skipButton;
        [SerializeField] private GameObject PanelParent;
        [SerializeField] private GameObject SkipWarningPanel;
        [SerializeField] private List<ButtonPanelPrefabByType> Panels;
        [SerializeField] private InteractiveGuide GuideController;
        private TaskPanel currentPanel;
        private GameObject skipPanel;
        private bool ShowedMessageToSkip;

        public void Start()
        {
            skipButton.onClick.AddListener(ShowSkipTask);
        }

        public void StartTask(ExhibitInteractive exhibit, int taskIndex)
        {
            HidePanels();
            var panel = Panels.FirstOrDefault(panel => panel.Type == exhibit.TaskUnits[taskIndex].Type);

            if (panel == null)
            {
#if DEBUG
                Debug.LogError("Missing panel for given task type!");
#endif
                return;
            }

            var newPanel = GameObject.Instantiate(panel.PanelPrefab, PanelParent.transform);
            currentPanel = newPanel.GetComponent<TaskPanel>();
            currentPanel.Initialize(exhibit, taskIndex);
            currentPanel.AnswerGiven.AddListener(SendAnswer);
            gameObject.SetActive(true);
            skipButton.interactable = true;
        }

        public void HidePanels()
        {
            if (currentPanel != null && currentPanel.TryGetComponent<WindowAbstract>(out var abstractWindow))
            {
                abstractWindow.DestroyWindow();
            }
            else if (currentPanel != null)
            {
                Destroy(currentPanel.gameObject);
            }


            if (skipPanel != null && skipPanel.TryGetComponent<WindowAbstract>(out var abstractWindowSkip))
            {
                abstractWindowSkip.DestroyWindow();
            }
            else if (skipPanel != null)
            {
                Destroy(skipPanel.gameObject);
            }

            gameObject.SetActive(false);
        }

        private void ShowSkipTask()
        {
            if (ShowedMessageToSkip)
            {
                HidePanels();
                GuideController.SkipTask();
            }
            else
            {
                if (skipPanel != null)
                {
                    Destroy(skipPanel);
                }

                skipPanel = GameObject.Instantiate(SkipWarningPanel, PanelParent.transform);
                var comp = skipPanel.GetComponent<SkipPanel>();
                comp.OkButton.onClick.AddListener(SkipTask);
            }
        }

        private void SkipTask()
        {
            ShowedMessageToSkip = true;
            ShowSkipTask();
        }

        private void SendAnswer(string answer)
        {
            GuideController.SetAnswer(answer);
            skipButton.interactable = false;
        }

        [Serializable]
        private class ButtonPanelPrefabByType
        {
            public TaskType Type;
            public GameObject PanelPrefab;
        }
    }
}
