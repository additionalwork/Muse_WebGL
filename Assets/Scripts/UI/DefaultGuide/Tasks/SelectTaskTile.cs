namespace Naval.UI
{
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class SelectTaskTile : MonoBehaviour
    {
        public TextMeshProUGUI Number;
        public TextMeshProUGUI Text;
        public Button Button;

        public string Answer;
    }
}
