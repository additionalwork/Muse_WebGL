namespace Naval.UI
{
    using Naval.Model;
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class TaskPanel : MonoBehaviour
    {
        [HideInInspector] public UnityEvent<string> AnswerGiven;
        public abstract void Initialize(ExhibitInteractive exhibit, int taskIndex);
        public abstract void Destroy();
    }
}
