namespace Naval.UI
{
    using UnityEngine;
    using TMPro;
    using System.Linq;
    using System.Collections.Generic;
    using UnityEngine.UI;
    using Naval.Model;

    public class SelectTaskPanel : TaskPanel
    {
        [SerializeField] private TextMeshProUGUI TaskNameText;
        [SerializeField] private TextMeshProUGUI TaskText;
        [SerializeField] private GameObject ButtonPref;
        [SerializeField] private Transform ButtonParent;

        private TaskUnit task;

        private SelectTaskTile CorrectTile;
        private List<SelectTaskTile> IncorrectTiles;

        public override void Initialize(ExhibitInteractive exhibit, int taskIndex)
        {
            for (int i = ButtonParent.childCount - 1; i >= 0; i--)
            {
                Destroy(ButtonParent.GetChild(i).gameObject);
            }

            task = exhibit.TaskUnits[taskIndex];
            TaskNameText.SetLocalizedStringText(task.Name);
            TaskText.SetLocalizedStringText(task.Text);

            var answers = task.IncorrectAnswers.ToList();
            answers.Add(task.CorrectAnswer);
            answers = answers.OrderBy(ans => Random.value).ToList();
            var counter = 0;

            IncorrectTiles = new List<SelectTaskTile>();

            foreach (var answer in answers)
            {
                counter++;

                var button = GameObject.Instantiate(ButtonPref, ButtonParent);
                var tile = button.GetComponent<SelectTaskTile>();
                tile.Answer = answer;
                tile.Text.SetLocalizedStringText(answer);
                tile.Number.text = counter.ToString();
                tile.Button.onClick.AddListener(() => SendAnswer(answer));

                if (answer == task.CorrectAnswer)
                {
                    CorrectTile = tile;
                }
                else
                {
                    IncorrectTiles.Add(tile);
                }
            }
        }

        private void SendAnswer(string answer)
        {
            CorrectTile.Text.color = Color.green;
            CorrectTile.gameObject.GetComponent<Button>().interactable = false;

            foreach (var incorrect in IncorrectTiles)
            {
                incorrect.gameObject.GetComponent<Button>().interactable = false;
                if (incorrect.Answer == answer)
                {
                    incorrect.Text.color = Color.red;
                }
            }

            AnswerGiven.Invoke(answer);
        }

        public override void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
