namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
    using Naval.Model;

    public class InputTaskPanel : TaskPanel
    {
        [SerializeField] private TMP_InputField Input;
        [SerializeField] private Button ProceedButton;
        [SerializeField] private TextMeshProUGUI TaskNameText;
        [SerializeField] private TextMeshProUGUI TaskText;

        public override void Initialize(ExhibitInteractive exhibit, int taskIndex)
        {
            var task = exhibit.TaskUnits[taskIndex];
            TaskNameText.SetLocalizedStringText(task.Name);
            TaskText.SetLocalizedStringText(task.Text);
            ProceedButton.onClick.AddListener(SendAnswer);
        }

        private void SendAnswer()
        {
            if (Input.text.Length != 0)
            {
                AnswerGiven.Invoke(Input.text);
            }
        }

        public override void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
