namespace Naval.UI
{
    using UnityEngine;
    using TMPro;
    using Naval.Model;

    public class QrTaskPanel : TaskPanel
    {
        [SerializeField] private TextMeshProUGUI TaskNameText;
        [SerializeField] private TextMeshProUGUI TaskText;
        [SerializeField] private GameObject QRWindow;

        public override void Initialize(ExhibitInteractive exhibit, int taskIndex)
        {
            var task = exhibit.TaskUnits[taskIndex];
            TaskNameText.SetLocalizedStringText(task.Name);
            TaskText.SetLocalizedStringText(task.Text);

            var processing = QRWindow.GetComponent<QrCodeProcessing>();
            processing.Callback.AddListener(SendAnswer);
            processing.OnlyCallback = true;
        }

        private void SendAnswer(string answer)
        {
            AnswerGiven.Invoke(answer);
            Destroy();
        }

        public override void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
