namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public sealed class BaseWindowBehaviour : MonoBehaviour
    {
        [SerializeField] public Button CornerButton;
        [SerializeField] public Button BackButton;

        private void Start()
        {
            if (CornerButton != null)
            {
                CornerButton.onClick.AddListener(AppControllerSingleton.ToggleMainMenu);
            }
        }

        private void OnDestroy()
        {
            if (CornerButton != null)
            {
                CornerButton.onClick.RemoveAllListeners();
            }

            if (BackButton != null)
            {
                BackButton.onClick.RemoveAllListeners();
            }
        }

        public void BackToMainMenu()
        {
            if (TryGetComponent<WindowAbstract>(out var window))
            {
                window.DestroyWindow();
            }
            else
            {
                Destroy(gameObject);
            }

            AppControllerSingleton.ToggleMainMenu();
        }

        public void SetupBackButton(UnityAction action)
        {
            if (BackButton == null)
            {
                return;
            }

            BackButton.gameObject.SetActive(true);

            BackButton.onClick.AddListener(() =>
            {
                action.Invoke();
                Destroy(this);
            });
        }
    }
}
