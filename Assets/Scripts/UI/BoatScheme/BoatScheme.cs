namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class BoatScheme : WindowAbstract, IDragHandler, IInitializePotentialDragHandler
    {
        [SerializeField] private RectTransform SchemeTransform;
        [SerializeField] private float ZoomSpeed = 2f;

        private Canvas Canvas;
        private Rect prevResolution;

        private void Start()
        {
            InitiateMap();

            prevResolution = Canvas.GetComponent<RectTransform>().rect;
        }

        private void Update()
        {
            UpdateExhibitMarks();

            if(Input.touchCount == 2){
                var rightTouch = Input.GetTouch(0);
                var leftTouch = Input.GetTouch(1);

                var rightTouchPrevPos = rightTouch.position - rightTouch.deltaPosition;
                var leftTouchPrevPos = leftTouch.position - leftTouch.deltaPosition;

                float prevMagnitude = (rightTouchPrevPos - leftTouchPrevPos).magnitude;
                float currentMagnitude = (rightTouch.position - leftTouch.position).magnitude;

                float difference = prevMagnitude - currentMagnitude;

                UpdateScale(difference * ZoomSpeed * 0.01f);
            }
            else if (Input.GetAxisRaw("Mouse ScrollWheel") != 0)
            {
                UpdateScale(Input.GetAxisRaw("Mouse ScrollWheel") * ZoomSpeed);
            }
            else if (prevResolution != Canvas.GetComponent<RectTransform>().rect)
            {
                UpdateScale(0);
            }

            UpdateDragging(SchemeTransform.localPosition);
        }

        private void InitiateMap()
        {
            Canvas = transform.parent.GetComponentInParent<Canvas>();
            UpdateScale(0f);
        }

        private void UpdateExhibitMarks()
        {

        }

        private void UpdateScale(float scaleOffset)
        {
            var currentSchemeScale = SchemeTransform.localScale.x + scaleOffset;
            var resolution = Canvas.GetComponent<RectTransform>().rect;

            if (resolution.width > SchemeTransform.rect.width * currentSchemeScale)
            {
                currentSchemeScale = resolution.width / (SchemeTransform.rect.width);
            }

            if (resolution.height > SchemeTransform.rect.height * currentSchemeScale)
            {
                currentSchemeScale = resolution.height / (SchemeTransform.rect.height);
            }

            SchemeTransform.localScale = new Vector3(currentSchemeScale, currentSchemeScale, currentSchemeScale);
        }

        private void UpdateDragging(Vector3 newPosition)
        {
            var resolution = Canvas.GetComponent<RectTransform>().rect;
            var maxOffsetX = ((SchemeTransform.rect.width * SchemeTransform.localScale.x) - resolution.width) / 2;
            var maxOffsetY = ((SchemeTransform.rect.height * SchemeTransform.localScale.y) - resolution.height) / 2;

            if (maxOffsetX < newPosition.x)
            {
                newPosition.x = maxOffsetX;
            }

            if (-maxOffsetX > newPosition.x)
            {
                newPosition.x = -maxOffsetX;
            }

            if (maxOffsetY < newPosition.y)
            {
                newPosition.y = maxOffsetY;
            }

            if (-maxOffsetY > newPosition.y)
            {
                newPosition.y = -maxOffsetY;
            }

            SchemeTransform.localPosition = newPosition;
        }

        public void OnDrag(PointerEventData eventData)
        {
            var newPosition = SchemeTransform.localPosition + new Vector3(eventData.delta.x, eventData.delta.y, 0f);
            UpdateDragging(newPosition);
        }

        public void OnInitializePotentialDrag(PointerEventData eventData)
        {
            eventData.useDragThreshold = false;
        }
    }
}
