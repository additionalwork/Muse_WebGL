namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.UI;

    public class ExhibitButton : MonoBehaviour
    {
        private void Start()
        {
            GetComponent<Button>().onClick.AddListener(() =>
            {
                AppControllerSingleton.StartExcursionDefaultById(name, true);
            });
        }

        private void OnDestroy()
        {
            GetComponent<Button>().onClick.RemoveAllListeners();
        }
    }
}
