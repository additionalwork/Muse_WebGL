using UnityEngine;
using UnityEngine.UI;
namespace Naval.UI
{
    public class SettingsWindow : MonoBehaviour
    {
        public bool FromStartWindow { get; set; }
        public bool FromGuideWindow { get; set; }

        [SerializeField] private Button BackButton;

        void Start()
        {
            BackButton.onClick.AddListener(GoBack);
        }

        private void OnDisable()
        {
            BackButton.onClick.RemoveAllListeners();
        }

        private void GoBack()
        {
            if (FromStartWindow)
            {
                AppControllerSingleton.CreateWindow(AppControllerSingleton.Instance.StartMenuPrefab);
            }
            else if (FromGuideWindow)
            {
                Destroy(this);
            }
            else
            {
                AppControllerSingleton.ToggleMainMenu();
            }
        }
    }
}
