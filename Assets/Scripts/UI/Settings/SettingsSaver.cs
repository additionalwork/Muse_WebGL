namespace Naval.UI
{
    using UnityEngine;
    using UnityEngine.UI;
    using TMPro;
    using System.Collections.Generic;
    using System;
    using Naval.Model;
    using UnityEngine.SocialPlatforms;

    public class SettingsSaver : MonoBehaviour
    {
        [SerializeField] private Toggle subtitlesToggle;
        [SerializeField] private Slider volumeSlider;
        [SerializeField] private TMP_Dropdown languageDropdown;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private GameObject localeSelectConfirmation;
        [SerializeField] private Button saveButton;
        private const string Subtitles = "subtitles";
        private const string Volume = "volume";
        private string SelectedLocale;
        
        private void Start()
        {
            subtitlesToggle.onValueChanged.AddListener((on) =>
            {
                PlayerPrefs.SetInt(Subtitles, on ? 1 : 0);
            });
            volumeSlider.onValueChanged.AddListener((val) =>
            {
                PlayerPrefs.SetFloat(Volume, val);
                AppControllerSingleton.Instance.AudioMixerController.SetMasterVolume(val);
            });

            languageDropdown.onValueChanged.AddListener((val) =>
            {
                SelectedLocale = AppControllerSingleton.Instance.LocalizationController.Locales[val];
                var isCurrentLocaleSelected = SelectedLocale == AppControllerSingleton.Instance.LocalizationController.CurrentLocale;
                localeSelectConfirmation.SetActive(!isCurrentLocaleSelected);
            });
            saveButton.onClick.AddListener(() =>
            {
                if (SelectedLocale != AppControllerSingleton.Instance.LocalizationController.CurrentLocale)
                {
                    AppControllerSingleton.Instance.LocalizationController.LocaleSelectByName(SelectedLocale);
                }
            });

            subtitlesToggle.isOn = PlayerPrefs.GetInt(Subtitles, 1) != 0;
            volumeSlider.value = PlayerPrefs.GetFloat(Volume, volumeSlider.maxValue);
            
            var languageOptions = new List<TMP_Dropdown.OptionData>();
            foreach (var locale in AppControllerSingleton.Instance.LocalizationController.Locales)
            {
                var name = (LocalizedString)AppControllerSingleton.Instance.LocalizationController.CurrentStringRepository[locale + "LocaleName"];
                name ??= new LocalizedString { Value = locale };
                languageOptions.Add(new TMP_Dropdown.OptionData(name.Value));
            }
            languageDropdown.options = languageOptions;

            SelectedLocale = AppControllerSingleton.Instance.LocalizationController.CurrentLocale;
            languageDropdown.value = Array.IndexOf(AppControllerSingleton.Instance.LocalizationController.Locales, SelectedLocale);

#if DEBUG
            text.text = Application.version;
#else
            Destroy(text.gameObject);
#endif
        }

        private void OnDestroy()
        {
            volumeSlider.onValueChanged.RemoveAllListeners();
            subtitlesToggle.onValueChanged.RemoveAllListeners();
            languageDropdown.onValueChanged.RemoveAllListeners();
            saveButton.onClick.RemoveAllListeners();
        }
    }
}
