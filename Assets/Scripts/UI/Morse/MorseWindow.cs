namespace Naval.UI
{
    using Naval.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;
    using App = AppControllerSingleton;

    public class MorseWindow : MonoBehaviour
    {
        [SerializeField] private Button nextButton;
        [SerializeField] private Button showTable;
        [SerializeField] private GameObject morseIntro;
        [SerializeField] private GameObject morseResults;
        [SerializeField] private GameObject morseContent;
        [SerializeField] private TextMeshProUGUI Results;
        [SerializeField] private TextMeshProUGUI morseWordHeader;
        [SerializeField] private TextMeshProUGUI morseWord;
        [SerializeField] private TMP_InputField input;
        [SerializeField] private GameObject buttons;
        [SerializeField] private GameObject table;
        [SerializeField] private Button dotButton;
        [SerializeField] private Button dashButton;
        [SerializeField] private Button pauseButton;

        [SerializeField] private Image showTableImage;
        [SerializeField] private Sprite morseTableButtonActive;
        [SerializeField] private Sprite morseTableButtonDisabled;

        [SerializeField] private Image morseTableImage;
        [SerializeField] private Sprite morseTableHorizontal;
        [SerializeField] private Sprite morseTableVertical;

        [SerializeField] private GameObject StartWindowPrefab;

        private MorseWord currentWord;
        public MorseController _morseController;
        private bool results = false;

        private GridLayoutGroup questionBodyLayoutGroup;
        private RectTransform questionBodyRectTransform;

        private List<string> Letters;
        private List<string> MorseLetters;

        private void Start()
        {
            _morseController = App.Instance.MorseController;
            nextButton.onClick.AddListener(OnClickNext);
            showTable.onClick.AddListener(ToggleTable);
            dotButton.onClick.AddListener(() => input.text += ".");
            dashButton.onClick.AddListener(() => input.text += "-");
            pauseButton.onClick.AddListener(() => input.text += " ");
            input.onValueChanged.AddListener(InputText);

            questionBodyLayoutGroup = buttons.GetComponent<GridLayoutGroup>();
            questionBodyRectTransform = morseContent.GetComponent<RectTransform>();
            Letters = new List<string> 
            {
                "\u0410", "\u0411", "\u0412", "\u0413",
                "\u0414", "\u0415", "\u0416", "\u0417",
                "\u0418", "\u0419", "\u041A", "\u041B",
                "\u041C", "\u041D", "\u041E", "\u041F",
                "\u0420", "\u0421", "\u0422", "\u0423",
                "\u0424", "\u0425", "\u0426", "\u0427",
                "\u0428", "\u0429", "\u042A", "\u042B",
                "\u042C", "\u042D", "\u042E", "\u042F",
            };

            MorseLetters = new List<string>
            {
                ".-", "-...", ".--", "--.",
                "-..", ".", "...-", "--..",
                "..", ".---", "-.-", ".-..",
                "--", "-.", "---", ".--.",
                ".-.", "...", "-", "..-",
                "..-.", "....", "-.-.", "---.",
                "----", "--.-", ".--.-.", "-.--",
                "-..-", "..-..", "..--", ".-.-",
            };

            if (_morseController.CurrentWord >= 0 && _morseController.CurrentWord < _morseController.ChosenWords.Count)
            {
                currentWord = _morseController.ChosenWords[_morseController.CurrentWord].Word;
            }
            SetupPanel();
        }

        private void Update()
        {
            UpdateOrientation();
        }

        private void OnDestroy()
        {
            nextButton.onClick.RemoveAllListeners();
            showTable.onClick.RemoveAllListeners();
            dotButton.onClick.RemoveAllListeners();
            dashButton.onClick.RemoveAllListeners();
            pauseButton.onClick.RemoveAllListeners();
            input.onValueChanged.RemoveAllListeners();
        }

        private void ToggleTable()
        {
            table.SetActive(!table.activeSelf);
            if (table.activeSelf)
            {
                showTableImage.sprite = morseTableButtonActive;
            }
            else
            {
                showTableImage.sprite = morseTableButtonDisabled;
            }
            
        }

        private void UpdateOrientation()
        {
            if (questionBodyRectTransform.rect.width > questionBodyRectTransform.rect.height)
            {
                morseTableImage.sprite = morseTableHorizontal;
                questionBodyLayoutGroup.constraintCount = 3;
            }
            else
            {
                morseTableImage.sprite = morseTableVertical;
                questionBodyLayoutGroup.constraintCount = 1;
            }
        }

        private void OnClickNext()
        {
            var correct = false;
            if (currentWord != null && input.text != "" && currentWord.ToEncrypt)
            {
                correct = _morseController.SetAnswer(ToWord(input.text));
            }
            else
            {
                correct = _morseController.SetAnswer(input.text);
            }

            currentWord = _morseController.NextQuestion();
            if (results)
            {
                App.CreateWindow(App.Instance.StartMenuPrefab);
                _morseController.ShuffleWords();
                Destroy(this);
            }
            else
            {
                SetupPanel();
            }
        }

        private void SetupPanel()
        {
            morseIntro.SetActive(false);
            morseContent.SetActive(false);
            morseResults.SetActive(false);
            showTable.gameObject.SetActive(false);

            if (!_morseController.MorseStarted)
            {
                morseIntro.SetActive(true);
            }
            else if (currentWord != null)
            {
                morseContent.SetActive(true);
                showTable.gameObject.SetActive(true);
                nextButton.interactable = false;
                SetupWord(currentWord);
            }
            else
            {
                morseResults.SetActive(true);
                results = true;
                SetupResults();
            }
        }

        private void SetupWord(MorseWord word)
        {
            input.text = "";

            if (word.ToEncrypt)
            {
                morseWordHeader.SetLocalizedString("Morse_Encrypt_Word_Label");
                morseWord.text = word.Word;
                buttons.SetActive(true);
            }
            else
            {
                morseWordHeader.SetLocalizedString("Morse_Decrypt_Word_Label");
                morseWord.text = ToMorse(word.Word);
                buttons.SetActive(false);
            }
        }

        private void InputText(string text)
        {
            if (text.Length == 0)
            {
                nextButton.interactable = false;
                return;
            }
            else
            {
                nextButton.interactable = true;
            }

            if (currentWord.ToEncrypt &&
                text[text.Length - 1] != '-' &&
                text[text.Length - 1] != '.' &&
                text[text.Length - 1] != ' ')
            {
                input.SetTextWithoutNotify(text.Substring(0, text.Length - 1));
            }

            if (!currentWord.ToEncrypt &&
                !char.IsLetter(text[text.Length - 1]))
            {
                input.SetTextWithoutNotify(text.Substring(0, text.Length - 1));
            }
        }

        private void SetupResults()
        {
            var correctAnswersCount = _morseController.ChosenWords.Count(item => item.Answer.ToUpper() == item.Word.Word.ToUpper());
            Results.text = correctAnswersCount + " / " + _morseController.ChosenWords.Count;
        }

        private string ToWord(string text)
        {
            var morseWords = text.Trim().Split(' ');
            var curWord = "";
            foreach (var word in morseWords)
            {
                var ind = MorseLetters.FindIndex(item => string.Equals(word, item, StringComparison.InvariantCultureIgnoreCase));
                if (ind >= 0 && ind < Letters.Count)
                {
                    curWord += Letters[ind];
                }
            }
            return curWord;
        }

        private string ToMorse(string text)
        {
            text = text.ToUpper();

            var sb = new StringBuilder();

            for (int i = 0; i < text.Length; i++)
            {
                var ind = Letters.FindIndex(item => string.Equals(text[i].ToString(), item, StringComparison.InvariantCultureIgnoreCase));
                if (ind >= 0 && ind < MorseLetters.Count)
                {
                    sb.Append(MorseLetters[ind]);

                    if (i != text.Length - 1)
                    {
                        sb.Append(' ');
                    }
                }
            }

            return sb.ToString();
        }
    }
}
