namespace Naval.UI
{
    using Naval.AR;
    using Naval.Model;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class ARAnimator : MonoBehaviour
    {
        [SerializeField] private SubtitleShowing _subtitles;

        [SerializeField] public Transform[] CharacterPositions = new Transform[5];
        [SerializeField] public Dictionary<int, Character> CharactersDict = new();

        private AudioPlayer _audioPlayer;
        private AudioSource _audioSource;

        private float currentPictureTimer = 0f;

        public Coroutine ARAnimationProfileRoutine;
        public Coroutine ARAnimationUnitRoutine;
        private List<Coroutine> _animationRoutines = new();

        private const float defaultSpeechDuration = 10f;

        private static readonly int IsTalking = UnityEngine.Animator.StringToHash("bTalk");

        private void Awake()
        {
            _audioPlayer = GetComponent<AudioPlayer>();
            _audioSource = GetComponent<AudioSource>();
        }

        public void PlayARAnimation(ARAnimationProfile profile)
        {
            ARAnimationProfileRoutine = StartCoroutine(ToPlayAR(profile));
        }

        public AudioClip StartAudioClip(ARAnimationProfile profile, ARAnimationSet animationSet)
        {
            if (animationSet.AudioClip == null)
            {
                return null;
            }

            var clip = animationSet.AudioClip;

            _audioPlayer.PlayAudio(clip, 0.5f);

            return clip;
        }

        protected private void ResetARAnimationUnitRoutine()
        {
            foreach (var animationRoutine in _animationRoutines)
            {
                StopCoroutine(animationRoutine);
            }

            _animationRoutines.Clear();

            if (ARAnimationUnitRoutine != null)
            {
                StopCoroutine(ARAnimationUnitRoutine);
                ARAnimationUnitRoutine = null;
            }
            
            if (_subtitles.gameObject.activeSelf)
            {
                _subtitles.HideSubtitle();
            }

            if (_audioSource != null && _audioSource.clip != null)
            {
                currentPictureTimer += (_audioSource.clip.length - _audioSource.time);
            }

            if (_audioPlayer != null)
            {
                _audioPlayer.StopAudio(0.5f);
            }
        }
        
        public void StopARAnimationProfileRoutine()
        {
            ResetARAnimationUnitRoutine();

            if (ARAnimationProfileRoutine != null)
            {
                StopCoroutine(ARAnimationProfileRoutine);
                ARAnimationProfileRoutine = null;
            }

            foreach (var character in CharactersDict)
            {
                Destroy(character.Value.CharacterObject);
            }

            CharactersDict.Clear();
        }

        private protected virtual IEnumerator ToPlayAR(ARAnimationProfile profile)
        {
            PrepareCharacters(profile);

            foreach (var animationSet in profile.ARAnimationSets)
            {
                ARAnimationUnitRoutine = StartCoroutine(ToPlayAnimationSet(profile, animationSet));
                while (ARAnimationUnitRoutine != null)
                {
                    yield return new WaitForEndOfFrame();
                }
            }

            ResetARAnimationUnitRoutine();
        }

        protected private IEnumerator ToPlayAnimationSet(ARAnimationProfile profile, ARAnimationSet animationSet)
        {
            yield return new WaitForSeconds(animationSet.PlaybackDelay);
            var animationDuration = PlayAnimation(profile, animationSet);
            var audioClip = StartAudioClip(profile, animationSet);
            var duration = audioClip != null ? audioClip.length : defaultSpeechDuration;

            if (PlayerPrefs.GetInt("subtitles") == 1 && animationSet.Subtitles != string.Empty && animationSet.Subtitles != "")
            {
                _subtitles.ShowSubtitle(animationSet.Subtitles, duration);
            }
            else if (audioClip == null)
            {
                duration = 0f;
            }

            yield return new WaitForSeconds(Mathf.Max(animationDuration, duration));

            foreach (var routine in _animationRoutines)
            {
                StopCoroutine(routine);
            }

            foreach (var character in CharactersDict)
            {
                character.Value.CharacterObject.transform.position = CharacterPositions[character.Value.CharacterPositionIndex].position;
            }

            ResetBoolAnimations();

            _animationRoutines.Clear();

            ARAnimationUnitRoutine = null;
        }

        private float PlayAnimation(ARAnimationProfile profile, ARAnimationSet animationSet)
        {
            ResetBoolAnimations();

            var duration = 0f;
            foreach (var animation in animationSet.AnimationUnits)
            {
                if (animation.CharacterIndex > profile.Characters.Length - 1)
                {
#if DEBUG
                    Debug.Log("Character index is out of range Characters array!");
#endif
                    continue;
                }

                var animator = CharactersDict[animation.CharacterIndex].CharacterObject.GetComponentInChildren<UnityEngine.Animator>();
                var animationDuration = SetAnimation(animation, animator);
                var moveDuration = MoveCharacter(animation);
                var maxDuration = Mathf.Max(animationDuration, moveDuration);

                if (duration < maxDuration)
                {
                    duration = maxDuration;
                }
            }
            
            return duration;
        }

        private float MoveCharacter(AnimationUnit animation)
        {
            var moveStep = animation.MoveDirection == MoveDirection.Left ? -1 : 1;

            if (animation.MoveType == MoveType.Walk)
            {
                var resultPositionIndex = CharactersDict[animation.CharacterIndex].CharacterPositionIndex + moveStep;
                if (resultPositionIndex >= 0 && resultPositionIndex <= CharacterPositions.Length - 1)
                {
                    _animationRoutines.Add(StartCoroutine(ToMove(moveStep, animation, animation.PlaybackDuration)));
                    return animation.PlaybackDuration + animation.PlaybackDelay;
                }
                else
                {
#if DEBUG
                    Debug.Log("Target position is wrong! (for " + CharactersDict[animation.CharacterIndex].CharacterObject.name + ")");
#endif
                }
            }
            else if (animation.MoveType == MoveType.Turn)
            {
                _animationRoutines.Add(StartCoroutine(ToTurn(moveStep, animation, animation.PlaybackDuration)));
                return animation.PlaybackDuration + animation.PlaybackDelay;
            }

            return 0f;
        }

        private IEnumerator ToMove(int moveStep, AnimationUnit animation, float duration)
        {
            var posIndex = CharactersDict[animation.CharacterIndex].CharacterPositionIndex;
            var character = CharactersDict[animation.CharacterIndex].CharacterObject;
            var targetPosition = CharacterPositions[posIndex + moveStep].position;
            var startPosition = character.transform.position;
            var timer = 0f;

            CharactersDict[animation.CharacterIndex] = new Character()
            {
                CharacterObject = CharactersDict[animation.CharacterIndex].CharacterObject,
                CharacterPositionIndex = posIndex + moveStep,
            };

            yield return new WaitForSeconds(animation.PlaybackDelay);

            while ((character.transform.position.x < CharacterPositions[posIndex + moveStep].position.x && moveStep > 0) ||
                (character.transform.position.x > CharacterPositions[posIndex + moveStep].position.x && moveStep < 0))
            {
                timer += Time.deltaTime;
                character.transform.position = Vector3.Lerp(startPosition, targetPosition, timer / duration);

                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator ToTurn(int moveStep, AnimationUnit animation, float duration)
        {
            var character = CharactersDict[animation.CharacterIndex].CharacterObject;
            var targetRotation = character.transform.rotation * Quaternion.AngleAxis(moveStep * 45f, character.transform.up);
            var startRotation = character.transform.rotation;
            var timer = 0f;

            yield return new WaitForSeconds(animation.PlaybackDelay);

            while (Quaternion.Angle(character.transform.rotation, targetRotation) > 1)
            {
                timer += Time.deltaTime / duration;

                character.transform.rotation = Quaternion.Slerp(startRotation, targetRotation, timer);

                yield return new WaitForEndOfFrame();
            }
        }

        private void ResetBoolAnimations()
        {
            foreach (var character in CharactersDict.Values)
            {
                var animator = character.CharacterObject.GetComponentInChildren<UnityEngine.Animator>();

                foreach (var parameter in animator.parameters)
                {
                    if (parameter.type == AnimatorControllerParameterType.Bool)
                    {
                        animator.SetBool(parameter.name, false);
                    }
                }
            }
        }

        private float SetAnimation(AnimationUnit animation, Animator animator)
        {
            if (animation.AnimationKey == string.Empty || 
                animation.AnimationKey == "" ||
                !animator.parameters.Any(item => item.name == animation.AnimationKey))
            {
                return 0f;
            }

            var duration = 0f;
            
            if (animation.AnimationType == AnimationType.Trigger)
            {
                duration = animator.GetCurrentAnimatorStateInfo(0).length;
            }

            StartCoroutine(ToAnimateWithDelay(animation, animator));

            return duration;
        }

        private IEnumerator ToAnimateWithDelay(AnimationUnit animation, Animator animator)
        {
            yield return new WaitForSeconds(animation.PlaybackDelay);

            switch (animation.AnimationType)
            {
                case AnimationType.Bool:
                    animator.SetBool(animation.AnimationKey, !animator.GetBool(animation.AnimationKey));
                    break;

                case AnimationType.Float:
#if DEBUG
                    Debug.Log("Not implemented realisation for float type!");
#endif
                    break;

                case AnimationType.Trigger:
                    animator.SetTrigger(animation.AnimationKey);
                    break;
            }
        }

        private protected void PrepareCharacters(ARAnimationProfile profile)
        {
            CharactersDict.Clear();

            var prefabCharDict = profile.Characters
                .Select((item, index) => new { index, item })
                .ToDictionary(item => item.index, item => item.item);

            for (var k = 0; k < CharacterPositions.Length; k++)
            {
                var characters = prefabCharDict
                        .Where(item => item.Value.CharacterPositionIndex == k)
                        .ToDictionary(item => item.Key, item => item.Value);

                foreach (Transform existCharacter in CharacterPositions[k])
                {
                    var coincidences = characters
                        .Where(item => existCharacter.name.Contains(item.Value.CharacterObject.name));

                    if (coincidences.Count() > 0)
                    {
                        CharactersDict.Add(coincidences.First().Key, 
                            new Character { CharacterObject = existCharacter.gameObject, CharacterPositionIndex = k });
                        characters.Remove(coincidences.First().Key);
                    }
                    else
                    {
                        Destroy(existCharacter.gameObject);
                    }
                }

                foreach (var character in characters)
                {
                    var newCharacter = Instantiate(character.Value.CharacterObject, CharacterPositions[k]);
                    CharactersDict.Add(character.Key, new Character { CharacterObject = newCharacter, CharacterPositionIndex = k });
                }
            }
        }
    }
}
