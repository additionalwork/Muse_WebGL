namespace Naval
{
    using System;
    using UnityEngine;

    public static class AuthManager
    {
        private static string _token;
        public static string Token
        {
            get => _token;
            set
            {
                _token = value;
                if (IsCorrectToken(value))
                {
                    Authorized = true;
                    LogonTimestamp = DateTime.Now;
                    PlayerPrefs.SetString("Token", value);
                    PlayerPrefs.SetString("LogonTimestamp", LogonTimestamp.ToString());
                    AppControllerSingleton.Authorize();
                }
            }
        }

        public static bool IsCorrectToken(string token)
        {
            return NumberMask.IsMatching(token, (ulong)(DateTime.Now.Day));
        }

        public static void TryToLoginWithPrefs()
        {
            if (Authorized)
            {
                return;
            }

            var token = PlayerPrefs.GetString("Token", "");
            var timestamp = PlayerPrefs.GetString("LogonTimestamp", "");
            if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(timestamp) && DateTime.TryParse(timestamp, out var res))
            {
                var dateDiff = DateTime.Now - res;
                if (dateDiff.TotalHours < 2)
                {
                    Token = token;
                    LogonTimestamp = res;
                    PlayerPrefs.SetString("LogonTimestamp", res.ToString());
                }
            }
        }

        public static void TryToLoginWithURL(string url)
        {
            if (Authorized)
            {
                return;
            }

            if (AppControllerSingleton.Instance.TryGetUrlQuery(url, out var urlQuery))
            {
                Token = urlQuery;
            }
        }

        public static void Logoff()
        {
            Token = string.Empty;
            Authorized = false;
            LogonTimestamp = DateTime.MinValue;
            AppControllerSingleton.Logoff();
            PlayerPrefs.DeleteKey("Token");
            PlayerPrefs.DeleteKey("LogonTimestamp");
        }

        public static bool Authorized { get; private set; }
        public static DateTime LogonTimestamp { get; private set; }
        
        public static INumberMask NumberMask;

        // TESTING ONLY
        public static string GetToken()
        {
            var token = NumberMask.ApplyMask((ulong)(DateTime.Now.Day));
            return token;
        }
    }
}