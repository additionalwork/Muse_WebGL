namespace Naval
{
    public interface INumberMask
    {
        public string ApplyMask(ulong value);
        public bool IsMatching(string value, ulong expected);
    }
}