namespace Naval
{
    public class NumberMask : INumberMask
    {
        private const ulong HeadMask = 18445;
        private const ulong HeadModulo = 1000000000000000;
        private const ulong TailMask = 56183;
        private const ulong TailModulo = 100000;
        private const ulong Key = 904629374023424;

        public string ApplyMask(ulong value)
        {
            return ((HeadMask * HeadModulo + value * TailModulo + TailMask)^Key).ToString();
        }

        public bool IsMatching(string value, ulong expected)
        {
            return value == ApplyMask(expected);
        }
    }
}