namespace Naval
{
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    public class SHA256Mask : INumberMask
    {
        public string ApplyMask(ulong value)
        {
            return string.Concat(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(value.ToString())).Select(item => item.ToString("x2")));
        }

        public bool IsMatching(string value, ulong expected)
        {
            return value == ApplyMask(expected);
        }
    }
}