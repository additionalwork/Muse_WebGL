namespace Naval
{
    public class TestMask : INumberMask
    {
        public string ApplyMask(ulong value)
        {
            return "12345";
        }

        public bool IsMatching(string value, ulong expected)
        {
            return value == ApplyMask(expected);
        }
    }
}