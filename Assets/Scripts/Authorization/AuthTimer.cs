namespace Naval
{
    using System;
    using TMPro;
    using UnityEngine;

    public class AuthTimer : MonoBehaviour
    {
        [SerializeField] private float TimerDuration = 60 * 60 * 2;
        [SerializeField] private TextMeshProUGUI TimerText;

        private void Update()
        {
            if (!AuthManager.Authorized)
            {
                TimerText.gameObject.SetActive(false);
                return;
            }

            if (!TimerText.gameObject.activeSelf)
            {
                TimerText.gameObject.SetActive(true);
            }

            var currentTimer = (float)(TimerDuration + (AuthManager.LogonTimestamp - DateTime.Now).TotalSeconds + 1);
            TimerText.text = $"{Mathf.FloorToInt(currentTimer / 3600):00}:{Mathf.FloorToInt(currentTimer / 60 % 60):00}:{Mathf.FloorToInt(currentTimer % 60):00}";

            if ((DateTime.Now - AuthManager.LogonTimestamp).TotalSeconds > TimerDuration)
            {
                AuthManager.Logoff();
            }
        }
    }
}