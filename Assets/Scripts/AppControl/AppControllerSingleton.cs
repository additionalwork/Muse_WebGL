namespace Naval
{
    using Naval.Model;
    using Naval.UI;
    using System.Collections;
    using System.Web;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class AppControllerSingleton : MonoBehaviour
    {
        [Header("Common")]
        [SerializeField] public Transform ActiveWindows;
        [SerializeField] public Transform ScreenCanvas;
        [SerializeField] public Transform BackgroundCanvas;
        [SerializeField] public AudioMixerController AudioMixerController;
        [SerializeField] public string URL = "/";
        [SerializeField] public string DefaultLocale;
        [SerializeField] public string[] Locales;
        [SerializeField] public string QueryKey;
        public LocalizationController LocalizationController;

        [Header("MainMenu")]
        [SerializeField] public MainMenu MainMenu;
        
        [Header("StartMenu")]
        [SerializeField] public GameObject StartMenuPrefab;

        [Header("ExcursionDefault")]
        [SerializeField] public GameObject DefaultGuidePrefab;
        [SerializeField] private string _rootExhibitDefaultProfileId = "45c12942-bc23-4f80-8a89-ea6deb529448";
        public ExcursionDefaultController ExcursionDefaultController { get; private set; }

        [Header("ExcursionInteractive")]
        [SerializeField] public GameObject InteractiveGuidePrefab;
        [SerializeField] private string _rootExhibitInteractiveProfileId = "a753ff83-4a06-4192-bbe1-0db4b724fe91";
        public ExcursionInteractiveController ExcursionInteractiveController { get; private set; }

        [Header("Quiz")]
        [SerializeField] public GameObject QuizPrefab;
        [SerializeField] private int _questionsCount = 10;
        public QuizController QuizController { get; private set; }

        [Header("Morse")]
        [SerializeField] public GameObject MorsePrefab;
        [SerializeField] private int _morseQuestionsCount = 10;
        public MorseController MorseController { get; private set; }

        [Header("Scheme")]
        [SerializeField] public GameObject SchemePrefab;

        [Header("Settings")]
        [SerializeField] public GameObject SettingsPrefab;

        [Header("QR")]
        [SerializeField] public GameObject QRPrefab;

        [Header("TimeOut")]
        [SerializeField] public GameObject TimeOutPrefab;

        public long AudioInMemory = 0L;
        public long ImageInMemory = 0L;

        public static AppControllerSingleton Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
#if DEBUG
                Debug.LogError("More than one " + GetType().ToString() + " in scene detected!");
#endif
                Destroy(this);
            }
            else
            {
                Instance = this;
            }

#if UNITY_EDITOR
            URL = "https://cms.trunk.basilevs.tech/";
#else
            if (Application.absoluteURL.Contains("naval.trunk.basilevs.tech") ||
                Application.absoluteURL.Contains("naval.myhost.com") ||
                Application.absoluteURL.Contains("192.168.0.12") ||
                Application.absoluteURL.Contains("localhost") ||
                string.IsNullOrEmpty(Application.absoluteURL))
            {
                URL = "https://cms.trunk.basilevs.tech/";
            }
            else
            {
                URL = "https://cms.staging.basilevs.tech/";
            }
#endif

            ResourceManager.Instance._url = URL;
            AuthManager.NumberMask = new SHA256Mask();
            InitiateRepositories();
        }

        public void OnDestroy()
        {
            Instance = null;
        }

        private IEnumerator Start()
        {
            ResetPlayerPrefsToDefault();
            Instance.MainMenu.Logoff();
            AuthManager.TryToLoginWithPrefs();
            AuthManager.TryToLoginWithURL(Application.absoluteURL);
#if UNITY_EDITOR
            AuthManager.Token = AuthManager.GetToken();
#endif

            yield return LocalizationController.CurrentStringRepository.ToInitiateLocale();

            yield return QuizController.QuizRepository.ToInitiateData();
            yield return ExcursionDefaultController.ExcursionDefaultRepository.ToInitiateData();
            yield return ExcursionInteractiveController.ExcursionInteractiveRepository.ToInitiateData();

            if (TryGetUrlQuery(Application.absoluteURL, out var query))
            {
                var qrWindow = CreateWindow(QRPrefab);
                qrWindow.GetComponentInChildren<QRWindow>().CallbackQR(query);
            }

            yield return ResourceManager.Instance.TryToPreload(
                Instance.ExcursionDefaultController.GetExhibit(Instance._rootExhibitDefaultProfileId).NextExhibit);

#if UNITY_EDITOR
            Debug.LogWarning("AudioInMemory: " + EditorUtility.FormatBytes(AudioInMemory));
            Debug.LogWarning("ImageInMemory: " + EditorUtility.FormatBytes(ImageInMemory));
#endif
        }

        private void LocaleChanged()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void ResetPlayerPrefsToDefault()
        {
            PlayerPrefs.SetFloat("volume", 1f);
            PlayerPrefs.SetInt("subtitles", 1);
            Time.timeScale = 1f;
        }

        public void InitiateRepositories()
        {
            InitiateFromWeb();
        }

        private void InitiateFromWeb()
        {
            var locale = PlayerPrefs.GetString("currentLocale", null);
            locale ??= DefaultLocale;
            var stringLocaleRepo = new StringLocaleRepository(locale,
                    new WebServerSource<LocalizeStringWebProfile, ISerializer>(
                        URL,
                        "api/v0/usertype/locale-string",
                        new JsonSerializerNewtonsoft(),
                        profile => Debug.Log("ru")));
            LocalizationController = new LocalizationController(locale, Locales, stringLocaleRepo);
            LocalizationController.LocaleChangedEvent += LocaleChanged;

            var excursionDefaultRepository =
                new WebExcursionDefaultRepository(
                    new WebServerSource<ExcursionDefaultWebProfile, ISerializer>(
                        URL,
                        $"api/v0/usertype/excursions/{_rootExhibitDefaultProfileId}",
                        new JsonSerializerNewtonsoftExhibits(),
                        profile => Debug.Log("ExhibitDefaultProfile")));
            ExcursionDefaultController = new ExcursionDefaultController(excursionDefaultRepository);

            var excursionInteractiveRepository =
                new WebExcursionInteractiveRepository(
                    new WebServerSource<ExcursionInteractiveWebProfile, ISerializer>(
                        URL,
                        $"api/v0/usertype/excursions/{_rootExhibitInteractiveProfileId}",
                        new JsonSerializerNewtonsoftExhibits(),
                        profile => Debug.Log("ExhibitInteractiveProfile")));
            ExcursionInteractiveController = new ExcursionInteractiveController(excursionInteractiveRepository);

            var quizRepository = 
                new WebQuizRepository(
                    new WebServerSource<QuestionWebProfile, ISerializer>(
                        URL,
                        "api/v0/usertype/quiz",
                        new JsonSerializerNewtonsoft(),
                        profile => Debug.Log("QuestionWebProfile")));
            QuizController = new QuizController(_questionsCount, quizRepository);

            MorseController = new MorseController(_morseQuestionsCount, quizRepository);
        }

        public static GameObject CreateWindow(GameObject windowPrefab)
        {
            ClearActiveWindows();

            return Instantiate(windowPrefab, Instance.ActiveWindows);
        }

        public static void ClearActiveWindows()
        {
            foreach (Transform window in Instance.ActiveWindows)
            {
                Destroy(window.gameObject);
            }

            Instance.MainMenu.gameObject.SetActive(false);
        }

        public static void ToggleMainMenu()
        {
            Instance.MainMenu.gameObject.SetActive(!Instance.MainMenu.gameObject.activeSelf);
        }

        public static void StartExcursionDefaultById(string exhibitId, bool withExhibitList = false)
        {
            var exhibit = Instance.ExcursionDefaultController.GetExhibit(exhibitId);

            StartExcursionDefaultFrom(exhibit, withExhibitList);
        }

        public static void StartExcursionDefaultFrom(ExhibitDefault exhibit, bool withExhibitList = false)
        {
            var guide = CreateWindow(Instance.DefaultGuidePrefab);
            var guideComponent = guide.GetComponent<Guide>();
            guideComponent.CurrentExhibit = exhibit;
            ResetExhibitsFromNode(exhibit);

            if (withExhibitList)
            {
                guide.GetComponent<GuideMenu>().SetWindowExhibits();
            }
            else
            {
                guideComponent.PlayExcursion(exhibit);
            }
        }

        public static void StartNewExcursionDefault()
        {
            var guide = CreateWindow(Instance.DefaultGuidePrefab);
            var exhibit = Instance.ExcursionDefaultController.GetExhibit(Instance._rootExhibitDefaultProfileId);
            guide.GetComponent<Guide>().PlayExcursion(exhibit);
        }

        public static void StartNewExcursionInteractive()
        {
            var guide = CreateWindow(Instance.InteractiveGuidePrefab);
            var exhibit = Instance.ExcursionInteractiveController.GetExhibit(Instance._rootExhibitInteractiveProfileId);
            guide.GetComponent<Guide>().PlayExcursion(exhibit);
        }

        public static void StartTask(string exhibitId, int taskIndex)
        {
            var guide = CreateWindow(Instance.InteractiveGuidePrefab);
            var exhibit = Instance.ExcursionInteractiveController.GetExhibit(exhibitId);
            guide.GetComponent<InteractiveGuide>().PlayTask(exhibit, taskIndex);
        }

        public static void ResetExcursionProgress(AbstractRepository repository = null)
        {
            var activeGuide = Instance.ActiveWindows.GetComponentInChildren<Guide>();

            if (repository == null)
            {
                repository = activeGuide is InteractiveGuide ?
                    Instance.ExcursionInteractiveController.ExcursionInteractiveRepository :
                    Instance.ExcursionDefaultController.ExcursionDefaultRepository;
            }

            foreach (var exhibit in repository.GetAllData<ExhibitDefault>())
            {
                exhibit.IsFinished = false;
            }

            if (activeGuide != null && activeGuide is InteractiveGuide)
            {
                foreach (var exhibit in repository.GetAllData<ExhibitInteractive>())
                {
                    var exhibitInteractive = exhibit;

                    if (exhibitInteractive.TaskUnits.Length > 0)
                    {
                        for (int i = 0; i < exhibitInteractive.TaskUnits.Length; i++)
                        {
                            exhibitInteractive.TaskUnits[i].IsFinished = false;
                            exhibitInteractive.TaskUnits[i].IsCorrect = false;
                            exhibitInteractive.TaskUnits[i].IsSkipped = false;
                        }
                    }
                }
            }
        }

        private static void ResetExhibitsFromNode(ExhibitDefault exhibit)
        {
            exhibit.IsFinished = false;

            if (exhibit.NextExhibit != null)
            {
                ResetExhibitsFromNode(exhibit.NextExhibit);
            }
        }

        public static void Authorize()
        {
            Instance.MainMenu.Authorize();
        }

        public static void Logoff()
        {
            Instance.MainMenu.Logoff();
            CreateWindow(Instance.TimeOutPrefab);
        }

        public bool TryGetUrlQuery(string url, out string query)
        {
            var splitted = url.Split('?');
            if (splitted.Length < 2)
            {
                query = string.Empty;
                return false;
            }
            var parsedQuery = HttpUtility.ParseQueryString(splitted[1]);
            query = parsedQuery.Get(QueryKey);
            return !string.IsNullOrEmpty(query);
        }
    }
}
