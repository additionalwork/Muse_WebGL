namespace Naval 
{
    using Naval.Model;
    using System;
    using System.Linq;
    using UnityEngine;

    public class LocalizationController 
    {
        public StringLocaleRepository CurrentStringRepository => StringRepositories[CurrentLocale];
        public string CurrentLocale { get; private set; }
        public string[] Locales { get; private set; }
        public event Action LocaleChangedEvent;

        private LocaleContainer<StringLocaleRepository> StringRepositories { get; }

        public LocalizationController(string defaultLocale, string[] locales, StringLocaleRepository defaultStringRepo)
        {
            PlayerPrefs.SetString("currentLocale", defaultLocale);
            CurrentLocale = defaultLocale;
            Locales = locales;
            StringRepositories = new LocaleContainer<StringLocaleRepository>(defaultStringRepo);
        }
        
        public void LocaleSelectByName(string name)
        {
            PlayerPrefs.SetString("currentLocale", name);
            CurrentLocale = name;
            LocaleChangedEvent?.Invoke();
        }

        public int GetLocaleId(string name)
        {
            return Array.IndexOf(Locales, name);
        }

        public string GetLocalizedString(string entryKey)
        {
            if (CurrentStringRepository == null || entryKey == null)
            {
                return entryKey;
            }

            var value = (LocalizedString)CurrentStringRepository[entryKey];
            return value != null ? value.Value : entryKey;
        }
    }
}
