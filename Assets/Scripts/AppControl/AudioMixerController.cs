namespace Naval
{
    using UnityEngine;
    using UnityEngine.Audio;

    public class AudioMixerController : MonoBehaviour
    {
        [SerializeField] private AudioMixer audioMixer;

        private void Start()
        {
            SetMasterVolume(PlayerPrefs.GetFloat("volume", 1));
        }
        
        public static float PercentToDecibel(float percent)
        {
            return 20 * Mathf.Log10(percent);
        }

        public void SetMasterVolume(float percent)
        {
            audioMixer.SetFloat("MasterVolume", PercentToDecibel(percent));
        }
    }
}
