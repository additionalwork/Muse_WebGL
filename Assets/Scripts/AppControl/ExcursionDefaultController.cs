namespace Naval
{
    using Naval.Model;

    public class ExcursionDefaultController
    {
        public ExhibitDefault CurrentExhibit { get; set; }

        public AbstractRepository ExcursionDefaultRepository { get; private set; }

        public ExcursionDefaultController(AbstractRepository excursionDefaultRepository)
        {
            ExcursionDefaultRepository = excursionDefaultRepository;
        }

        public ExhibitDefault GetExhibit(string exhibitId)
        {
            return ExcursionDefaultRepository.GetData(exhibitId) as ExhibitDefault;
        }
    }
}
