namespace Naval
{
    using Naval.Model;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class MorseController
    {
        public List<WordData> ChosenWords { get; private set; } = new();
        public bool MorseStarted { get; private set; } = false;
        public int CurrentWord { get; private set; } = -1;
        public AbstractRepository MorseRepository { get; private set; }

        public int _questionsCount;

        public MorseController(int questionsCount, AbstractRepository morseRepository)
        {
            _questionsCount = questionsCount;
            MorseRepository = morseRepository;
            MorseRepository.DataLoadedEvent += () => ResetMorse();
        }

        public void ResetMorse()
        {
            MorseStarted = false;
            CurrentWord = -1;
            ShuffleWords();
        }

        public void ShuffleWords()
        {
            var words = ((MorseRepository.GetData("Morse") as QuizQuestion).QuestionType as MorseQuestionType).Words;

            bool[] indices = new bool[words.Length];
            ChosenWords.Clear();

            if (_questionsCount > words.Length)
            {
                _questionsCount = words.Length;
            }

            while (ChosenWords.Count < _questionsCount && indices.Any(i => !i))
            {
                var randomInt = Random.Range(0, words.Length);
                if (!indices[randomInt])
                {
                    indices[randomInt] = true;
                    ChosenWords.Add(new WordData { Word = new MorseWord { Word = words[randomInt], ToEncrypt = Random.value > 0.5f } });
                }
            }
        }

        public MorseWord NextQuestion()
        {
            if (ChosenWords.Count == 0)
            {
#if DEBUG
                Debug.LogError("No words are loaded!");
#endif
                return null;
            }

            if (CurrentWord == -1)
            {
                MorseStarted = true;
            }

            CurrentWord++;

            if (CurrentWord < _questionsCount)
            {
                return ChosenWords[CurrentWord].Word;
            }
            else if (CurrentWord == _questionsCount)
            {
                return null;
            }

            ResetMorse();
            return null;
        }

        public bool SetAnswer(string answer)
        {
            if (CurrentWord < _questionsCount && CurrentWord >= 0)
            {
                ChosenWords[CurrentWord].Answer = answer;
                var correct = ChosenWords[CurrentWord].Word.Word.ToUpper().Equals(answer.ToUpper());
                return correct;
            }

            return false;
        }

        public class WordData
        {
            public MorseWord Word;
            public string Answer;
        }
    }
}
