namespace Naval
{
    using Naval.Model;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuizController
    {
        public List<QuestionData> ChosenQuestions { get; private set; } = new();
        public bool QuizStarted { get; private set; } = false;
        public int CurrentQuestion { get; private set; } = -1;
        public AbstractRepository QuizRepository { get; private set; }

        public int _questionsCount;

        public QuizController(int questionsCount, AbstractRepository quizRepository)
        {
            _questionsCount = questionsCount;
            QuizRepository = quizRepository;
            QuizRepository.DataLoadedEvent += () => ResetQuiz();
        }

        public void ResetQuiz()
        {
            QuizStarted = false;
            CurrentQuestion = -1;
            ShuffleQuestions();
        }

        public void ShuffleQuestions()
        {
            var questions = QuizRepository.GetAllData<QuizQuestion>(item => ((QuizQuestion)item).Id != "Morse")
                .ToArray();

            if (questions.Length < _questionsCount)
            {
                _questionsCount = questions.Length;
            }

            bool[] indices = new bool[questions.Length];
            ChosenQuestions.Clear();

            while (ChosenQuestions.Count < _questionsCount)
            {
                var randomInt = Random.Range(0, questions.Length);
                if (!indices[randomInt])
                {
                    indices[randomInt] = true;
                    var answers = GetShuffleAnswers(questions[randomInt].IncorrectAnswers
                        .Select(item => new Answer { AnswerKey = item, IsCorrectAnswer = false })
                        .Append(new Answer { AnswerKey = questions[randomInt].CorrectAnswer, IsCorrectAnswer = true })
                        .ToArray());

                    ChosenQuestions.Add(new QuestionData
                    {
                        Id = questions[randomInt].Id,
                        Question = questions[randomInt].QuestionType,
                        Answers = answers,
                    });
                }
            }
        }

        public QuestionData NextQuestion()
        {
            if (ChosenQuestions.Count == 0)
            {
#if DEBUG
                Debug.LogError("No question are loaded!");
#endif
                return null;
            }

            if (CurrentQuestion == -1)
            {
                QuizStarted = true;
            }

            CurrentQuestion++;

            if (CurrentQuestion < _questionsCount)
            {
                return ChosenQuestions[CurrentQuestion];
            }
            else if (CurrentQuestion == _questionsCount)
            {
                return null;
            }

            ResetQuiz();
            return null;
        }

        private Answer[] GetShuffleAnswers(params Answer[] answers)
        {
            System.Random random = new System.Random();
            for (int i = answers.Length - 1; i >= 1; i--)
            {
                int j = random.Next(i + 1);
                (answers[i], answers[j]) = (answers[j], answers[i]);
            }

            return answers;
        }

        public class QuestionData
        {
            public string Id;
            public AbstractQuizQuestionType Question;
            public Answer[] Answers;
        }

        public class Answer
        {
            public Button Button;
            public bool IsSelected;
            public bool IsCorrectAnswer;
            public string AnswerKey;
        }
    }
}
