namespace Naval
{
    using Naval.Model;
    using TMPro;
    using UnityEngine;

    public static class GameObjectLocalizeExtension
    {
        public static void SetLocalizedString(this TextMeshProUGUI obj, string textKey)
        {
            if (obj.TryGetComponent<LocalizedStringEvent>(out var localizedString))
            {
                localizedString.SetString(textKey);
            }
            else
            {
#if DEBUG
                Debug.Log($"Missing LocalizedStringEvent ({obj.gameObject.name})!");
#endif
            }
        }

        public static void SetLocalizedStringText(this TextMeshProUGUI obj, string textKey)
        {
            if (obj.TryGetComponent<LocalizedStringEvent>(out var localizedString))
            {
                localizedString.SetString(textKey, true);
            }
            else
            {
                obj.text = textKey;
            }
        }
    }
}
