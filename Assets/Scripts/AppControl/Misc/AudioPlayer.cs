namespace Naval
{
    using System.Collections;
    using UnityEngine;

    public class AudioPlayer : MonoBehaviour
    {
        [SerializeField] private AudioClip audioClipForDestroy;
        public AudioSource audioSource;
        private Coroutine coroutine;

        public bool IsMuted;

        public void Awake()
        {
            if (!TryGetComponent(out audioSource))
            {
#if DEBUG
                Debug.LogError("Object has no AudioSource!");
#endif
            }
        }

        public void PlayAudio(AudioClip audioClip, float smoothDuration = 0f, float targetVolume = 1f)
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
            audioSource.pitch = 1f;
            coroutine = StartCoroutine(ToPlayVolumeSmooth(audioClip, smoothDuration, targetVolume));

        }

        public void PlayAudio(AudioClip audioClip, (float min, float max) pitchRange, float smoothDuration = 0f, float targetVolume = 1f)
        {
            PlayAudio(audioClip, smoothDuration, targetVolume);
            audioSource.pitch = Random.Range(pitchRange.min, pitchRange.max);
        }

        public void StopAudio(float smoothDuration = 0f)
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }

            coroutine = StartCoroutine(ToStopVolumeSmooth(smoothDuration));
            audioSource.Stop();
            audioSource.volume = 1;
        }

        public bool ToggleMute()
        {
            IsMuted = !IsMuted;
            audioSource.mute = IsMuted;
            return IsMuted;
        }

        public void OnDestroy()
        {
            if (audioClipForDestroy != null)
            {
                AudioSource.PlayClipAtPoint(audioClipForDestroy, transform.position);
            }
        }

        private IEnumerator ToStopVolumeSmooth(float duration)
        {
            float startVolume = audioSource.volume;
            var currentTimer = 0f;
            while (currentTimer < duration)
            {
                currentTimer += Time.deltaTime;
                audioSource.volume = Mathf.Lerp(startVolume, 0f, currentTimer / duration);
                yield return new WaitForEndOfFrame();
            }

            yield return null;
        }

        private IEnumerator ToPlayVolumeSmooth(AudioClip audioClip, float duration, float targetVolume)
        {
            yield return ToStopVolumeSmooth(duration);

            audioSource.clip = audioClip;
            audioSource.Play();

            var currentTimer = 0f;
            while (currentTimer < duration)
            {
                currentTimer += Time.deltaTime;
                audioSource.volume = Mathf.Lerp(0f, targetVolume, currentTimer / duration);
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
