﻿namespace Naval
{
    using UnityEngine;

    public class InitializationOrientation : MonoBehaviour
    {
        private static InitializationOrientation instance;

        private void Awake()
        {
            if (instance != null)
            {
#if DEBUG
                Debug.LogError("More than one ExcursionController in scene detected!");
#endif
                return;
            }

            instance = this;
        }

        public static InitializationOrientation Instance
        {
            get
            {
                if (instance == null)
                {
#if DEBUG
                    Debug.LogError("There's no ExcursionController in scene detected!");
#endif
                    return null;
                }

                return instance;
            }
        }

        public void Start()
        {
            MobileOrientationDetector.Init();
        }
    }
}
