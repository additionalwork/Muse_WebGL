﻿namespace Naval
{
    using AOT;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MobileOrientationDetector
    {
        public delegate void dlgOrientationChange(int angle);
        public static event dlgOrientationChange orientationChange;
        private static bool isFullScreenSupport = false;

        [DllImport("__Internal")]
        private static extern int JS_OrientationDetectorLib_Init(Action<int> eventHandler);

        [DllImport("__Internal")]
        private static extern int JS_OrientationDetectorLib_GetOrientation();

        [DllImport("__Internal")]
        private static extern void JS_OrientationDetectorLib_FullScreen();

        [DllImport("__Internal")]
        private static extern void JS_OrientationDetectorLib_ExitFullScreen();

        [DllImport("__Internal")]
        private static extern void JS_OrientationDetectorLib_Lock();

        [DllImport("__Internal")]
        private static extern void JS_OrientationDetectorLib_Unlock();


        [MonoPInvokeCallback(typeof(Action<int>))]
        private static void OnOrientationChanged(int angle)
        {
            if (angle == -999)
            {
                isFullScreenSupport = false;
            }
            orientationChange.Invoke(angle);
        }

        public static void Init()
        {
#if !UNITY_EDITOR
            isFullScreenSupport = true;

            var res = JS_OrientationDetectorLib_Init(OnOrientationChanged);
            if (res == -1)
            {
                throw new Exception("This device does not support Screen Orientation API");
            }
#endif
        }

        public static int GetOrientation()
        {
            var angle = JS_OrientationDetectorLib_GetOrientation();
            return angle;
        }

        public static void FullScreen()
        {
            if (!isFullScreenSupport) return;
            JS_OrientationDetectorLib_FullScreen();
        }

        public static void ExitFullScreen()
        {
            if (!isFullScreenSupport) return;
            JS_OrientationDetectorLib_ExitFullScreen();
        }

        public static void ScreenLock()
        {
            if (!isFullScreenSupport) return;
            JS_OrientationDetectorLib_Lock();
        }

        public static void ScreenUnlock()
        {
            JS_OrientationDetectorLib_Unlock();
        }
    }
}
