namespace Naval
{
    using Naval.Model;

    public class ExcursionInteractiveController
    {
        public AbstractRepository ExcursionInteractiveRepository { get; private set; }

        public ExcursionInteractiveController(AbstractRepository excursionInteractiveRepository)
        {
            ExcursionInteractiveRepository = excursionInteractiveRepository;
        }

        public ExhibitInteractive GetExhibit(string exhibitId)
        {
            return ExcursionInteractiveRepository.GetData(exhibitId) as ExhibitInteractive;
        }
    }
}
