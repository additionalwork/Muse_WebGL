namespace Naval.Model
{
    using System;
    using System.Collections;
    using UnityEngine;

    public class ResourceUnit : IDisposable
    {
        public bool IsResourceLoaded { get; private set; }
        public bool IsResourceLoading { get; private set; }
        public object Value;
        public event Action<object> ResourceLoadedEvent;

        public Guid Guid { get; private set; }
        public readonly string _url;
        public readonly ResourceType _type;
        private readonly IResourceLoader _loader;

        public ResourceUnit(string urlPrefix, string url, ResourceType type)
        {
            Guid = Guid.NewGuid();
            _url = urlPrefix + url;
            _type = type;

            if (string.IsNullOrEmpty(url))
            {
                IsResourceLoaded = true;
                Guid = default;
            }

            if (_type == ResourceType.Image)
            {
                _loader = new ImageLoader();
            }
            else if (_type == ResourceType.Audio)
            {
                _loader = new AudioLoader();
            }
        }

        public IEnumerator Load()
        {
            var retry = 0;
            while (IsResourceLoading && retry < ResourceManager.Instance.LoadRetryAttpemtsCount)
            {
                retry++;
                yield return new WaitForSeconds(ResourceManager.Instance.LoadRetryPauseDuration);
            }

            if (IsResourceLoading)
            {
#if DEBUG
                Debug.Log($"Resource is not loaded! ({_url})");
#endif
                yield break;
            }

            if (string.IsNullOrEmpty(_url))
            {
#if DEBUG
                Debug.Log($"Trying to load resource with empty url!");
#endif
                yield break;
            }

            if (IsResourceLoaded)
            {
                yield break;
            }

            IsResourceLoading = true;

            yield return _loader.DownloadResource(_url, (res) => Value = res);

            if (Value != null)
            {
                Loaded();
            }
            else
            {
#if DEBUG
                Debug.Log($"DownloadResource on url \"{_url}\" failed");
#endif
            }
        }

        private void Loaded()
        {
            ResourceLoadedEvent?.Invoke(Value);
            IsResourceLoaded = true;
            IsResourceLoading = false;
        }

        public Sprite GetSprite()
        {
            if (_type == ResourceType.Image && Value != null)
            {
                return (Sprite)Value;
            }

            return default;
        }

        public AudioClip GetAudio()
        {
            if (_type == ResourceType.Audio && Value != null)
            {
                return (AudioClip)Value;
            }

            return default;
        }

        public void Dispose()
        {
            if (_type == ResourceType.Image && Value != null)
            {
                var sprite = (Sprite)Value;
                UnityEngine.Object.Destroy(sprite.texture);
                UnityEngine.Object.Destroy(sprite);
            }

            if (_type == ResourceType.Audio && Value != null)
            {
                UnityEngine.Object.Destroy((AudioClip)Value);
            }

            IsResourceLoaded = false;
            IsResourceLoading = false;
        }

        public enum ResourceType
        {
            Image,
            Audio
        }
    }
}
