namespace Naval.Model
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class ResourceManager
    {
        private static ResourceManager instance;
        private readonly Dictionary<Guid, ResourceUnit> _resources = new();

        private readonly Dictionary<string, Guid> _urlDictionary = new();
        private readonly Dictionary<string, List<Guid>> _exhibitResources = new();

        private readonly List<Guid> _quizResources = new();

        public readonly int LoadRetryAttpemtsCount = 1;
        public readonly float LoadRetryPauseDuration = 1;
        public readonly int CountToPreload = 3;
        public string _url;
        
        private bool IsPreloading;
        private Queue<ExhibitDefault> LastPreloadExhibits = new();
        private string LastPreloadId;

        private ResourceManager() { }

        public static ResourceManager Instance
        {
            get
            {
                instance ??= new ResourceManager();

                return instance;
            }
        }

        public Guid AddExhibitAudio(string exhibitId, string audioKey)
        {
            var audioGuid = AddResource(audioKey, ResourceUnit.ResourceType.Audio);

            if (audioGuid == default)
            {
#if DEBUG
                Debug.Log($"Given an empty audio url for exhibit \"{exhibitId}\"");
#endif
                return default;
            }

            if (!_exhibitResources.ContainsKey(exhibitId))
            {
                _exhibitResources.Add(exhibitId, new List<Guid>());
            }

            _exhibitResources[exhibitId].Add(audioGuid);

            return audioGuid;
        }

        public Guid AddExhibitImage(string exhibitId, string url)
        {
            var guid = AddResource(url, ResourceUnit.ResourceType.Image);

            if (guid == default)
            {
#if DEBUG
                Debug.Log($"Given an empty image url for exhibit \"{exhibitId}\"");
#endif
                return guid;
            }

            if (!_exhibitResources.ContainsKey(exhibitId))
            {
                _exhibitResources.Add(exhibitId, new List<Guid>());
            }

            _exhibitResources[exhibitId].Add(guid);

            return guid;
        }

        public Guid AddQuizImage(string url)
        {
            var guid = AddResource(url, ResourceUnit.ResourceType.Image);

            if (guid == default)
            {
#if DEBUG
                Debug.Log("Given an empty url for quiz");
#endif
                return guid;
            }

            _quizResources.Add(guid);

            return guid;
        }

        public Guid AddResource(string url, ResourceUnit.ResourceType type)
        {
            if (string.IsNullOrEmpty(url))
            {
                url = string.Empty;
            }

            if (_urlDictionary.ContainsKey(url))
            {
                return _urlDictionary[url];
            }

            var res = new ResourceUnit(_url, url, type);

            _resources.Add(res.Guid, res);
            _urlDictionary.Add(url, res.Guid);

            return res.Guid;
        }

        public ResourceUnit GetResource(Guid guid)
        {
            return _resources.GetValueOrDefault(guid);
        }

        public bool IsLoadedExhibit(string id)
        {
            if (!_exhibitResources.ContainsKey(id))
            {
                return true;
            }

            return _exhibitResources[id].All(guid => GetResource(guid).IsResourceLoaded);
        }

        public IEnumerator LoadExhibit(string id)
        {
            if (!_exhibitResources.ContainsKey(id))
            {
                yield break;
            }

            foreach (var guid in _exhibitResources[id])
            {
                yield return LoadResource(GetResource(guid));
            }
        }

        public IEnumerator LoadQuiz()
        {
            foreach (var quizGuid in _quizResources)
            {
                yield return LoadResource(GetResource(quizGuid));
            }
        }

        public IEnumerator LoadResource(ResourceUnit unit)
        {
            if (unit == null)
            {
#if DEBUG
                Debug.Log("Passed null ref on ResourceUnit");
#endif
                yield break;
            }

            yield return unit.Load();
        }

        public IEnumerator TryToPreload(ExhibitDefault exhibit)
        {
            if (IsPreloading || exhibit.IsRoot)
            {
                yield break;
            }

            var curExhibit = exhibit;
            if (!exhibit.IsHall && exhibit.Parent != null)
            {
                curExhibit = exhibit.Parent;
            }

            if (LastPreloadExhibits.Contains(curExhibit) && LastPreloadId == curExhibit.Id)
            {
                var nextHall = curExhibit.NextExhibit;
                while (nextHall != null && !nextHall.IsHall)
                {
                    nextHall = nextHall.NextExhibit;
                }

                if (nextHall != null)
                {
                    yield return PreloadHall(nextHall);
                }
            }
            else if (!LastPreloadExhibits.Contains(curExhibit))
            {
                yield return PreloadHall(curExhibit);
            }
        }

        private IEnumerator PreloadHall(ExhibitDefault exhibit)
        {
            if (LastPreloadExhibits.Count > CountToPreload - 1 && LastPreloadExhibits.TryDequeue(out var lastExhibit))
            {
                ReleaseHallResources(lastExhibit);
            }

            LastPreloadExhibits.Enqueue(exhibit);
            LastPreloadId = exhibit.Id;
            yield return PreloadHallResources(exhibit);
        }

        private IEnumerator PreloadHallResources(ExhibitDefault exhibit)
        {
            if (!exhibit.IsHall)
            {
                yield break;
            }

            IsPreloading = true;

            if (_exhibitResources.ContainsKey(exhibit.Id))
            {
                foreach (var guid in _exhibitResources[exhibit.Id])
                {
                    yield return LoadResource(GetResource(guid));
                }
            }

            foreach (var child in exhibit.ChildExhibits)
            {
                if (_exhibitResources.ContainsKey(child.Id))
                {
                    foreach (var guid in _exhibitResources[child.Id])
                    {
                        yield return LoadResource(GetResource(guid));
                    }
                }
            }
            IsPreloading = false;
        }

        public void ReleaseHallResources(ExhibitDefault exhibit)
        {
            if (!exhibit.IsHall)
            {
                return;
            }

            if (_exhibitResources.ContainsKey(exhibit.Id))
            {
                foreach (var guid in _exhibitResources[exhibit.Id])
                {
                    ReleaseResource(guid);
                }
            }

            foreach (var child in exhibit.ChildExhibits)
            {
                if (_exhibitResources.ContainsKey(child.Id))
                {
                    foreach (var guid in _exhibitResources[child.Id])
                    {
                        ReleaseResource(guid);
                    }
                }
            }
        }

        public void ReleaseResource(Guid guid)
        {
            _resources.TryGetValue(guid, out var resource);
            if (resource != null)
            {                
                resource.Dispose();
            }
        }
    }
}
