using System;
using System.Collections;
using UnityEngine;

namespace Naval.Model
{
    public interface IResourceLoader
    {
        public IEnumerator DownloadResource(string url, Action<object> callback);
    }
}
