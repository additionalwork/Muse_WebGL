using Naval.Editor;
using System;
using UnityEngine.Networking;
using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Naval.Model
{
    public class ImageLoader : IResourceLoader
    {
        public IEnumerator DownloadResource(string url, Action<object> callback)
        {
            UnityWebRequest www;
            UnityWebRequestAsyncOperation operation;
            try
            {
                www = UnityWebRequestTexture.GetTexture(url);
                operation = www.SendWebRequest();
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                yield break;
            }

            yield return new WaitUntil(() => operation.isDone);

#if UNITY_EDITOR
            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError($"Failed: {www.error}");
                yield break;
            }
#endif
            var texture = DownloadHandlerTexture.GetContent(www);

            texture.Compress(false);

            var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
            www.Dispose();

#if UNITY_EDITOR
            var memorySize = UnityEngine.Profiling.Profiler.GetRuntimeMemorySizeLong(texture);
            Debug.LogWarning("Image memory size: " + url + " - " + EditorUtility.FormatBytes(memorySize));
            AppControllerSingleton.Instance.ImageInMemory += memorySize;
#endif

            callback.Invoke(sprite);
        }
    }
}
