using UnityEngine.Networking;
using UnityEngine;
using System.Collections;
using System;
using UnityEditor;

namespace Naval.Model
{
    public class AudioLoader : IResourceLoader
    {
        public IEnumerator DownloadResource(string url, Action<object> callback)
        {
            UnityWebRequest www;
            UnityWebRequestAsyncOperation operation;
            try
            {
                www = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.MPEG);
                operation = www.SendWebRequest();
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                yield break;
            }
            
            yield return new WaitUntil(() => operation.isDone);

            if (www.result != UnityWebRequest.Result.Success)
            {
#if UNITY_EDITOR
                Debug.LogError($"Failed: {www.error}");
#endif
                yield break;
            }

            ((DownloadHandlerAudioClip)www.downloadHandler).compressed = true;
            var audioClip = DownloadHandlerAudioClip.GetContent(www);

#if UNITY_EDITOR
            var memorySize = UnityEngine.Profiling.Profiler.GetRuntimeMemorySizeLong(audioClip);
            Debug.LogWarning("Audio memory size: " + url + " - " + EditorUtility.FormatBytes(memorySize));
            AppControllerSingleton.Instance.AudioInMemory += memorySize;
#endif

            callback.Invoke(audioClip);
        }
    }
}
