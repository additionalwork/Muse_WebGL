namespace Naval.Model
{
    public class MorseWords : IData
    {
        public string Id { get; set; }
        public MorseWord[] Words { get; set; }
    }

    public class MorseWord
    {
        public string Word { get; set; }
        public bool ToEncrypt { get; set; }
    }
}
