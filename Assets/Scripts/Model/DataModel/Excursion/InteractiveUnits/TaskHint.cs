namespace Naval.Model
{
    using System;

    [Serializable]
    public struct TaskHint
    {
        public string Name;
        public PictureUnit[] PictureUnits;
        public SpeechUnit[] SpeechUnits;
        public AnimationUnit[] AnimationUnits;
    }
}
