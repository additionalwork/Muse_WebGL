namespace Naval.Model
{
    using System;

    [Serializable]
    public struct TaskUnit
    {
        public TaskType Type;
        public string Name;
        public string Text;
        public string CorrectAnswer;
        public TaskHint[] Hints;
        public bool IsFinished;
        public bool IsCorrect;
        public bool IsSkipped;

        // only for TaskType.Select
        public string[] IncorrectAnswers;
    }
}
