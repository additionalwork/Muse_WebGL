namespace Naval.Model
{
    public class ExhibitDefault : IData
    {
        public bool HasParent { get => Parent != null; }
        public ExhibitDefault[] ChildExhibits { get; set; }
        public ExhibitDefault Parent { get; set; }
        public string Id { get; set; }
        public bool IsRoot { get; set; }
        public string Description { get; set; }
        public bool IsHall { get; set; }
        public string Name { get; set; }
        public PictureUnit[] PictureUnits { get; set; }
        public SpeechUnit[] SpeechUnits { get; set; }
        public AnimationUnit[] AnimationUnits { get; set; }
        public FAQUnit[] FAQUnits { get; set; }
        public bool IsFinished { get; set; }
        public Character[] Characters { get; set; }
        public ExhibitDefault PrevExhibit { get; set; }
        public ExhibitDefault NextExhibit { get; set; }
        public ResourceUnit[] Resources { get; set; }
    }
}
