namespace Naval.Model
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct AnimationUnit
    {
        [Header("Common:")]
        public int CharacterIndex;
        public float PlaybackDelay;
        public float PlaybackDuration;

        [Header("Animation:")]
        public AnimationType AnimationType;
        public string AnimationKey;

        [Header("Moving:")]
        public MoveType MoveType;
        public MoveDirection MoveDirection;
    }

    public enum MoveDirection
    {
        Left,
        Right,
    }

    public enum MoveType
    {
        None,
        Turn,
        Walk,
    }

    public enum AnimationType
    {
        Trigger,
        Float,
        Bool,
    }
}
