namespace Naval.Model
{
    using System;

    [Serializable]
    public struct FAQUnit
    {
        public string Name;
        public string Text;
        public Guid Image;
    }
}
