﻿namespace Naval.Model
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct Character
    {
        [Range(0, 4, order = 1)]
        public int CharacterPositionIndex;
        public GameObject CharacterObject;
    }
}
