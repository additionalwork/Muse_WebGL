namespace Naval.Model
{
    using System;

    [Serializable]
    public struct PictureUnit
    {
        public Guid Image;
        public float AppearanceDelay;
    }
}
