namespace Naval.Model
{
    using System;

    [Serializable]
    public struct SpeechUnit
    {
        public Guid AudioGuid;
        public string Subtitles;
        public float PlaybackDelay;
        public AnimationUnit[] AnimationUnits;
    }
}
