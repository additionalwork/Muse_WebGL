namespace Naval.Model
{
    using Naval.Editor;
    using System;
    using UnityEngine;

    [Serializable]
    public class QuizQuestion : IData
    {
        public string Id;
        [SerializeReference]
        [SubclassPicker]
        public AbstractQuizQuestionType QuestionType;
        public string CorrectAnswer;
        public string[] IncorrectAnswers;
    }
}
