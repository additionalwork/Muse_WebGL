namespace Naval.Model
{
    using System;

    [Serializable]
    public class TextQuestionType : AbstractQuizQuestionType
    {
        public string Text;
    }
}
