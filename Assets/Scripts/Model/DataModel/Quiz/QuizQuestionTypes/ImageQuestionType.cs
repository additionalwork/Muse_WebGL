namespace Naval.Model
{
    using System;
    using UnityEngine;

    [Serializable]
    public class ImageQuestionType : AbstractQuizQuestionType
    {
        public Guid Image;
    }
}
