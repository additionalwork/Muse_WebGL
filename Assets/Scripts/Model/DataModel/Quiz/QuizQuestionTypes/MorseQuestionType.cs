namespace Naval.Model
{
    using System;

    [Serializable]
    public class MorseQuestionType : AbstractQuizQuestionType
    {
        public string[] Words;
    }
}
