namespace Naval.Model
{
    public class LocalizedString : ILocalizedData
    {
        public string Value { get; set; }
    }
}
