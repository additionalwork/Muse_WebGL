namespace Naval.Model
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class StringLocaleRepository : AbstractLocaleRepository
    {
        public StringLocaleRepository(string localeId, IDataSourceTemplate<IDataProfile> dataSource)
            : base(localeId, dataSource)
        {
        }

        protected private override IEnumerator InitiateLocale(IEnumerable<IDataProfile> rawData)
        {
            foreach (var profile in rawData)
            {
                var currentProfile = (LocalizeStringWebProfile)profile;

                if (_localeDictionary.ContainsKey(currentProfile.Name))
                {
#if DEBUG
                    Debug.LogError(this.GetType().ToString() + " - an item with the same key has already been added!");
#endif
                }
                else
                {
                    var value = new LocalizedString() { Value = currentProfile.StringValue };
                    _localeDictionary.Add(currentProfile.Name, value);
                }
            }

            yield break;
        }
    }
}
