using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;

namespace Naval.Model
{
    public abstract class AbstractLocaleRepository
    {
        public event Action LocaleLoadedEvent;
        private protected readonly IDataSourceTemplate<IDataProfile> _dataSource;
        public string LocaleId { get; private set; }
        public bool IsLocaleLoaded { get; private set; }
        private protected readonly Dictionary<string, ILocalizedData> _localeDictionary = new();
        public ILocalizedData this[string key]
        {
            get
            {
                if (_localeDictionary.TryGetValue(key, out var value))
                {
                    return value;
                }

                return default;
            }
        }

        public AbstractLocaleRepository(string localeId, IDataSourceTemplate<IDataProfile> dataSource)
        {
            _dataSource = dataSource;
            IsLocaleLoaded = false;
            LocaleId = localeId;
        }

        public IEnumerator ToInitiateLocale()
        {
            yield return _dataSource.LoadData();

            if (_dataSource.DataSet != null && _dataSource.DataSet.Count() > 0)
            {
                yield return InitiateLocale(_dataSource.DataSet);
                LocaleLoadedEvent?.Invoke();
                IsLocaleLoaded = true;
            }
        }

        protected private abstract IEnumerator InitiateLocale(IEnumerable<IDataProfile> rawData);
    }
}
