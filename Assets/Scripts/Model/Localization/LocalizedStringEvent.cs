using TMPro;
using UnityEngine;

namespace Naval.Model
{
    public class LocalizedStringEvent : MonoBehaviour
    {
        [SerializeField] private string Entry;
        [SerializeField] private bool IsPlainText;
        [SerializeField] private TextMeshProUGUI Text;

        public void Start()
        {
            if (AppControllerSingleton.Instance == null)
            {
                return;
            }

            var repos = AppControllerSingleton.Instance.LocalizationController.CurrentStringRepository;
            repos.LocaleLoadedEvent += UpdateString;

            if (repos.IsLocaleLoaded)
            {
                UpdateString();
            }
        }

        public void SetString(string entry, bool isPlainText = false)
        {
            Entry = entry;
            IsPlainText = isPlainText;
            UpdateString();
        }

        private void UpdateString()
        {
            if (IsPlainText)
            {
                Text.text = Entry;
            }
            else
            {
                Text.text = AppControllerSingleton.Instance.LocalizationController.GetLocalizedString(Entry);
                if (string.IsNullOrEmpty(Text.text))
                {
                    Text.text = Entry;
                }
            }
        }
    }
}
