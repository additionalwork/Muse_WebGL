namespace Naval.Model
{
    using System.Collections.Generic;

    public class LocaleContainer<T>
        where T : AbstractLocaleRepository
    {
        private Dictionary<string, T> _localeData = new();

        public T this[string localeId]
        {
            get
            {
                if (_localeData.TryGetValue(localeId, out var value))
                {
                    return value;
                }

                return default;
            }
        }

        public LocaleContainer(params T[] repositories)
        {
            foreach (var repository in repositories)
            {
                AddLocale(repository.LocaleId, repository);
            }
        }

        public void AddLocale(string locale, T rep)
        {
            _localeData.Add(locale, rep);
        }
    }
}
