namespace Naval.Model
{
    using System.Collections.Generic;
    
    public abstract class LinksStorage
    {
        public List<string> TextureLinks;
        public List<string> AudioLinks;
    }
}