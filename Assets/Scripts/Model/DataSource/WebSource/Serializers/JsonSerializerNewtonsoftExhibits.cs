namespace Naval.Model
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class JsonSerializerNewtonsoftExhibits : ISerializer
    {
        public WebProfileCollection<T> Deserialize<T>(string json) where T : IDataProfile
        {
            var profiles = new WebProfileCollection<T>();
            profiles.profiles = new List<T>
            {
                JsonConvert.DeserializeObject<SingleExhibitWebProfile<T>>(json).Value
            };

            return profiles;
        }
    }
}
