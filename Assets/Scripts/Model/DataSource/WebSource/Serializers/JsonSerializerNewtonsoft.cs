namespace Naval.Model
{
    using Newtonsoft.Json;

    public class JsonSerializerNewtonsoft : ISerializer
    {
        public WebProfileCollection<T> Deserialize<T>(string json) where T : IDataProfile
        {
            return JsonConvert.DeserializeObject<WebProfileCollection<T>>(json);
        }
    }
}
