namespace Naval.Model
{
    using UnityEngine;

    public class JsonSerializerDefault : ISerializer
    {
        public WebProfileCollection<T> Deserialize<T>(string json) where T : IDataProfile
        {
            return JsonUtility.FromJson<WebProfileCollection<T>>(json);
        }
    }
}
