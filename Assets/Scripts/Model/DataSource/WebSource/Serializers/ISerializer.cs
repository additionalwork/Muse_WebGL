namespace Naval.Model
{
    public interface ISerializer
    {
        public WebProfileCollection<T> Deserialize<T>(string json) where T : IDataProfile;
    }
}