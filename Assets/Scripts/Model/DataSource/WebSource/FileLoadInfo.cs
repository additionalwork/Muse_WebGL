namespace Naval.Model
{
    public class FileLoadInfo : IDataProfile
    {
        public string Url;

        public FileType Type;
    }
}