namespace Naval.Model
{
    using Naval.Editor;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.Networking;

    public class WebServerSource<T, TSerializer> : IDataSourceTemplate<IDataProfile>
        where T : IDataProfile
        where TSerializer : ISerializer
    {
        public string URL { get; }
        private readonly TSerializer _serializer;
        private readonly string _configFileName;
        private readonly Action<IDataProfile> _loadingAction;
        private readonly Dictionary<Guid, dynamic> _dynamicData = new();

        public WebServerSource(string URL, string configFileName, TSerializer serializer, Action<IDataProfile> loadingAction)
        {
            this.URL = URL;
            _configFileName = configFileName;
            _serializer = serializer;
            _loadingAction = loadingAction;
        }
        
        public IEnumerable<IDataProfile> DataSet { get; set; }
        public IEnumerator LoadData()
        {
            string configFile = string.Empty;
            yield return GetTextWebRequest(_configFileName, str => configFile = str);
            if (configFile != string.Empty)
            {
                DataSet = _serializer.Deserialize<T>(configFile).profiles
                    .Select(item => (IDataProfile)item);
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogError(_configFileName + " deserialize failed!");
#endif
            }
        }

        private IEnumerator GetTextWebRequest(string configFileName, Action<string> callback)
        {
            Debug.Log(URL + configFileName);
            using var www = UnityWebRequest.Get(URL + configFileName);
            www.SetRequestHeader("Accept-Language", AppControllerSingleton.Instance.LocalizationController.CurrentLocale);

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
#if UNITY_EDITOR
                Debug.Log(www.error);
#endif
                yield break;
            }
            else
            {
                callback.Invoke(www.downloadHandler.text);
            }
        }
    }
}
