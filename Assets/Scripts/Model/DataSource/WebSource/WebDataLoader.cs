namespace Naval.Model
{
    using Naval.Editor;
    using System;
    using System.Collections;
    using System.IO;
    using System.Threading.Tasks;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.Networking;

    public static class WebDataLoader
    {
        public static IEnumerator GetTexture(string uri, Action<Sprite> callback)
        {
            var www = UnityWebRequestTexture.GetTexture(uri);
#if UNITY_EDITOR
            www.certificateHandler = new CertificateFake();
#endif
            var operation = www.SendWebRequest();
            yield return new WaitUntil(() => operation.isDone);
            var texture = DownloadHandlerTexture.GetContent(www);

            callback(Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero));
        }

        public static IEnumerator GetAudioClip(string url, AudioType type, Action<AudioClip> callback)
        {
            var www = UnityWebRequestMultimedia.GetAudioClip(url, type);
#if UNITY_EDITOR
            www.certificateHandler = new CertificateFake();
#endif
            var operation = www.SendWebRequest();
            yield return operation;

            callback(GetCheckedDownloader<DownloadHandlerAudioClip>(www).audioClip);
        }

        private static T GetCheckedDownloader<T>(UnityWebRequest www) where T : DownloadHandler
        {
            return www.result == UnityWebRequest.Result.Success ? (T) www.downloadHandler : throw new InvalidOperationException(www.error);
        }

        public static async Task<Sprite> GetTextureAsync(string url)
        {
            try
            {
                using var www = UnityWebRequestTexture.GetTexture(url, false);

                Debug.Log(url);

                var operation = www.SendWebRequest();

                while (!operation.isDone)
                    await Task.Yield();

                if (www.result != UnityWebRequest.Result.Success)
                    Debug.LogError($"Failed: {www.error}");

                var texture = DownloadHandlerTexture.GetContent(www);
                texture.Compress(false);
#if UNITY_EDITOR
                var memorySize = UnityEngine.Profiling.Profiler.GetRuntimeMemorySizeLong(texture);
                Debug.LogWarning("Texture memory size: " + url + " - " + EditorUtility.FormatBytes(memorySize));
                AppControllerSingleton.Instance.ImageInMemory += memorySize;
#endif
                var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);

                return sprite;
            }
            catch (Exception ex)
            {
                Debug.LogError($"{nameof(GetTextureAsync)} failed: {ex.Message}");
                return default;
            }
        }

        public static async Task<AudioClip> GetAudioAsync(string url, AudioType type)
        {
            try
            {
                using var uwr = UnityWebRequestMultimedia.GetAudioClip(url, type);

                Debug.Log(url);

                var operation = uwr.SendWebRequest();

                while (!operation.isDone)
                    await Task.Yield();
#if UNITY_EDITOR
                if (uwr.result != UnityWebRequest.Result.Success)
                    Debug.LogError($"Failed: {uwr.error}");
#endif
                ((DownloadHandlerAudioClip)uwr.downloadHandler).compressed = true;
                var audioClip = DownloadHandlerAudioClip.GetContent(uwr);
#if UNITY_EDITOR
                var memorySize = UnityEngine.Profiling.Profiler.GetRuntimeMemorySizeLong(audioClip);
                Debug.LogWarning("Audio memory size: " + url + " - " + EditorUtility.FormatBytes(memorySize));
                AppControllerSingleton.Instance.AudioInMemory += memorySize;
#endif
                return audioClip;
            }
            catch (Exception ex)
            {
#if UNITY_EDITOR
                Debug.LogError($"{nameof(GetAudioAsync)} failed: {ex.Message}");
#endif
                return default;
            }
        }
    }
}
