namespace Naval.Model
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class WebProfileCollection<T>
        where T : IDataProfile
    {
        //[JsonProperty("profiles")]
        public List<T> profiles;
    }
}
