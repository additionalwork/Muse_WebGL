namespace Naval.Model
{
    using Newtonsoft.Json;
    using System;

    [Serializable]
    public class ExcursionHallInteractiveWebProfile : ExcursionHallDefaultWebProfile
    {
        [JsonProperty("exhibits-default")]
        public new ExhibitInteractiveWebProfile[] ChildExhibits;
    }
}
