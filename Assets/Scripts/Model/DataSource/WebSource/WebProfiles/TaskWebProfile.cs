namespace Naval.Model
{
    using System;

    [Serializable]
    public class TaskWebProfile : IDataProfile
    {
        public TaskType Type;
        public string Name;
        public string Text;
        public string CorrectAnswer;
        public TaskHint[] Hints;

        // only for TaskType.Select
        public string[] IncorrectAnswers;
    }
}
