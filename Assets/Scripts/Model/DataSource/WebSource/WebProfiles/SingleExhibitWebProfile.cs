namespace Naval.Model
{
    using System;

    [Serializable]
    public class SingleExhibitWebProfile<T>
        where T : IDataProfile
    {
        public string Key;
        public T Value;
    }
}
