namespace Naval.Model
{
    using System;

    [Serializable]
    public struct PictureUnitWebProfile
    {
        public string ImageName;
        public float AppearanceDelay;
    }
}
