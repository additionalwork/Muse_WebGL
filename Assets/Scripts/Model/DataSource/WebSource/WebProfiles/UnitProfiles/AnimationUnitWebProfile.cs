namespace Naval.Model
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct AnimationUnitWebProfile
    {
        [Header("Common:")]
        public int CharacterIndex;
        public float PlaybackDelay;
        public float PlaybackDuration;

        [Header("Animation:")]
        public int AnimationType;
        public string AnimationKey;

        [Header("Moving:")]
        public int MoveType;
        public int MoveDirection;
    }
}
