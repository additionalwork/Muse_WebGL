﻿namespace Naval.Model
{
    using System;
    using UnityEngine;

    [Serializable]
    public struct CharacterWebProfile
    {
        [Range(0, 4, order = 1)]
        public int CharacterPositionIndex;
        public string CharacterObjectName;
    }
}
