namespace Naval.Model
{
    using System;

    [Serializable]
    public struct FAQUnitWebProfile
    {
        public string Name;
        public string Text;
        public string ImageName;
    }
}
