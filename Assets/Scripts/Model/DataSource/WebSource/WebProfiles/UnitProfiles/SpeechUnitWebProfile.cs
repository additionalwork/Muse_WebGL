namespace Naval.Model
{
    using System;

    [Serializable]
    public struct SpeechUnitWebProfile
    {
        public string URLValue;
        public string Subtitles;
        public float PlaybackDelay;
        public AnimationUnitWebProfile[] AnimationUnits;
    }
}
