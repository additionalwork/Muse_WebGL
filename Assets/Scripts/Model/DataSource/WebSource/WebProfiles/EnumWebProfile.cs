namespace Naval.Model
{
    using System;

    [Serializable]
    public struct EnumWebProfile
    {
        public int EnumNumber;
        public string Name;
    }
}
