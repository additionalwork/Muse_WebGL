namespace Naval.Model
{
    using System;

    [Serializable]
    public class ExcursionDefaultWebProfile : IDataProfile
    {
        public string Id;
        public string Name;
        public string Descripton;
        public ExcursionHallDefaultWebProfile[] halls;
    }
}
