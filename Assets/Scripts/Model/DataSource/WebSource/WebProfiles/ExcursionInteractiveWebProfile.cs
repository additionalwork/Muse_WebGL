namespace Naval.Model
{
    using System;

    [Serializable]
    public class ExcursionInteractiveWebProfile : ExcursionDefaultWebProfile
    {
        public new ExcursionHallInteractiveWebProfile[] halls;
    }
}
