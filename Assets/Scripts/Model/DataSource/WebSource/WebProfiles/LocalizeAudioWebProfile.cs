namespace Naval.Model
{
    using System;

    [Serializable]
    public class LocalizeAudioWebProfile : IDataProfile
    {
        public string Key;
        public string TableName; // localization table name
        public string URLValue;
    }
}
