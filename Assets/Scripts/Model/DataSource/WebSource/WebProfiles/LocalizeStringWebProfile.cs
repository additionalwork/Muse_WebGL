namespace Naval.Model
{
    using System;

    [Serializable]
    public class LocalizeStringWebProfile : IDataProfile
    {
        public string Name;
        public string StringValue;
    }
}
