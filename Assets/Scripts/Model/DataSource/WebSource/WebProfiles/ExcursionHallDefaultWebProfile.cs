namespace Naval.Model
{
    using Newtonsoft.Json;
    using System;

    [Serializable]
    public class ExcursionHallDefaultWebProfile : IDataProfile
    {
        public string Id;
        public string Name;
        public string Descripton;

        [JsonProperty("exhibits-default")]
        public ExhibitDefaultWebProfile[] ChildExhibits;
    }
}
