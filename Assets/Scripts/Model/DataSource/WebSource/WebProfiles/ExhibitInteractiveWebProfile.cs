namespace Naval.Model
{
    using System;

    [Serializable]
    public class ExhibitInteractiveWebProfile : ExhibitDefaultWebProfile
    {
        public new ExhibitInteractiveWebProfile[] ChildExhibits;
        public TaskUnitWebProfile[] Tasks;
    }
}
