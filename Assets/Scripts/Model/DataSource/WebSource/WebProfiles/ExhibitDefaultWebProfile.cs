namespace Naval.Model
{
    using System;

    [Serializable]
    public class ExhibitDefaultWebProfile : IDataProfile
    {
        public string Id;
        public string Name;
        public string Descripton;
        public bool IsHall;
        public PictureUnitWebProfile[] PictureUnits;
        public SpeechUnitWebProfile[] SpeechUnits;
        public FAQUnitWebProfile[] FAQUnits;
        public CharacterWebProfile[] Characters;
        public ExhibitDefaultWebProfile[] ChildExhibits;
    }
}
