namespace Naval.Model
{
    using System;

    [Serializable]
    public struct TaskHintWebProfile
    {
        public string Name;
        public PictureUnitWebProfile[] PictureUnits;
        public SpeechUnitWebProfile[] SpeechUnits;
        public AnimationUnitWebProfile[] AnimationUnits;
    }
}
