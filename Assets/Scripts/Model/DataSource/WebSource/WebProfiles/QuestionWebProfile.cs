namespace Naval.Model
{
    using System;

    [Serializable]
    public class QuestionWebProfile : IDataProfile
    {
        public string Id;
        public string Name;
        public string QuestionTitle;
        public EnumWebProfile QuizQuestionType;
        public string EntityValue;
        public string CorrectAnswer;
        public string[] IncorrectAnswers;
    }

    public enum QuestionTypeWebEnum
    {
        None,
        Text,
        Image,
        Morse
    }
}
