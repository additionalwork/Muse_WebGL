using System.Collections;
using System.Collections.Generic;

namespace Naval.Model
{
    public interface IDataSourceTemplate<T> where T : IDataProfile
    {
        public IEnumerable<T> DataSet { get; set; }
        public IEnumerator LoadData();
    }
}