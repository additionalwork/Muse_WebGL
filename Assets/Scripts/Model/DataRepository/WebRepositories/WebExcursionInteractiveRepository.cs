namespace Naval.Model
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class WebExcursionInteractiveRepository : WebExcursionDefaultRepository
    {
        private ExhibitInteractive _prevExhibit;

        public WebExcursionInteractiveRepository(IDataSourceTemplate<IDataProfile> dataSource) 
            : base(dataSource)
        {
        }

        protected private override IEnumerator InitiateData(IEnumerable<IDataProfile> rawData)
        {
            var root = new ExhibitInteractive();
            yield return InitiateExhibit(root, NormalizeProfile(rawData.First() as ExcursionInteractiveWebProfile), null);
            Root = root;
        }

        private ExhibitInteractiveWebProfile NormalizeProfile(ExcursionInteractiveWebProfile currentProfile)
        {
            return new ExhibitInteractiveWebProfile
            {
                Id = currentProfile.Id,
                Name = currentProfile.Name,
                Descripton = currentProfile.Descripton,
                ChildExhibits = currentProfile.halls.Select(hall => NormalizeProfile(hall)).ToArray(),
            };
        }

        private ExhibitInteractiveWebProfile NormalizeProfile(ExcursionHallInteractiveWebProfile currentProfile)
        {
            return new ExhibitInteractiveWebProfile
            {
                Id = currentProfile.Id,
                Name = currentProfile.Name,
                Descripton = currentProfile.Descripton,
                ChildExhibits = currentProfile.ChildExhibits,
                IsHall = true,
            };
        }

        protected private override IEnumerator InitiateExhibit(ExhibitDefault exhibit, ExhibitDefaultWebProfile profile, ExhibitDefault parent)
        {
            var currentProfile = (ExhibitInteractiveWebProfile)profile;
            var newExhibit = (ExhibitInteractive)exhibit;

            newExhibit.Id = currentProfile.Id;
            newExhibit.Name = currentProfile.Name;
            newExhibit.Description = currentProfile.Descripton;
            newExhibit.IsHall = currentProfile.IsHall;
            newExhibit.SpeechUnits = ParseSpeechUnits(currentProfile.SpeechUnits, currentProfile);
            newExhibit.FAQUnits = ParseFAQUnits(currentProfile.FAQUnits, currentProfile);
            newExhibit.Characters = ParseCharacter(currentProfile.Characters);
            newExhibit.PictureUnits = ParsePictureUnits(currentProfile.PictureUnits, currentProfile);

            if (currentProfile.Tasks != null)
            {
                newExhibit.TaskUnits = currentProfile.Tasks
                    .Select(profile => new TaskUnit
                    {
                        Type = (TaskType)profile.Type.EnumNumber,
                        Name = profile.Name,
                        Text = profile.Text,
                        CorrectAnswer = profile.CorrectAnswer,
                        IncorrectAnswers = profile.IncorrectAnswers,
                        Hints = profile.Hints == null ? new TaskHint[0] :
                            profile.Hints
                            .Select(hint => new TaskHint()
                            {
                                Name = hint.Name,
                                PictureUnits = ParsePictureUnits(hint.PictureUnits, currentProfile),
                                SpeechUnits = ParseSpeechUnits(hint.SpeechUnits, currentProfile),
                                AnimationUnits = ParseAnimationUnits(hint.AnimationUnits),
                            })
                            .ToArray(),
                    })
                    .ToArray();
            }
            else
            {
                newExhibit.TaskUnits = new TaskUnit[0];
            }

            newExhibit.PrevExhibit = _prevExhibit;
            _prevExhibit = newExhibit;

            if (newExhibit.PrevExhibit != null)
            {
                newExhibit.PrevExhibit.NextExhibit = newExhibit;
            }

            if (parent != null)
            {
                newExhibit.Parent = parent;
            }
            else
            {
                newExhibit.IsRoot = true;
            }

            if (currentProfile.ChildExhibits == null)
            {
                currentProfile.ChildExhibits = new ExhibitInteractiveWebProfile[0];
            }

            var children = new ExhibitInteractive[currentProfile.ChildExhibits.Length];
            for (int i = 0; i < currentProfile.ChildExhibits.Length; i++)
            {
                var newChild = new ExhibitInteractive();
                yield return InitiateExhibit(newChild, currentProfile.ChildExhibits[i], newExhibit);
                children[i] = newChild;
            }
            newExhibit.ChildExhibits = children;

            if (_dataDictionary.ContainsKey(newExhibit.Id))
            {
#if DEBUG
                Debug.LogError(this.GetType().ToString() + " - an item with the same key has already been added!");
#endif
            }
            else
            {
                _dataDictionary.Add(newExhibit.Id, newExhibit);
            }

            yield break;
        }
    }
}
