namespace Naval.Model
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    public class WebExcursionDefaultRepository : AbstractExcursionRepository
    {
        private ExhibitDefault _prevExhibit;

        public WebExcursionDefaultRepository(IDataSourceTemplate<IDataProfile> dataSource) 
            : base(dataSource)
        {
        }

        protected private override IEnumerator InitiateData(IEnumerable<IDataProfile> rawData)
        {
            var root = new ExhibitDefault();
            yield return InitiateExhibit(root, NormalizeProfile(rawData.First() as ExcursionDefaultWebProfile), null);
            Root = root;
        }

        private ExhibitDefaultWebProfile NormalizeProfile(ExcursionDefaultWebProfile currentProfile)
        {
            return new ExhibitDefaultWebProfile
            {
                Id = currentProfile.Id,
                Name = currentProfile.Name,
                Descripton = currentProfile.Descripton,
                ChildExhibits = currentProfile.halls.Select(hall => NormalizeProfile(hall)).ToArray(),
            };
        }

        private ExhibitDefaultWebProfile NormalizeProfile(ExcursionHallDefaultWebProfile currentProfile)
        {
            return new ExhibitDefaultWebProfile
            {
                Id = currentProfile.Id,
                Name = currentProfile.Name,
                Descripton = currentProfile.Descripton,
                ChildExhibits = currentProfile.ChildExhibits,
                IsHall = true,
            };
        }

        protected private virtual IEnumerator InitiateExhibit(ExhibitDefault newExhibit, ExhibitDefaultWebProfile profile, ExhibitDefault parent)
        {
            var currentProfile = (ExhibitDefaultWebProfile)profile;

            newExhibit.Id = currentProfile.Id;
            newExhibit.Name = currentProfile.Name;
            newExhibit.Description = currentProfile.Descripton;
            newExhibit.IsHall = currentProfile.IsHall;
            newExhibit.SpeechUnits = ParseSpeechUnits(currentProfile.SpeechUnits, currentProfile);
            newExhibit.FAQUnits = ParseFAQUnits(currentProfile.FAQUnits, currentProfile);
            newExhibit.Characters = ParseCharacter(currentProfile.Characters);
            newExhibit.PictureUnits = ParsePictureUnits(currentProfile.PictureUnits, currentProfile);

            newExhibit.PrevExhibit = _prevExhibit;
            _prevExhibit = newExhibit;

            if (newExhibit.PrevExhibit != null)
            {
                newExhibit.PrevExhibit.NextExhibit = newExhibit;
            }

            if (parent != null)
            {
                newExhibit.Parent = parent;
            }
            else
            {
                newExhibit.IsRoot = true;
            }

            if (currentProfile.ChildExhibits == null)
            {
                currentProfile.ChildExhibits = new ExhibitDefaultWebProfile[0];
            }

            var children = new ExhibitDefault[currentProfile.ChildExhibits.Length];
            for (int i = 0; i < currentProfile.ChildExhibits.Length; i++)
            {
                var newChild = new ExhibitDefault();
                yield return InitiateExhibit(newChild, currentProfile.ChildExhibits[i], newExhibit);
                children[i] = newChild;
            }
            newExhibit.ChildExhibits = children;

            if (_dataDictionary.ContainsKey(newExhibit.Id))
            {
#if DEBUG
                Debug.LogError(this.GetType().ToString() + " - an item with the same key has already been added!");
#endif
            }
            else
            {
                _dataDictionary.Add(newExhibit.Id, newExhibit);
            }

            yield break;
        }

        private protected PictureUnit[] ParsePictureUnits(PictureUnitWebProfile[] profiles, ExhibitDefaultWebProfile currentProfile)
        {
            if (profiles == null) return new PictureUnit[0];

            return profiles
                .Select(profile => new PictureUnit()
                {
                    Image = ResourceManager.Instance.AddExhibitImage(currentProfile.Id, profile.ImageName),
                    AppearanceDelay = profile.AppearanceDelay,
                })
                .ToArray();
        }

        private protected AnimationUnit[] ParseAnimationUnits(AnimationUnitWebProfile[] profiles)
        {
            if (profiles == null) return new AnimationUnit[0];

            return profiles
                .Select(anim => new AnimationUnit
                {
                    CharacterIndex = anim.CharacterIndex,
                    PlaybackDelay = anim.PlaybackDelay,
                    PlaybackDuration = anim.PlaybackDuration,
                    AnimationType = (AnimationType)anim.AnimationType,
                    AnimationKey = anim.AnimationKey,
                    MoveType = (MoveType)anim.MoveType,
                    MoveDirection = (MoveDirection)anim.MoveDirection,
                })
                .ToArray();
        }

        private protected SpeechUnit[] ParseSpeechUnits(SpeechUnitWebProfile[] profiles, ExhibitDefaultWebProfile currentProfile)
        {
            if (profiles == null) return new SpeechUnit[0];

            return profiles
                .Select(unit => new SpeechUnit
                {
                    AudioGuid = ResourceManager.Instance.AddExhibitAudio(currentProfile.Id, unit.URLValue),
                    Subtitles = unit.Subtitles,
                    PlaybackDelay = unit.PlaybackDelay,
                    AnimationUnits = ParseAnimationUnits(unit.AnimationUnits),
                })
                .ToArray();
        }

        private protected FAQUnit[] ParseFAQUnits(FAQUnitWebProfile[] profiles, ExhibitDefaultWebProfile currentProfile)
        {
            if (profiles == null) return new FAQUnit[0];

            return profiles
                .Select(profile => new FAQUnit()
                {
                    Name = profile.Name,
                    Text = profile.Text,
                    Image = ResourceManager.Instance.AddExhibitImage(currentProfile.Id, profile.ImageName),
                })
                .ToArray();
        }

        private protected Character[] ParseCharacter(CharacterWebProfile[] profiles)
        {
            if (profiles == null) return new Character[0];

            return profiles
                .Select(unit => new Character()
                {
                    CharacterPositionIndex = unit.CharacterPositionIndex,
                    CharacterObject = Resources.Load<GameObject>("Prefabs/Characters/" + unit.CharacterObjectName),
                })
                .ToArray();
        }
    }
}
