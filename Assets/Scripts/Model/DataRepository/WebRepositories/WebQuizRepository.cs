namespace Naval.Model
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class WebQuizRepository : AbstractRepository
    {
        private string _url;

        public WebQuizRepository(IDataSourceTemplate<IDataProfile> dataSource) : base(dataSource)
        {
            _url = ((WebServerSource<QuestionWebProfile, ISerializer>)_dataSource).URL;
        }

        protected private override IEnumerator InitiateData(IEnumerable<IDataProfile> rawData)
        {
            foreach (var profile in rawData)
            {
                var currentProfile = (QuestionWebProfile)profile;

                AbstractQuizQuestionType questionType;
                var type = (QuestionTypeWebEnum)currentProfile.QuizQuestionType.EnumNumber;
                switch (type)
                {
                    case QuestionTypeWebEnum.Text:
                        questionType = new TextQuestionType()
                        {
                            QuestionTitle = currentProfile.QuestionTitle,
                            Text = currentProfile.EntityValue,
                        };
                        break;

                    case QuestionTypeWebEnum.Image:
                        var guid = ResourceManager.Instance.AddQuizImage(currentProfile.EntityValue);
                        questionType = new ImageQuestionType()
                        {
                            QuestionTitle = currentProfile.QuestionTitle,
                            Image = guid,
                        };
                        break;

                    case QuestionTypeWebEnum.Morse:
                        currentProfile.Id = "Morse";
                        currentProfile.EntityValue ??= "";
                        questionType = new MorseQuestionType()
                        {
                            QuestionTitle = currentProfile.QuestionTitle,
                            Words = currentProfile.EntityValue.Split(','),
                        };
                        break;

                    default:
                        questionType = null;
                        Debug.LogError("Invalid question type parsed!");
                        break;
                }

                var newQuestion = new QuizQuestion
                {
                    Id = currentProfile.Id,
                    QuestionType = questionType,
                    CorrectAnswer = currentProfile.CorrectAnswer,
                    IncorrectAnswers = currentProfile.IncorrectAnswers,
                };

                if (_dataDictionary.ContainsKey(newQuestion.Id))
                {
#if DEBUG
                    Debug.LogError(this.GetType().ToString() + " - an item with the same key has already been added!");
#endif
                }
                else
                {
                    _dataDictionary.Add(newQuestion.Id, newQuestion);
                }
            }

            yield break;
        }
    }
}
