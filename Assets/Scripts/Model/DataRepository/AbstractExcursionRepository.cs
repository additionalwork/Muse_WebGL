namespace Naval.Model
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public abstract class AbstractExcursionRepository : AbstractRepository
    {
        public ExhibitDefault Root { get; protected private set; }

        protected AbstractExcursionRepository(IDataSourceTemplate<IDataProfile> dataSource) : base(dataSource)
        {
        }
    }
}
