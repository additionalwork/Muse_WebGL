namespace Naval.Model
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public abstract class AbstractRepository
    {
        public bool IsDataLoaded { get; private set; }
        public event Action DataLoadedEvent;
        internal readonly IDataSourceTemplate<IDataProfile> _dataSource;
        private protected readonly Dictionary<string, IData> _dataDictionary = new();
        public IData this[string dataId]
        {
            get { return _dataDictionary[dataId]; }
        }

        public AbstractRepository(IDataSourceTemplate<IDataProfile> dataSource)
        {
            _dataSource = dataSource;
            IsDataLoaded = false;
        }

        public IEnumerator ToInitiateData()
        {
            yield return _dataSource.LoadData();

            if (_dataSource.DataSet != null && _dataSource.DataSet.Count() > 0)
            {
                yield return InitiateData(_dataSource.DataSet);
                DataLoadedEvent?.Invoke();
                IsDataLoaded = true;
            }
        }

        protected private abstract IEnumerator InitiateData(IEnumerable<IDataProfile> rawData);

        public IData GetData(string dataId)
        {
            return _dataDictionary[dataId];
        }

        public bool HasData(string dataId)
        {
            return _dataDictionary.ContainsKey(dataId);
        }

        public IEnumerable<T> GetAllData<T>()
            where T : IData
        {
            return _dataDictionary.Values.Select(item => (T)item);
        }

        public IEnumerable<T> GetAllData<T>(Predicate<IData> predicate)
            where T : IData
        {
            return _dataDictionary.Values.Where(item => predicate(item)).Select(item => (T)item);
        }
    }
}
