namespace Naval.AR
{
    using Naval.Model;
    using System;
    using UnityEngine;

    [Serializable]
    public struct ARAnimationSet
    {
        public AudioClip AudioClip;
        public string Subtitles;
        public float PlaybackDelay;
        public AnimationUnit[] AnimationUnits;
    }
}
