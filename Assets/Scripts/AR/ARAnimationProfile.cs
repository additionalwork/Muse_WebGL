﻿namespace Naval.AR
{
    using Naval.Model;
    using UnityEngine;

    public class ARAnimationProfile : ScriptableObject
    {
        public string Id;
        public ARAnimationSet[] ARAnimationSets;
        public Character[] Characters;
    }
}
