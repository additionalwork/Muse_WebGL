namespace Naval.AR
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class ARControllerSingleton : MonoBehaviour
    {
        public Dictionary<string, ARAnimationProfile> AnimationProfiles;

        public static ARControllerSingleton Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
#if DEBUG
                Debug.LogError("More than one " + GetType().ToString() + " in scene detected!");
#endif
                Destroy(this);
            }
            else
            {
                Instance = this;
            }

            AnimationProfiles = Resources.LoadAll<ARAnimationProfile>("ARAnimationProfiles")
                .ToDictionary(itemKey => itemKey.Id);
        }

        private void OnDestroy()
        {
            Instance = null;
        }
    }
}
