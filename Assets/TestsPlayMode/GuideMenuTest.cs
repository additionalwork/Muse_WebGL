using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Naval.UI;
using Naval.Model;
using UnityEngine.SceneManagement;
using System.Linq;


public class GuideMenuTest
{
    [UnityTest]
    public IEnumerator CreateSubWindow_MenuHide_IsNull()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Guide/InteractiveGuideWindow"));
        var window = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Guide/QRWindow"));

        var guide = obj.GetComponent<GuideMenu>();
        yield return new WaitForSecondsRealtime(1);

        guide.CreateSubWindow(window);
        yield return new WaitForSecondsRealtime(1);

        Assert.IsNull(guide.CurrentWindow);
    }

    [UnityTest]
    public IEnumerator CreateMajorWindow_MenuHide_IsNull()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Guide/InteractiveGuideWindow"));
        var window = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Guide/QRWindow"));

        var guide = obj.GetComponent<GuideMenu>();
        yield return new WaitForSecondsRealtime(1);

        guide.CreateMajorWindow(window);
        yield return new WaitForSecondsRealtime(1);

        Assert.IsNull(guide.CurrentWindow);
    }
}
