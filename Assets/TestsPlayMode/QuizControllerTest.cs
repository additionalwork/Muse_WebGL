using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Naval.UI;
using UnityEngine.SceneManagement;

public class QuizControllerTest
{
    [UnityTest]
    public IEnumerator ResetQuiz_QuizStartedChange_IsFasle()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Quiz/QuizPanel"));

        var quiz = obj.GetComponent<QuizWindow>();
        yield return new WaitForSecondsRealtime(1);
        quiz._quizController.ResetQuiz();
        Assert.IsFalse(quiz._quizController.QuizStarted);
    }

    [UnityTest]
    public IEnumerator NextQuestion_QuizStartedChange_IsFasle()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Quiz/QuizPanel"));

        var quiz = obj.GetComponent<QuizWindow>();
        yield return new WaitForSecondsRealtime(2);
        var lastNumber = quiz._quizController.CurrentQuestion;
        quiz._quizController.NextQuestion();
        yield return new WaitForSecondsRealtime(2);
        Assert.IsFalse(quiz._quizController.CurrentQuestion == lastNumber);
    }

    [UnityTest]
    public IEnumerator ShuffleQuestions_ReturnEmptyArray_IsTrue()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Quiz/QuizPanel"));

        var quiz = obj.GetComponent<QuizWindow>();
        yield return new WaitForSecondsRealtime(2);
        quiz._quizController._questionsCount = -2;
        quiz._quizController.ShuffleQuestions();
        yield return new WaitForSecondsRealtime(2);
        Assert.IsTrue(quiz._quizController.ChosenQuestions.Count == 0);
    }
}
