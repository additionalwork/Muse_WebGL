using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Naval.UI;
using Naval.Model;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using System.Linq;


public class GuideControllerTest
{
    [UnityTest]
    public IEnumerator SetPlaySpeed_TimeScaleChange_IsTrue()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Guide/InteractiveGuideWindow"));

        var guide = obj.GetComponent<InteractiveGuide>();
        yield return new WaitForSecondsRealtime(1);

        guide.SetPlaySpeed(10f);
        Assert.True(Time.timeScale == 10f);
    }

    [UnityTest]
    public IEnumerator StartSpeech_TimeScaleChange_IsTrue()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Guide/InteractiveGuideWindow"));

        var controller = obj.GetComponent<InteractiveGuide>();
        var speechUnit = new SpeechUnit { /*AudioGuid = "Hall2",*/ PlaybackDelay = 10f, Subtitles = "Hall2_NameKey" };
        yield return new WaitForSecondsRealtime(1);
        controller.currentSpeedIndex = 0;
        controller.StartAudioSpeech(speechUnit, new ExhibitDefault {});

        Assert.IsTrue(Time.timeScale == 1);
        Object.Destroy(controller.gameObject);
    }

    [UnityTest]
    public IEnumerator SetAnswer_AnswerIsSet_IsTrue()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Guide/InteractiveGuideWindow"));


        var controller = obj.GetComponent<InteractiveGuide>();
        yield return new WaitForSecondsRealtime(1);

        controller.SetAnswer("answer");
        Assert.IsTrue(controller.taskAnswer == "answer");
        Object.Destroy(controller.gameObject);
    }

    [UnityTest]
    public IEnumerator SkipTask_IsSkipping_IsTrue()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Guide/InteractiveGuideWindow"));


        var controller = obj.GetComponent<InteractiveGuide>();
        yield return new WaitForSecondsRealtime(1);

        controller.SkipTask();
        Assert.IsTrue(controller.toSkipTask);
        Object.Destroy(controller.gameObject);
    }

    [UnityTest]
    public IEnumerator ShowEndPanel_IsShowing_IsTrue()
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        yield return new WaitForSecondsRealtime(1);

        var obj = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/UI2.0/Guide/InteractiveGuideWindow"));

        var controller = obj.GetComponent<InteractiveGuide>();
        yield return new WaitForSecondsRealtime(1);

        controller.ShowEndPanel(new ExhibitInteractive { ChildExhibits = new ExhibitInteractive[0] });

        Assert.IsNull(controller.slideRoutine);
        Object.Destroy(controller.gameObject);
    }
}
